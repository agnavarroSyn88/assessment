using System;
using FullInspector.Internal;

namespace FullInspector.Generated {
    [CustomPropertyEditor(typeof(UniRx.IntReactiveProperty))]
    public class Generated_UniRx_IntReactiveProperty_PropertyEditor : fiGenericPropertyDrawerPropertyEditor<Generated_UniRx_IntReactiveProperty_MonoBehaviourStorage, UniRx.IntReactiveProperty> {
        public override bool CanEdit(Type type) {
            return typeof(UniRx.IntReactiveProperty).IsAssignableFrom(type);
        }
    }
}
