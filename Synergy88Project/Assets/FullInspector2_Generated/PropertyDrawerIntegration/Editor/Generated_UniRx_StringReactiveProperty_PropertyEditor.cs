using System;
using FullInspector.Internal;

namespace FullInspector.Generated {
    [CustomPropertyEditor(typeof(UniRx.StringReactiveProperty))]
    public class Generated_UniRx_StringReactiveProperty_PropertyEditor : fiGenericPropertyDrawerPropertyEditor<Generated_UniRx_StringReactiveProperty_MonoBehaviourStorage, UniRx.StringReactiveProperty> {
        public override bool CanEdit(Type type) {
            return typeof(UniRx.StringReactiveProperty).IsAssignableFrom(type);
        }
    }
}
