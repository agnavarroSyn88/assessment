using System;
using FullInspector.Internal;
using Sandbox.Playables;

namespace FullInspector.Generated {
    [CustomPropertyEditor(typeof(UnityEngine.Events.UnityEvent<CharacterAnimPlayables.EventParam_>))]
    public class Generated_UnityEngine_Events_UnityEvent_CharacterAnimPlayables_EventParam__PropertyEditor : fiGenericPropertyDrawerPropertyEditor<Generated_UnityEngine_Events_UnityEvent_CharacterAnimPlayables_EventParam__MonoBehaviourStorage, Generated_UnityEngine_Events_UnityEvent_CharacterAnimPlayables_EventParam__NoGenerics> {
        public override bool CanEdit(Type type) {
            return typeof(UnityEngine.Events.UnityEvent<CharacterAnimPlayables.EventParam_>).IsAssignableFrom(type);
        }
    }
}
