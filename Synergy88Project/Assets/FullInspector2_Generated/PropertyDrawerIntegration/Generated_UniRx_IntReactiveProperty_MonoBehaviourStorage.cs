using System;
using FullInspector.Internal;
using UnityEngine;

namespace FullInspector.Generated {
    [AddComponentMenu("")]
    public class Generated_UniRx_IntReactiveProperty_MonoBehaviourStorage : fiPropertyDrawerMonoBehaviorContainer<UniRx.IntReactiveProperty> {} 
}
