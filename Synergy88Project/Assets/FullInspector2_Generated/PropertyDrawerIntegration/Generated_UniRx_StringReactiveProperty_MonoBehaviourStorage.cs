using System;
using FullInspector.Internal;
using UnityEngine;

namespace FullInspector.Generated {
    [AddComponentMenu("")]
    public class Generated_UniRx_StringReactiveProperty_MonoBehaviourStorage : fiPropertyDrawerMonoBehaviorContainer<UniRx.StringReactiveProperty> {} 
}
