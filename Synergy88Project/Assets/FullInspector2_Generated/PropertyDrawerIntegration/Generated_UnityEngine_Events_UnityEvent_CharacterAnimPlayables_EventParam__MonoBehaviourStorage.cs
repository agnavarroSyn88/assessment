using System;
using FullInspector.Internal;
using UnityEngine;
using Sandbox.Playables;

namespace FullInspector.Generated {
    [Serializable]
    public class Generated_UnityEngine_Events_UnityEvent_CharacterAnimPlayables_EventParam__NoGenerics : UnityEngine.Events.UnityEvent<CharacterAnimPlayables.EventParam_> {} 
    [AddComponentMenu("")]
    public class Generated_UnityEngine_Events_UnityEvent_CharacterAnimPlayables_EventParam__MonoBehaviourStorage : fiPropertyDrawerMonoBehaviorContainer<Generated_UnityEngine_Events_UnityEvent_CharacterAnimPlayables_EventParam__NoGenerics> {} 
}
