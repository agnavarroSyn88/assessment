﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using UniRx;
using FrameworkInputSystem;
using System;
using UnityEngine.AI;
using Common.Query;
using Synergy88;

namespace Sandbox.Character
{

    public struct MoveInput
    {
        public float x;
        public float z;
        public bool jump;
    }
    public partial class CharacterMovement : MonoBehaviour
    {
        bool _BlockMovement;
        public void SetBLockMovement(bool _var)
        {
            Move(0, 0, false);
            _BlockMovement = _var;
        }
        #region VARIABLES

        CharacterMovement _CharMovement;
        [SerializeField]
        private int PlayerID;
        public int GetPlayerID()
        {
            return PlayerID;
        }
        //AUTO SET
        //----------------------------------------------------------
        private Rigidbody _Rigidbody;

        private Transform _Transform;
        public Transform Transform
        {
            get
            {
                if (!_Transform) _Transform = transform;
                return _Transform;
            }
        }
        //MANUAL SET
        //----------------------------------------------------------
        [SerializeField]
        private CapsuleCollider _CharacterCollider;
        [SerializeField]
        private MovementStats _MovementData;
        [SerializeField]
        private Transform _CameraTransform;
        [SerializeField]
        private CharacterPresenter _Presenter;

        Animator _Animator;

        //SWITCHES
        //----------------------------------------------------------
        public bool IsDead { get; private set; }
        public bool Strafing { get; private set; }
        public bool IsGrounded { get; private set; }
        public bool IsSprinting { get; private set; }
        
        [SerializeField] private bool _IsMoving;
        public bool IsMoving
        {
            get { return _IsMoving; }
            private set { _IsMoving = value; }
        }
        //EXTERNAL SETUP
        //----------------------------------------------------------
        [HideInInspector] public bool CanSnapToTarget;
        [HideInInspector] public Transform LockOnTarget;
        
        [HideInInspector]
        [Obsolete("Too high maintenance; will be switching to a more automated behaviour.")]
        public bool AllowRotation;
        public void SetAllowRotation(bool value)
        {
            AllowRotation = value;
        }
        public void SetLockOnTarget(Transform t)
        {
            LockOnTarget = t;
        }
        //EXTERNAL ACCESS
        //----------------------------------------------------------

        public bool CanRotate
        {
            get { return ActiveAction == ActionState.Move; }
        }
        private bool DoesNeedFullLegacyStaminaRecovery
        {
            get
            {
                return false;
            }
        }

        //CONSTANTS
        //----------------------------------------------------------
        private const float STOPPING_THRESHOLD = 0.005f;
        private const float SMOOTH_TIME = 0.05f;
        
        //EDITOR
        //----------------------------------------------------------
        public bool MouseControlsFacingDirection;
        public bool useFixedDeltaTime = true;

        #endregion
        //==============================================================================================================================================================================================
        private void Awake()
        {
            _Animator = GetComponent<Animator>();
        }
        private void Start()
        {
            MoveSpeedMultiplier = _MovementData.MoveSpeedMultiplier;
            _VariableGroundCheckDistance = _MovementData.GroundCheckDistance;
            _Rigidbody = GetComponent<Rigidbody>();
        }
        public void InitializeThisCharacter(int id)
        {
            _CameraTransform = QuerySystem.Query<Transform>(QueryIds.GameCamera);
            PlayerID = id;
        }
        //MOVE DEPENDENCIES
        //==============================================================================================================================================================================================
        public void Sprint()
        {
            IsSprinting = true;
        }
        #region Movement
        private MoveInput _MoveInput;
        private bool _IntendToMove;
        private Vector3 _MoveIntentVector;
        
        public void Move(float x, float z, bool jump)
        {
            if (_BlockMovement)
            {
                return;
            }

            // set intend to move
            _IntendToMove = x != 0 || z != 0;
            Strafing = true;
            if (!_Presenter.CanMove)
            {
                x = 0f;
                z = 0f;
                jump = false;
            }

            if (IsGrounded)
            {
                _MoveInput = FreeMove(x, z);
            }

            _MoveInput.jump = jump;

            // TODO @any movement #1
            // update animator XZ values
            // which, in turn, will drive the character's root motion
            _Animator.SetFloat(AnimationConstants.ANIM_VEL_X, _MoveInput.x);
            _Animator.SetFloat(AnimationConstants.ANIM_VEL_Z, _MoveInput.z);
        }



        #region Movement Modes
        
        private void FreeMoveRotate(Vector3 dir)
        {
            bool moving = dir != Vector3.zero;

            if (CanRotate && moving)
            {
                Quaternion desiredRot = Quaternion.LookRotation(dir, Vector3.up);
                transform.localRotation = Quaternion.Slerp(transform.rotation, desiredRot, _MovementData.RotationSpeed * DeltaTime);
            }
        }

        private float _LastSpeed = 0;
        private MoveInput FreeMove(float x, float z)
        {
            Vector3 input = (_MovementData.InvertInputAxisWhenDefending) ? new Vector3(-x, 0, -z) : new Vector3(x, 0, z);

            _MoveIntentVector = input;

            if (IsSprinting && (x < 0.001f && x > -0.001f) && (z < 0.001f && z > -0.001f))
            {
                Debug.Log("stopped sprinting");
                IsSprinting = false;
            }

            // transform input relative to camera.
            Vector3 dir = _CameraTransform.TransformDirection(input);
            dir = Vector3.ProjectOnPlane(dir, Vector3.up);

            // determine how to rotate character in free move
            if (CanSnapToTarget) SnapToTarget(dir);
            else FreeMoveRotate(dir);

            // get speed from input's magnitude
            float speed = IsSprinting ? 2.0f : input.magnitude;

            // decrease speed if needs a full stamina recovery
            if (DoesNeedFullLegacyStaminaRecovery) speed = Mathf.Min(speed, 0.5f);

            // smooth speed
            float vel = 0;
            speed = Mathf.SmoothDamp(_LastSpeed, speed, ref vel, SMOOTH_TIME);
            if (!IsSprinting) speed = Mathf.Clamp01(speed);
            if (speed < STOPPING_THRESHOLD) speed = 0;
            _LastSpeed = speed;

            return new MoveInput() { x = 0, z = speed, jump = false };
        }

        #endregion
        #endregion
        //==============================================================================================================================================================================================
        #region MATH
   
        private Vector3 _LastInput = Vector3.zero;

        private Vector3 Clean(Vector3 input)
        {
            Vector3 result = input;

            if (float.IsNaN(result.x) || float.IsInfinity(result.x)) result.x = 0;
            if (float.IsNaN(result.y) || float.IsInfinity(result.y)) result.y = 0;
            if (float.IsNaN(result.z) || float.IsInfinity(result.z)) result.z = 0;

            return result;
        }

        #endregion
        //UTILS
        //==============================================================================================================================================================================================
        #region UTILS
        private void SnapToTarget(Vector3 dir)
        {
            bool moving = dir != Vector3.zero;

            Quaternion desiredRot = Transform.localRotation;
            if (moving)
            {
                desiredRot = Quaternion.LookRotation(dir, Vector3.up);
            }

            bool hasTarget = _Presenter.GetTarget() != null;
            if (hasTarget)
            {
                Vector3 targetDir = _Presenter.GetTarget().transform.position - Transform.position;

                bool shouldSnapToTarget = Vector3.Angle(Transform.forward, moving ? dir : targetDir) < _MovementData.SnappingAngleThreshold;

                if (shouldSnapToTarget) desiredRot = Quaternion.LookRotation(targetDir, Vector3.up);
            }

            if (AllowRotation && (moving || hasTarget))
            {
                Transform.localRotation = Quaternion.Slerp(Transform.rotation, desiredRot, _MovementData.RotationSpeed * DeltaTime);
            }
        }

        private IEnumerator Lerp(float from, float to, float duration, System.Action<float> lerpAction, System.Action doneAction = null)
        {
            float t = 0;

            while (t < duration)
            {
                float value = Mathf.Lerp(from, to, t / duration);
                if (lerpAction != null) lerpAction(value);

                t += DeltaTime;
                yield return null;
            }

            if (doneAction != null) doneAction();
        }


        private Vector3 GetMouseDirection()
        {
            Plane p = new Plane(Vector3.up, Transform.position);
            Ray r = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            float d;
            if (p.Raycast(r, out d))
            {
                return r.GetPoint(d);
            }

            return Vector3.zero;
        }


        #endregion
        //DEBUG
        //==============================================================================================================================================================================================
        #region DEBUG
#if UNITY_EDITOR
        public static void DrawDebugX(Vector3 center, float radius, Color color)
        {
            Vector3 line1Start = center + Vector3.forward * radius;
            Vector3 line1End = center - Vector3.forward * radius;
            Vector3 line2Start = center + Vector3.right * radius;
            Vector3 line2End = center - Vector3.right * radius;
            Debug.DrawLine(line1Start, line1End, color);
            Debug.DrawLine(line2Start, line2End, color);
        }

        public static void DrawDebugX(Vector3 center, float radius, Color color, float duration)
        {
            Vector3 line1Start = center + Vector3.forward * radius;
            Vector3 line1End = center - Vector3.forward * radius;
            Vector3 line2Start = center + Vector3.right * radius;
            Vector3 line2End = center - Vector3.right * radius;
            Debug.DrawLine(line1Start, line1End, color, duration);
            Debug.DrawLine(line2Start, line2End, color, duration);
        }

        private void OnDrawGizmos()
        {
            // a visual representation of the sphere used to detect if the character is grounded or not.
            Gizmos.color = IsGrounded ? Color.yellow : Color.red;
            Gizmos.DrawWireSphere(_sphereOrigin, _radius);
        }
#endif
#if UNITY_EDITOR
        private Vector3 _sphereOrigin;
        private float _radius;
#endif

        #endregion
        //UPDATE FUNCTIONALITY
        //==============================================================================================================================================================================================
        #region UPDATE

            
        private bool _DisableGravity = false;
#if UNITY_EDITOR
        public float currentSpeed;
#endif
        
        public Vector3 RigidbodyVelocity
        {
            get
            {
                if (_Timeline) return Clean(_Timeline.rigidbody.velocity);
                else return Clean(_Rigidbody.velocity);
            }
            private set
            {
                if (_Timeline) _Timeline.rigidbody.velocity = Clean(value);
                else _Rigidbody.velocity = Clean(value);
            }
        }

        [SerializeField]
        private float _MoveSpeedMultiplier = 1.0f;
        public float MoveSpeedMultiplier
        {
            get { return _MoveSpeedMultiplier; }
            set
            {
                _MoveSpeedMultiplier = value;
                if (_Animator) _Animator.SetFloat(AnimationConstants.MOVE_SPEED_MULTIPLIER, _MoveSpeedMultiplier);
            }
        }

        [SerializeField]
        private Vector3 _AnimatorMoveDelta;
        public Vector3 AnimatorMoveDelta
        {
            get
            {
                Vector3 result = _AnimatorMoveDelta;

                return Clean(result);
            }
            private set
            {
                _AnimatorMoveDelta = value;
            }
        }
        private Vector3 _LastWorldPosition = Vector3.zero;


        private void OnAnimatorMove()
        {
            bool shouldApplyRootMotion = true;
            if (Strafing) shouldApplyRootMotion = ActiveAction != ActionState.Move;

            _Animator.applyRootMotion = IsGrounded && shouldApplyRootMotion;
            AnimatorMoveDelta = _Animator.deltaPosition / (useFixedDeltaTime ? FixedDeltaTime : DeltaTime);
        }
        //UPDATE
        //----------------------------------------------------------
        private void Update()
        {
            // update animator values
            _Animator.SetFloat(AnimationConstants.ANIM_VEL_Y, RigidbodyVelocity.y);
            _Animator.SetBool(AnimationConstants.ANIM_GROUNDED, IsGrounded);
            _Animator.SetBool(AnimationConstants.INTENT_TO_MOVE, /*_intendToMove*/ _MoveIntentVector.magnitude > 0.1f);

            _Animator.SetBool(AnimationConstants.STRAFING, Strafing);
        }
        //----------------------------------------------------------
        //FIX UPDATE
        //----------------------------------------------------------
        private void FixedUpdate()
        {
            // determine if character is moving or not
            float distanceDelta = Vector3.Distance(_LastWorldPosition, Transform.position);
            IsMoving = distanceDelta > STOPPING_THRESHOLD;

            _LastWorldPosition = Transform.position;

            PlayerFixedUpdate();
        }
        //----------------------------------------------------------
        //PLAYED UPDATE
        //----------------------------------------------------------
        private void PlayerFixedUpdate()
        {
            Vector3 moveDelta;

            if (Strafing && ActiveAction == ActionState.Move)
            {
                Vector3 worldMoveIntent = _CameraTransform.TransformDirection(_MoveIntentVector);

                moveDelta = (worldMoveIntent * _MovementData.MaxMoveSpeed * MoveSpeedMultiplier) * (DoesNeedFullLegacyStaminaRecovery ? 0.5f : 1.0f);
            }
            else
            {
                moveDelta = AnimatorMoveDelta;
            }
            
            // Apply jump force (if any)
            //moveDelta = ProcessJump(moveDelta);

            // Check if grounded
            IsGrounded = CheckGrounded();

            if (IsGrounded)
            {
                moveDelta = GroundedMoveBehaviour(moveDelta);
            }
            else
            {
                moveDelta = AirborneMoveBehaviour(moveDelta);
            }

            // set rigidbody velocity to computed move delta
            RigidbodyVelocity = moveDelta;

            // draw debug velocity
            if (RigidbodyVelocity != Vector3.zero) DrawArrow.ForDebug(transform.position, RigidbodyVelocity, Color.green);

#if UNITY_EDITOR
            currentSpeed = RigidbodyVelocity.magnitude;
#endif

            // change ground check distance based on sign of rigidbody velocity's y-component.
            _VariableGroundCheckDistance = (RigidbodyVelocity.y <= 0) ? _MovementData.GroundCheckDistance : 0.01f;
        }
        //----------------------------------------------------------
        #endregion
        //GROUNDED
        //==============================================================================================================================================================================================

        private float _VariableGroundCheckDistance = 0.2f;
        private RaycastHit _GroundHitInfo;
        private Vector3 _GroundNormal;

        #region GROUNDED
        private bool DontAllowMovement
        {
            get
            {
                return
                    //!_BreakOverride
                    (!_IntendToMove)
                    && ActiveAction != ActionState.Attack
                    && ActiveAction != ActionState.Skill
                    && ActiveAction != ActionState.Hit
                    && ActiveAction != ActionState.FullDodge;
            }
        }

        private Vector3 Gravity
        {
            get
            {
                return Physics.gravity * _MovementData.GravityMultiplier;
            }
        }

        private bool CheckGrounded()
        {
            bool grounded;
            float radius = _CharacterCollider.radius * _MovementData.SphereCastScale;
            float groundCheckDist = radius + _VariableGroundCheckDistance;
            Vector3 spherecastOrigin = Transform.position;
            Ray ray = new Ray(spherecastOrigin + Vector3.up * _MovementData.RayOffset, Vector3.down * groundCheckDist);

            if (Physics.SphereCast(ray, radius, out _GroundHitInfo, groundCheckDist, _MovementData.GroundLayerMask, QueryTriggerInteraction.Ignore))
            {
                _GroundNormal = _GroundHitInfo.normal;

                // find ground angle
                float groundAngle = Vector3.Angle(_GroundNormal, Vector3.up);

                // find distance of hit point from transform position (projected on XZ plane)
                Vector3 origin = new Vector3(spherecastOrigin.x, _GroundHitInfo.point.y, spherecastOrigin.z);
                float hitPointDistance = Vector3.Distance(origin, _GroundHitInfo.point);

                // check the following criteria to refine ground detection
                // (taking into account walls and if too far from the origin)
                bool isWall = groundAngle > _MovementData.WallAngleThreshold;
                bool farFromOrigin = hitPointDistance > (radius * _MovementData.HitPointDistanceThreshold);

                grounded = !isWall && !farFromOrigin;
            }
            else
            {
                grounded = false;
                _GroundNormal = Vector3.up;
            }

#if UNITY_EDITOR
            // draw all debug

            // draw ray and origin
            DrawDebugX(ray.origin, 0.1f, Color.yellow);
            DrawArrow.ForDebug(ray.origin, ray.direction, Color.yellow);

            // draw hit data if grounded
            if (grounded)
            {
                _sphereOrigin = _GroundHitInfo.point + _GroundNormal * radius;
                _radius = radius;

                // draw hit point and normal
                DrawDebugX(_GroundHitInfo.point, 0.1f, Color.red);
                DrawArrow.ForDebug(_GroundHitInfo.point, _GroundNormal * 0.25f, Color.white, 0.05f);
            }
            else
            {
                _sphereOrigin = spherecastOrigin + _GroundNormal * radius;
                _radius = radius * 0.05f;
            }
#endif
            return grounded;
        }

        private Vector3 GroundedMoveBehaviour(Vector3 inputVector)
        {
            if (!_DisableGravity)
            {
                // project onto ground normal
                Vector3 outputVector = Vector3.ProjectOnPlane(inputVector, _GroundNormal);

                // clamp to ground
                outputVector = ClampToGround(outputVector);

                // Don't allow movement on the xz-plane when following condition is statisfied.
                if (DontAllowMovement)
                {
                    // set xz to zero
                    outputVector = new Vector3(0, outputVector.y, 0);
                }

                return outputVector;
            }

            return inputVector;
        }

        private Vector3 AirborneMoveBehaviour(Vector3 inputVector)
        {
            if (!_DisableGravity)
            {
                Vector3 outputVector = inputVector + Gravity;

                return outputVector;
            }
            return inputVector;
        }

        private Vector3 ClampToGround(Vector3 inputVector)
        {
            Vector3 hitpoint = _GroundHitInfo.point;
            Vector3 origin = transform.position;

            float offsetVal = Mathf.Abs(hitpoint.y - origin.y);
            Vector3 dir = hitpoint - origin;
            dir = Vector3.Project(dir, Vector3.up).normalized * offsetVal;
            DrawArrow.ForDebug(origin, dir, Color.cyan, 0.1f);

            // apply aggressive clamping
            float aggressiveClamping = _MovementData.AggressiveGroundClamping ? (useFixedDeltaTime ? FixedDeltaTime : DeltaTime) : 1.0f;

            return inputVector + dir / aggressiveClamping;
        }
        #endregion

        //TIME
        //==============================================================================================================================================================================================
        #region TIME
        private Chronos.Timeline _Timeline;
        public float DeltaTime
        {
            get
            {
                if (_Timeline) return _Timeline.deltaTime;
                else return UnityEngine.Time.deltaTime;
            }
        }

        public float FixedDeltaTime
        {
            get
            {
                if (_Timeline) return _Timeline.fixedDeltaTime;
                else return UnityEngine.Time.fixedDeltaTime;
            }
        }

        public float TimeSinceGameStart
        {
            get
            {
                if (_Timeline) return _Timeline.time;
                else return UnityEngine.Time.time;
            }
        }
        #endregion

    }
}