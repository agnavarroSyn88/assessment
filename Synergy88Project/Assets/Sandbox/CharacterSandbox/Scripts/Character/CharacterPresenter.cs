﻿using UnityEngine;
using UniRx;
using FrameworkInputSystem;
using FullInspector;
using System.Collections.Generic;
using System;
using Rewired;
using UnityEngine.Playables;
using UnityEngine.Animations;

namespace Sandbox.Character
{
    public class CharacterPresenter : BaseBehavior
    {
        [SerializeField]
        private ReactiveProperty<CharacterPresenter> _Target;

        public CharacterPresenter GetTarget() { return _Target.Value; }
        public bool CanMove { get; set; }

        public event Action<ActionState> OnActionStateChanged;
        public void NotifyActionStateChange(ActionState state)
        {
            if (OnActionStateChanged != null) OnActionStateChanged(state);
        }
    }
}
 