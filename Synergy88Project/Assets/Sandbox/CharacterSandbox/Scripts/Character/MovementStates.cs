﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FullInspector;

namespace Sandbox.Character
{
    public partial class CharacterMovement : MonoBehaviour
    {
        [SerializeField]
        private ActionState _ActiveAction = ActionState.Move;
        private ActionState _PreviousAction;

        public delegate void StateResolver(CharacterMovement moveControl, Animator animator);
        private StateResolver _OnExitState;

        public ActionState ActiveAction { get { return _ActiveAction; } }
        public ActionState PreviousAction { get { return _PreviousAction; } }

        private void EnterState(ActionState state, StateResolver enterResolver)
        {
            //Debug.LogFormat("{0}: State <color=orange>{1}</color> entered", gameObject.name, state.ToString());
            if (enterResolver != null) enterResolver(this, _Animator);
        }

        private void ExitState(ActionState state, StateResolver exitResolver)
        {
            //Debug.LogFormat("{0}: State <color=orange>{1}</color> exited", gameObject.name, state.ToString());
            if (exitResolver != null) exitResolver(this, _Animator);
        }

        public void SetActiveState(ActionState newState, StateResolver enterStateDelegate, StateResolver exitStateDelegate)
        {
            if (_ActiveAction == newState)
            {
                //Debug.LogFormat("{0}: previous state is equal to active state; skipping transition...", gameObject.name);
                return;
            }

            _PreviousAction = _ActiveAction;
            _ActiveAction = newState;

            //Debug.LogFormat("{0}: Changing action state: <color=yellow>{1}</color> -> <color=green>{2}</color>", gameObject.name, _PreviousAction.ToString(), _ActiveAction.ToString());

            ExitState(_PreviousAction, _OnExitState);        // exit from previous state using cached exit resolver
            EnterState(_ActiveAction, enterStateDelegate);  // enter new state using parameter enter resolver
            _OnExitState = exitStateDelegate;                // update cached exit resolver

            _Presenter.NotifyActionStateChange(newState);
        }

        public void SetActiveState(ActionState newState)
        {
            SetActiveState(newState, null, null);
        }
    }
}

public enum ActionState
{
    Move = 0,
    Attack = 1,
    Skill = 2,
    Block = 3,
    Parry = 4,
    FullDodge = 5,
    HalfDodge = 6,
    Hit = 7,
    Dead = 8,
    Break = 9,
    Emote = 10,
    AttackDeflected = 11,
    Rest = -1
}