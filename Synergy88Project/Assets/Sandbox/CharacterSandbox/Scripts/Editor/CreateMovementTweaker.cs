﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreateMovementTweaker {
    
    [MenuItem("Assets/Create/MovementStats")]
    public static void CreateMovementStats()
    {
        ScriptableObjectUtility.CreateAsset<MovementStats>();
    }
}