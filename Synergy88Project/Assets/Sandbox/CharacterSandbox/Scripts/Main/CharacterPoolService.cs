﻿using Common.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using UniRx;
using FrameworkInputSystem;
using Common.Query;
using Synergy88;
using System.Linq;
using Sandbox.Spawner;

namespace Sandbox.Character
{
    public class CharacterPoolService : BaseBehavior
    {
        //VARIABLES
        [SerializeField]
        private Transform _GameCam;

        [SerializeField]
        private Dictionary<string, GameObject> _CharacterList;
        
        [SerializeField]
        private PrefabManager _Pool;

        [SerializeField]
        private List<SwarmItem> _ActiveItemList;
        
        //INITIALIZATION
        protected override void Awake()
        {
            base.Awake();
            initializedata();
            Assertion.AssertNotNull(_Pool);
        }

        void initializedata()
        {
            Factory.Register<CharacterPoolService>(this);
            _CharacterList = new Dictionary<string, GameObject>();

            QuerySystem.RegisterResolver(QueryIds.GameCamera, (IQueryRequest request, IMutableQueryResult result) =>
            {
                result.Set(_GameCam);
            });
        }

        private void OnDestroy()
        {
            Factory.Clean<CharacterPoolService>();
        }

        //MAIN FUNCTIONALITY
        public void SpawnCharacter()
        {
            SpawnCharacter(MessageBroker.Default,null);
        }

        public void SpawnCharacter(IMessageBroker broker, SpawnData _spawndata)
        {
            SwarmItem temp = _Pool.Request("Char1").GetComponent<SwarmItem>();
            int PlayerID = _ActiveItemList.Count;

            temp.GetComponent<CharacterMovement>().InitializeThisCharacter( PlayerID );
            if(_spawndata != null)
            {
                temp.transform.position = _spawndata.SpawnLocation;
            }

            broker.Publish(new RequestSignalInjectionSignal
            {
                PlayerID = PlayerID.ToString()
            });

            if (_CharacterList.ContainsKey("P" + PlayerID.ToString()) == false)
            {
                _CharacterList.Add("P" + PlayerID.ToString(), temp.gameObject);
            }
            _ActiveItemList.Add(temp);
        }

        public GameObject FetchCharacterFromList(string id)
        {
            if (_CharacterList.ContainsKey(id))
            {
                return _CharacterList[id];
            }
            else
            {
                Debug.LogError(id + " Doesnt Exist!");
                return null;
            }
        }

        public void Destroy()
        {
            if (_ActiveItemList.Count >= 0)
            {
                SwarmItem item = _ActiveItemList.FirstOrDefault();
                item.Kill();
                _ActiveItemList.Remove(item);
            }
        }
    }
}