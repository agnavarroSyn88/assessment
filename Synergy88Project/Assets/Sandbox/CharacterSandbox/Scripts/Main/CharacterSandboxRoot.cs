﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UniRx;
using Common.Utils;
using Synergy88;
using Sandbox.Spawner;

namespace Sandbox.Character
{
    public class CharacterSandboxRoot : Scene
    {
        MessageBroker _MsgBroker = new MessageBroker();
        public MessageBroker GetMsgBroker
        {
            get { return _MsgBroker; }
        }

        private void InitializeSignal()
        {
            
        }

        protected override void Awake()
        {
            base.Awake();
            InitializeSignal();
        }

        protected override void Start()
        {
            base.Start();
            
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        public void SpawnCharacter()
        {
            Factory.Get<CharacterPoolService>().SpawnCharacter(_MsgBroker,null);
        }
        public void SpawnCharacter(SpawnData _spawnData)
        {
            Factory.Get<CharacterPoolService>().SpawnCharacter(_MsgBroker, _spawnData);
        }
    }

}
