﻿using FullInspector;
using UnityEngine;

[fiInspectorOnly]
public class MovementStats : ScriptableObject
{

    [InspectorCategory("General")] public float RotationSpeed = 10;
    [InspectorCategory("General")] public float SnappingAngleThreshold = 45;
    [InspectorCategory("General")] public float JumpForce = 10;
    [InspectorCategory("General")] public float GravityMultiplier = 2.0f;
    [InspectorCategory("General")] public bool InvertInputAxisWhenDefending;
    
    [InspectorCategory("Animation")] public float MoveSpeedMultiplier = 1.0f;
    [InspectorCategory("Animation")] public float MaxMoveSpeed = 2.5f;
    [InspectorCategory("Animation")] public AnimationCurve AnimationSpeedMap;

    [InspectorCategory("General")] public int TestInt = 2;
    [InspectorCategory("General")] public string TestString= "2";
    [InspectorCategory("General")] public Vector3 TestVEctor3 = new Vector3(0,0,0);
    [InspectorCategory("General")] public Quaternion TestQuaternion = new Quaternion( 0, 0, 0,0);

    [InspectorCategory("Grounded Checking")] public LayerMask GroundLayerMask = 1 << LayerConstants.LAYER_DEFAULT | 1 << LayerConstants.LAYER_ENVIRONMENT_MID | 1 << LayerConstants.LAYER_ENVIRONMENT_LARGE | 1 << LayerConstants.LAYER_DESTRUCTIBLES;
    [InspectorCategory("Grounded Checking")] public float GroundCheckDistance = 0.2f;
    [InspectorCategory("Grounded Checking")] public bool AggressiveGroundClamping = true;
    /// <summary>
    /// Y-offset of sphere cast's origin from the character's base.
    /// </summary>
    [InspectorCategory("Grounded Checking")] public float RayOffset = 0.5f;
    /// <summary>
    /// Scale of the sphere cast's radius relative to the character's capsule collider radius.
    /// </summary>
    [InspectorCategory("Grounded Checking")] [InspectorRange(0.0f, 1.0f)] public float SphereCastScale = 0.9f;
    /// <summary>
    /// The angle of the ground normal, relative to the world-space y-axis,
    /// that should be considered a "wall" by the character controller.
    /// </summary>
    [InspectorCategory("Grounded Checking")] [InspectorRange(45.0f, 90.0f)] public float WallAngleThreshold = 80.0f;
    /// <summary>
    /// An additional parameter to check whether the sphere cast result should be considered "ground."
    /// If the point of contact's distance from the character's origin exceeds this threshold, it should not be grounded.
    /// </summary>
    [InspectorCategory("Grounded Checking")] [InspectorRange(0.0f, 1.0f)] public float HitPointDistanceThreshold = 0.95f;
















    
}
public static class LayerConstants
{
        public const int LAYER_DEFAULT = 0;
        public const int LAYER_CHARACTERS = 8;
        public const int LAYER_POINTERS = 10;
        public const int LAYER_ENVIRONMENT_SMALL = 11;
        public const int LAYER_ENVIRONMENT_MID = 12;
        public const int LAYER_ENVIRONMENT_LARGE = 13;
        public const int LAYER_DESTRUCTIBLES = 14;
        public const int LAYER_DESTRUCTIBLE_TRIGGERS = 15;
        public const int LAYER_PHASING = 16;
        public const int LAYER_EFFECTORS = 17;
}