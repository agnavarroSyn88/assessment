﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.Character
{
    public static class AnimationConstants
    {

        public static string ANIM_VEL_X = "anim_velX";
        public static string ANIM_VEL_Z = "anim_velZ";
        public static string MOVE_SPEED_MULTIPLIER = "MoveSpeedMultiplier";
        public static string ANIM_VEL_Y = "anim_velY";
        public static string ANIM_GROUNDED = "anim_grounded";
        public static string INTENT_TO_MOVE = "IntendToMove";
        public static string STRAFING = "Strafing";
    }
}
