﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Sandbox.MovementDebug
{
    [Serializable]
    public class DebugButtonTemplate : MonoBehaviour
    {
        public int ID;
        public MessageBroker MessageBroker;
        public string VarName;
        public UnityEngine.UI.Button Button;
        public Text Text;
        public InputField[] InputFields;
    }
}