﻿using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Sandbox.MovementDebug;

namespace Synergy88
{
    public class MovementDebugSandboxRoot : Scene
    {
        [SerializeField]
        private PrefabManager _PrefabManager;

        [SerializeField]
        private MovementStats _MovementStats;
        public MovementStats MovementStats { get { return _MovementStats; } }

        [SerializeField]
        public List<DebugButtonTemplate> DebugButtonList;
        
        [SerializeField]
        private Transform _ButtonParent;
        
        protected override void Awake()
        {
            base.Awake();
        }

        protected override void Start()
        {
            base.Start();
            
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        public DebugButtonTemplate GenerateInputTemplates(string varName, object var)
        {
            GameObject obj = CheckTypeOfTemplate(var);
            obj.transform.SetParent(_ButtonParent);

            DebugButtonTemplate debugTemplate = obj.GetComponent<DebugButtonTemplate>();
            debugTemplate.MessageBroker = new MessageBroker();
            debugTemplate.VarName = varName;
            debugTemplate.ID = DebugButtonList.Count;

            debugTemplate.Button.OnClickAsObservable().Subscribe(_ =>
            {
                string test = "";

                if (var.GetType() == typeof(Vector3) || var.GetType() == typeof(Quaternion))
                {
                    test = new Vector3(
                        float.Parse(debugTemplate.InputFields[0].text),
                        float.Parse(debugTemplate.InputFields[1].text),
                        float.Parse(debugTemplate.InputFields[2].text)).ToString();
                }
                else
                {
                    test = debugTemplate.InputFields[0].text;
                }

                debugTemplate.MessageBroker.Publish(new UpdateDebugMoveSignal
                {
                    ID = debugTemplate.ID,
                    Val = test,
                    VarName = debugTemplate.VarName
                });
            });
            debugTemplate.Text.text = varName + ": " + var.ToString();
            DebugButtonList.Add(debugTemplate);

            return debugTemplate;
        }

        private GameObject CheckTypeOfTemplate(object var)
        {
            if (var.GetType() == typeof(int) || var.GetType() == typeof(float))
            {
                return _PrefabManager.Request("Float").GetComponent<SwarmItem>().gameObject;
            }
            else if (var.GetType() == typeof(string))
            {
                return _PrefabManager.Request("String").GetComponent<SwarmItem>().gameObject;
            }
            else if (var.GetType() == typeof(bool))
            {
                return _PrefabManager.Request("Bool").GetComponent<SwarmItem>().gameObject;
            }
            else if (var.GetType() == typeof(Vector3) || var.GetType() == typeof(Quaternion))
            {
                return _PrefabManager.Request("Vector3").GetComponent<SwarmItem>().gameObject;
            }
            return null;
        }
    }
}
