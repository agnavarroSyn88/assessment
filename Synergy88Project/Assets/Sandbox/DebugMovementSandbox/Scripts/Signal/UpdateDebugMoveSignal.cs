﻿namespace Sandbox.MovementDebug
{
    public class UpdateDebugMoveSignal
    {
        public int ID;
        public string Val;
        public string VarName;
    }
}