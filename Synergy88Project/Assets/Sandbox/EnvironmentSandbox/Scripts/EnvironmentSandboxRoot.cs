﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UniRx;

namespace Synergy88
{
    public class EnvironmentSandboxRoot : Scene
    {
        [SerializeField]
        private GameObject _SceneParent;
        public void InjectSceneObject(GameObject obj)
        {
            obj.transform.SetParent(_SceneParent.transform);
        }

        protected override void Awake()
        {
            base.Awake();
            
        }

        protected override void Start()
        {
            base.Start();
            
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }

}
