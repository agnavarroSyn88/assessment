﻿using Common.Query;
using FrameworkInputSystem;
using Rewired;
using Synergy88;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using System;
using System.Linq;
using Sandbox.Character;

namespace Sandbox.ControllerInput
{
    public class BaseInputScheme : MonoBehaviour, IInputScheme
    {
        protected IMessageBroker _InputBroker;
        [SerializeField]
        protected Player _RewiredPlayer;
        [SerializeField]
        private int _PlayerId;
        [SerializeField]
        private string _ControllerId;
        private Vector3 _MoveVector;
        protected bool _IsInitialized;

        [SerializeField]
        private ControllerType _ControllerType;
        public ControllerType GetControllerType()
        {
            return _ControllerType;
        }

        private Controller _Controller;
        public void SetController(Controller contrl)
        {
            _Controller = contrl;
        }
        
        public virtual void InitializeControlScheme(int id, string controllerid)
        {
            _PlayerId = id;
            _ControllerId = controllerid;
            _RewiredPlayer = ReInput.players.GetPlayer(_PlayerId);

            IQueryRequest _Query = QuerySystem.Start(QueryIds.MessageBroker);
            _Query.AddParameter(QueryIds.MessageBrokerId, "P" + _PlayerId.ToString());
            _InputBroker = QuerySystem.Complete<IMessageBroker>();

            CheckKeyboardFunctionality();

            _IsInitialized = true;
            Debug.Log("BaseInputScheme :: Initializing Control Scheme to Player: P" + _PlayerId);
        }


        public virtual void FixedUpdate()
        {
            _MoveVector.x = _RewiredPlayer.GetAxis(InputActionConstants.MOVE_HORIZONTAL);
            _MoveVector.z = _RewiredPlayer.GetAxis(InputActionConstants.MOVE_VERTICAL);

            _InputBroker.Publish(new MoveCharacterSignal()
            {
                x = _MoveVector.x,
                z = _MoveVector.z
            });
        }

        public GameObject SelfReference()
        {
            return gameObject;
        }
        
        private void CheckKeyboardFunctionality()
        {
            if (_ControllerType == ControllerType.Keyboard)
            {
                foreach (Controller cont in ReInput.controllers.Controllers.ToArray())
                {
                    if (cont.type == ControllerType.Keyboard)
                    {
                        _RewiredPlayer.controllers.AddController(cont.type, cont.id, true);
                        break;
                    }
                }
            }
        }

    }
}