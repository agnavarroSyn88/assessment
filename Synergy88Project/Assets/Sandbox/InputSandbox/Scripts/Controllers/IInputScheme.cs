﻿using Rewired;
using UnityEngine;

public interface IInputScheme
{

    void InitializeControlScheme(int id, string id2);
    GameObject SelfReference();
    ControllerType GetControllerType();
}