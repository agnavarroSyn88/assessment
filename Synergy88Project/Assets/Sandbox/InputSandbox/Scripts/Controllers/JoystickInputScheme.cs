﻿using Common.Query;
using FrameworkInputSystem;
using Rewired;
using Synergy88;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;


namespace Sandbox.ControllerInput
{
    public class JoystickInputScheme : BaseInputScheme
    {
        public override void FixedUpdate()
        {
            if (ReInput.isReady == false)
                return;

            if (_RewiredPlayer == null)
                return;

            if (_IsInitialized == false)
                return;

            base.FixedUpdate();

            if (_RewiredPlayer.GetButtonDown("Attack"))
            {
                _InputBroker.Publish(new ActionSignal());
            }
            if (_RewiredPlayer.GetButtonDown("Cancel"))
            {
                _InputBroker.Publish(new CancelSignal());
            }
        }
    }
}