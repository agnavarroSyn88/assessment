﻿using FullInspector;
using Rewired;
using System;
using UnityEngine;

namespace Sandbox.ControllerInput
{
    [Serializable]
    public struct ControllerData
    {
        [SerializeField, InspectorDisabled]
        private string _ID;
        [SerializeField, InspectorDisabled]
        private Controller _Controller;
        [SerializeField, InspectorDisabled]
        private bool _IsRegistered;
        [SerializeField, InspectorDisabled]
        private int _RegisteredToPlayer;

        public string GetID()
        {
            return _ID;
        }
        public Controller GetController()
        {
            return _Controller;
        }
        public bool GetisRegistered()
        {
            return _IsRegistered;
        }
        public int GetregisteredToPlayer()
        {
            return _RegisteredToPlayer;
        }

        public void BuildControllerData(string id, Controller cont, bool isReg, int regToPlayer)
        {
            _ID = id;
            _Controller = cont;
            _IsRegistered = isReg;
            _RegisteredToPlayer = regToPlayer;
        }
    }
}