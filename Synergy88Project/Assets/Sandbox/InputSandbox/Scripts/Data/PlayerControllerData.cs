﻿using System;
using UnityEngine;

namespace Sandbox.ControllerInput
{
    [Serializable]
    public struct PlayerControllerData
    {
        public int ID;
        public GameObject[] ControllerSchemes;
    }
}