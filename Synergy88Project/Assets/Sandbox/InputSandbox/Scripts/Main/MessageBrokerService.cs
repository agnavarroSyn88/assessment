﻿using Common.Query;
using Sandbox.Character;
using Synergy88;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Sandbox.ControllerInput
{
    public class MessageBrokerService : MonoBehaviour
    {
        [SerializeField]
        private Dictionary<string, IMessageBroker> _Brokers;

        [SerializeField]
        private Dictionary<string, CharacterMovement> _PlayerMovements;

        private void Awake()
        {
            InitializeMessageBroker();
        }

        void InitializeMessageBroker()
        {
            _Brokers = new Dictionary<string, IMessageBroker>();

            QuerySystem.RegisterResolver(QueryIds.MessageBroker, (IQueryRequest request, IMutableQueryResult result) =>
            {
                string _BrokerId = (string)request.GetParameter(QueryIds.MessageBrokerId);

                if (!_Brokers.ContainsKey(_BrokerId))
                {
                    _Brokers.Add(_BrokerId, new MessageBroker());
                }

                result.Set(_Brokers[_BrokerId]);
            });
        }
    }
}
