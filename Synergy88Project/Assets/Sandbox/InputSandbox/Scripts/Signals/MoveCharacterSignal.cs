﻿using UnityEngine;

namespace Sandbox.ControllerInput
{
    public class MoveCharacterSignal
    {
        public float x;
        public float z;
    }
}
