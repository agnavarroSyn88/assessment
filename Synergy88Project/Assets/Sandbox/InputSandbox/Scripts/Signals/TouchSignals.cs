﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.TouchInput
{
    public class PinchSignal
    {
        public float Scale;
    }

    public class PanSignal
    {
        public Vector2 Translation;
        
    }

    public class TouchPadSignal
    {
        public Vector2 TouchPosition;
    }
}
