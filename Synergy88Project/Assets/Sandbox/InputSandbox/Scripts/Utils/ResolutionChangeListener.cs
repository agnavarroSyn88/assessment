﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;
using Sandbox.ScrabbleExam;

public class ResolutionChangeListener: BaseBehavior
{
    [SerializeField]
    private Camera GameCamera;

    [SerializeField]
    private ReactiveProperty<Rect> CameraPixelRect = new ReactiveProperty<Rect>();

    protected override void Awake()
    {
        base.Awake();
        //GameCamera = QuerySystem.Query<Camera>(QueryIds.SystemCamera);

        CameraPixelRect.Value = GameCamera.pixelRect;
        QuerySystem.RegisterResolver(QueryIds.CameraPixelRect, delegate (IQueryRequest request, IMutableQueryResult result)
        {
            result.Set(CameraPixelRect);
        });
    }

    void Start()
    {
        
    }

    void Update()
    {
        CameraPixelRect.Value = GameCamera.pixelRect;
    }

    protected void OnDestroy()
    {

        QuerySystem.RemoveResolver(QueryIds.CameraPixelRect);
    }

}
