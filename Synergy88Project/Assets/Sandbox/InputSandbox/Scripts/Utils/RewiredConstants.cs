﻿/* Rewired Constants
   This list was generated on 6/21/2017 5:09:54 PM
   The list applies to only the Rewired Input Manager from which it was generated.
   If you use a different Rewired Input Manager, you will have to generate a new list.
   If you make changes to the exported items in the Rewired Input Manager, you will need to regenerate this list.
*/

namespace FrameworkInputSystem
{
    public static class InputActionConstants
    {

        public static string MOVE_HORIZONTAL = "MoveHorizontal";
        public static string MOVE_VERTICAL = "MoveVertical";
        
    }
    public static class InputCategory
    {
        public const int Default = 0;
        public const int Movement = 3;
        public const int Fighting = 4;
        public const int Normal_Targetting = 6;
        public const int Skill_Targetting = 2;
        public const int Camera = 5;
        public const int Break_Actions = 7;
        public const int Break_Camera = 8;
        public const int Control_Schemes = 9;
        public const int CharacterSelect = 10;
    }
    public static class InputLayout
    {
        public static class Joystick
        {
            public const int Default = 0;
            public const int InGameScheme1 = 6;
            public const int InGameScheme2 = 1;
            public const int InGameScheme3 = 3;
            public const int InGameScheme4 = 4;
            public const int CharacterSelect = 5;
        }
        public static class Keyboard
        {
            public const int Default = 0;
            public const int InGameScheme1 = 7;
            public const int InGameScheme2 = 4;
            public const int InGameScheme3 = 5;
            public const int InGameScheme4 = 6;
            public const int CharacterSelect = 8;
        }
        public static class Mouse
        {
            public const int Default = 0;
            public const int InGameScheme1 = 4;
            public const int InGameScheme2 = 1;
            public const int InGameScheme3 = 2;
            public const int InGameScheme4 = 3;
            public const int CharacterSelect = 5;
        }
        public static class CustomController
        {
            public const int Default = 0;
        }
    }
}
