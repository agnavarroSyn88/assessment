﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;

using uPromise;

using UniRx;

using FullInspector;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;
using Synergy88;
using FrameworkInputSystem;
using Common.Utils;
using Sandbox.Character;
using Sandbox.ControllerInput;
using Sandbox.MovementDebug;
using Sandbox.Spawner;
using Sandbox.Playables;

public class CharacterInstaller : ConcreteInstaller, IInstaller
    {

        private Scene _Scene;
        private IMessageBroker _Signal;

        protected override void Awake()
        {

        }

        public override void Add()
        {

        }

        public override void Install()
        {
            _Scene = Scene.GetScene<SystemRoot>(EScene.System);

            Promise.AllSequentially(_Scene.EndFramePromise)
                .Then(_ => _Scene.LoadSceneAdditivePromise<InputSandboxRoot>(EScene.InputSandboxRoot))
                .Then(_ =>
                {
                    Factory.Get<PlayerInputService>().InitalizeControllers();

                })
                .Then(_ => _Scene.LoadSceneAdditivePromise<CharacterSandboxRoot>(EScene.CharacterSandboxRoot))
                .Then(_ =>
                {
                    CharacterSandboxRoot _CharRoot = Scene.GetScene<CharacterSandboxRoot>(EScene.CharacterSandboxRoot);
                    _CharRoot.GetMsgBroker.Receive<RequestSignalInjectionSignal>().Subscribe(x =>
                    {
                        RequestSignalInjection(x.PlayerID);
                    });
                })
                .Then(_ => _Scene.LoadSceneAdditivePromise<SpawnSandboxRoot>(EScene.SpawnSandboxRoot))
                .Then(_ =>
                {
                    CharacterSandboxRoot _CharacterScene = Scene.GetScene<CharacterSandboxRoot>(EScene.CharacterSandboxRoot);

                    SpawnerPoolService _Spawner = Factory.Get<SpawnerPoolService>();

                    _Spawner.GetMsgBroker.Receive<RequestCharacterSpawnSignal>().Subscribe(x =>
                    {
                        _CharacterScene.SpawnCharacter(x.SpawnData);
                    });
                })
                .Then(_ => _Scene.LoadSceneAdditivePromise<MovementDebugSandboxRoot>(EScene.MovementDebugSandboxRoot))
                .Then(_ =>
                {

                    MovementDebugSandboxRoot _MoveDebugRoot = Scene.GetScene<MovementDebugSandboxRoot>(EScene.MovementDebugSandboxRoot);
                    MovementStats _MoveSTats = _MoveDebugRoot.MovementStats;
                    DebugButtonTemplate _ButtonTemplate = null;


                    _ButtonTemplate = _MoveDebugRoot.GenerateInputTemplates("RotationSpeed", _MoveSTats.RotationSpeed);
                    _ButtonTemplate.MessageBroker.Receive<UpdateDebugMoveSignal>().Subscribe(x =>
                    {
                        Debug.LogError("Received Signal: " + x.Val);
                        _MoveSTats.RotationSpeed += float.Parse( x.Val );
                        _MoveDebugRoot.DebugButtonList[x.ID].Text.text = x.VarName + ": " + _MoveSTats.RotationSpeed.ToString();
                    });

                    _ButtonTemplate = _MoveDebugRoot.GenerateInputTemplates("SnappingAngleThreshold", _MoveSTats.SnappingAngleThreshold);
                    _ButtonTemplate.MessageBroker.Receive<UpdateDebugMoveSignal>().Subscribe(x =>
                    {
                        _MoveSTats.SnappingAngleThreshold += float.Parse(x.Val);
                        _MoveDebugRoot.DebugButtonList[x.ID].Text.text = x.VarName + ": " + _MoveSTats.SnappingAngleThreshold.ToString();
                    });

                    _ButtonTemplate = _MoveDebugRoot.GenerateInputTemplates("TestString", _MoveSTats.TestString);
                    _ButtonTemplate.MessageBroker.Receive<UpdateDebugMoveSignal>().Subscribe(x =>
                    {
                        _MoveSTats.TestString = (string)(x.Val);
                        _MoveDebugRoot.DebugButtonList[x.ID].Text.text = x.VarName+": " + _MoveSTats.TestString.ToString();
                    });

                    _ButtonTemplate = _MoveDebugRoot.GenerateInputTemplates("TestVEctor3", _MoveSTats.TestVEctor3);
                    _ButtonTemplate.MessageBroker.Receive<UpdateDebugMoveSignal>().Subscribe(x =>
                    {
                        _MoveSTats.TestVEctor3 = StringToVector3(x.Val);
                        _MoveDebugRoot.DebugButtonList[x.ID].Text.text = x.VarName + ": " + _MoveSTats.TestVEctor3.ToString();
                    });
                    _ButtonTemplate = _MoveDebugRoot.GenerateInputTemplates("TestQuaternion", _MoveSTats.TestQuaternion);
                    _ButtonTemplate.MessageBroker.Receive<UpdateDebugMoveSignal>().Subscribe(x =>
                    {
                        _MoveSTats.TestQuaternion = new Quaternion( StringToVector3(x.Val).x, StringToVector3(x.Val).y, StringToVector3(x.Val).z,1);
                        _MoveDebugRoot.DebugButtonList[x.ID].Text.text = x.VarName + ": " + _MoveSTats.TestQuaternion.ToString();
                    });

                    _ButtonTemplate = _MoveDebugRoot.GenerateInputTemplates("InvertInputAxisWhenDefending", _MoveSTats.InvertInputAxisWhenDefending);
                    _ButtonTemplate.MessageBroker.Receive<UpdateDebugMoveSignal>().Subscribe(x =>
                    {
                        _MoveSTats.InvertInputAxisWhenDefending = !_MoveSTats.InvertInputAxisWhenDefending;
                        _MoveDebugRoot.DebugButtonList[x.ID].Text.text = x.VarName + ": " + _MoveSTats.InvertInputAxisWhenDefending.ToString();
                    });
                })
                ;
        }

        public override void UnInstall()
        {

        }

        private void RequestSignalInjection(string playerId)
        {

            CharacterPoolService _Charpool = Factory.Get<CharacterPoolService>();

            IQueryRequest _Query = QuerySystem.Start(QueryIds.MessageBroker);
            _Query.AddParameter(QueryIds.MessageBrokerId, "P" + playerId.ToString());
            IMessageBroker _InputBroker = QuerySystem.Complete<IMessageBroker>();

            Debug.Log("CharacterInstaller :: Assigning Move Signal to: P"+playerId);
            _InputBroker.Receive<MoveCharacterSignal>().Subscribe(__ =>
            {
                _Charpool.FetchCharacterFromList("P" + playerId.ToString()).GetComponent<CharacterMovement>().Move(__.x,__.z, false);
            });
            _InputBroker.Receive<DisableInputSandboxSignal>().Subscribe(__ =>
            {
                _Charpool.FetchCharacterFromList("P" + playerId.ToString()).GetComponent<CharacterMovement>().SetBLockMovement(__.BlockMovement);
            });
            _InputBroker.Receive<ActionSignal>().Subscribe(__ =>
            {
                _Charpool.FetchCharacterFromList("P" + playerId.ToString()).GetComponent<CharacterAnimPlayables>().ReceiveActionSignal();
            });
            _InputBroker.Receive<CancelSignal>().Subscribe(__ =>
            {
                _Charpool.FetchCharacterFromList("P" + playerId.ToString()).GetComponent<CharacterAnimPlayables>().ReceiveCancelSignal();
            });
        }
    


    public static Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith("(") && sVector.EndsWith(")"))
        {
            sVector = sVector.Substring(1, sVector.Length - 2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }
}