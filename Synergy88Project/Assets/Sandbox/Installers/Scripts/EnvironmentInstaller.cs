﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;

using uPromise;

using UniRx;

using FullInspector;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;
using Synergy88;
using FrameworkInputSystem;
using Common.Utils;
using Sandbox.Character;
using Sandbox.ControllerInput;
using Sandbox.MovementDebug;

public class EnvironmentInstaller : ConcreteInstaller, IInstaller
{

    private Scene _Scene;
    private IMessageBroker _Signal;
    
    public override void Install()
    {
        _Scene = Scene.GetScene<SystemRoot>(EScene.System);

        Promise.AllSequentially(_Scene.EndFramePromise)
            .Then(_ => _Scene.LoadSceneAdditivePromise<EnvironmentGroundRoot>(EScene.EnvironmentGroundRoot))
            .Then(_ => _Scene.LoadSceneAdditivePromise<EnvironmentMeshRoot>(EScene.EnvironmentMeshRoot))
            .Then(_ => _Scene.LoadSceneAdditivePromise<EnvironmentCollisionRoot>(EScene.EnvironmentCollisionRoot))
            .Then(_ => _Scene.LoadSceneAdditivePromise<EnvironmentSandboxRoot>(EScene.EnvironmentSandboxRoot))
            .Then(_ =>
            {
                EnvironmentSandboxRoot _EnvironmentSandboxRoot = Scene.GetScene<EnvironmentSandboxRoot>(EScene.EnvironmentSandboxRoot);

                EnvironmentGroundRoot _EnvironmentGroundRoot = Scene.GetScene<EnvironmentGroundRoot>(EScene.EnvironmentGroundRoot);
                EnvironmentMeshRoot _EnvironmentMeshRoot = Scene.GetScene<EnvironmentMeshRoot>(EScene.EnvironmentMeshRoot);
                EnvironmentCollisionRoot _EnvironmentCollisionRoot = Scene.GetScene<EnvironmentCollisionRoot>(EScene.EnvironmentCollisionRoot);

                _EnvironmentSandboxRoot.InjectSceneObject(_EnvironmentGroundRoot.GetSceneParent);
                _EnvironmentSandboxRoot.InjectSceneObject(_EnvironmentMeshRoot.GetSceneParent);
                _EnvironmentSandboxRoot.InjectSceneObject(_EnvironmentCollisionRoot.GetSceneParent);
            })
            .Then(_ =>
            {
                Scene.UnloadScene(EScene.EnvironmentGroundRoot);
                Scene.UnloadScene(EScene.EnvironmentMeshRoot);
                Scene.UnloadScene(EScene.EnvironmentCollisionRoot);
            });
    }
    
    private void RequestSignalInjection(string playerId)
    {
        CharacterPoolService _CharPool = Factory.Get<CharacterPoolService>();

        IQueryRequest _Query = QuerySystem.Start(QueryIds.MessageBroker);
        _Query.AddParameter(QueryIds.MessageBrokerId, "P" + playerId.ToString());
        IMessageBroker _InputBroker = QuerySystem.Complete<IMessageBroker>();

        Debug.Log("CharacterInstaller :: Assigning Move Signal to: P"+playerId);
        _InputBroker.Receive<MoveCharacterSignal>().Subscribe(__ =>
        {
            _CharPool.FetchCharacterFromList("P" + playerId.ToString()).GetComponent<CharacterMovement>().Move(__.x,__.z, false);
        });
    }


    public static Vector3 StringToVector3(string stringVector)
    {
        // Remove the parentheses
        if (stringVector.StartsWith("(") && stringVector.EndsWith(")"))
        {
            stringVector = stringVector.Substring(1, stringVector.Length - 2);
        }

        // split the items
        string[] sArray = stringVector.Split(',');

        // store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(sArray[0]),
            float.Parse(sArray[1]),
            float.Parse(sArray[2]));

        return result;
    }
}