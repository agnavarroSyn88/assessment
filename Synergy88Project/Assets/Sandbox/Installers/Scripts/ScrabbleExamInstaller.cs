﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using uPromise;

using UniRx;

using FullInspector;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;
using Synergy88;
using FrameworkInputSystem;
using Common.Utils;
using Sandbox.ScrabbleExam;

public class ScrabbleExamInstaller : ConcreteInstaller, IInstaller
{
    private Scene _Scene;

    protected override void Awake()
    {

    }

    public override void Add()
    {

    }

    public override void Install()
    {

        _Scene = Scene.GetScene<SystemRoot>(EScene.System);


        Promise.AllSequentially(_Scene.EndFramePromise)
            .Then(_ => _Scene.LoadSceneAdditivePromise<ScrabbleExamServicesRoot>(EScene.ScrabbleExamServicesRoot))
            .Then(_ => _Scene.LoadSceneAdditivePromise<ScrabbleExamSandboxRoot>(EScene.ScrabbleExamSandboxRoot))
            .Then(_ => _Scene.LoadSceneAdditivePromise<ScrabbleExamUIRoot>(EScene.ScrabbleExamUIRoot));
    }
}
