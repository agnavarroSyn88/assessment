﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using uPromise;

using UniRx;

using FullInspector;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;
using Synergy88;
using FrameworkInputSystem;
using Common.Utils;
using Sandbox.ScrabbleExam;

public class ScrabbleExamSceneFlowInstaller : ConcreteInstaller, IInstaller
{
    // initial state
    private const string IDLE = "Idle";

    // events
    private const string START_SPLASH = "StartSplash";
    private const string START_PRELOAD = "StartPreload";
    private const string LOAD_HOME = "LoadHome";
    private const string LOAD_GAME = "LoadGame";
    private const string FINISHED = "Finished";

    private Scene Scene;
    private PreloaderRoot Preloader;
    private PopupCollectionRoot PopupCollection;

    // Fsm
    private Fsm Fsm;

    [SerializeField]
    private EScene _CurrentScene = EScene.Invalid;
    public EScene CurrentScene
    {
        get
        {
            return _CurrentScene;
        }
        private set
        {
            _CurrentScene = value;
        }
    }

    [SerializeField]
    private EScene _PreviousScene = EScene.Invalid;
    public EScene PreviousScene
    {
        get
        {
            return _PreviousScene;
        }
        private set
        {
            _PreviousScene = value;
        }
    }

    private void OnDestroy()
    {
        QuerySystem.RemoveResolver(QueryIds.CurrentScene);
        QuerySystem.RemoveResolver(QueryIds.PreviousScene);
        QuerySystem.RemoveResolver(QueryIds.SystemState);
        QuerySystem.RemoveResolver(QueryIds.Preloader);
        QuerySystem.RemoveResolver(QueryIds.PopupCollection);
    }

    public override void Add()
    {

    }

    public override void Install()
    {
        Scene = Scene.GetScene<SystemRoot>(EScene.System);

        SetupQueries();
        SetupListeners();
        PrepareSceneFsm();
    }

    private void SetupQueries()
    {
        QuerySystem.RegisterResolver(QueryIds.CurrentScene, delegate (IQueryRequest request, IMutableQueryResult result)
        {
            result.Set(CurrentScene);
        });

        QuerySystem.RegisterResolver(QueryIds.PreviousScene, delegate (IQueryRequest request, IMutableQueryResult result)
        {
            result.Set(PreviousScene);
        });

        QuerySystem.RegisterResolver(QueryIds.SystemState, delegate (IQueryRequest request, IMutableQueryResult result)
        {
            result.Set(Fsm.GetCurrentState());
        });

        QuerySystem.RegisterResolver(QueryIds.Preloader, delegate (IQueryRequest request, IMutableQueryResult result)
        {
            result.Set(Preloader);
        });

        QuerySystem.RegisterResolver(QueryIds.PopupCollection, delegate (IQueryRequest request, IMutableQueryResult result)
        {
            result.Set(PopupCollection);
        });
    }

    private void SetupListeners()
    {
        // Setup Listeners
        this.Receive<OnLoadSceneSignal>()
            .Subscribe(sig => OnLoadScene(sig.SceneName))
            .AddTo(this);

        this.Receive<LoadSplashSignal>()
            .Subscribe(_ => OnLoadSplash())
            .AddTo(this);

        this.Receive<SplashDoneSignal>()
            .Subscribe(_ => OnSplashDone())
            .AddTo(this);


        this.Receive<LoadHomeSignal>()
            .Subscribe(_ => OnLoadHome())
            .AddTo(this);


        this.Receive<LoadGameSignal>()
            .Subscribe(_ => OnLoadGame())
            .AddTo(this);
    }

    #region Signals

    /// <summary>
    /// Records the currently and previously loaded scenes.
    /// Does not track Invalid and Cleaner scenes.
    /// </summary>
    private void OnLoadScene(EScene sceneName)
    {
        Debug.LogFormat("{0} OnLoadScene: {1}", Time.time, sceneName);

        if (CurrentScene == EScene.Invalid)
        {
            CurrentScene = sceneName;
        }
        else if (sceneName != EScene.Invalid && sceneName != EScene.Cleaner)
        {
            PreviousScene = CurrentScene;
            CurrentScene = sceneName;
        }
    }

    private void OnLoadSplash()
    {
        Fsm.SendEvent(START_SPLASH);
    }

    private void OnSplashDone()
    {
        Fsm.SendEvent(START_PRELOAD);
    }

    private void OnLoadHome()
    {
        Fsm.SendEvent(LOAD_HOME);
    }


    private void OnLoadGame()
    {
        Fsm.SendEvent(LOAD_GAME);
    }

    #endregion

    #region Scene Flow Fsm
    private void PrepareSceneFsm()
    {
        Fsm = new Fsm("SceneFsm");

        // states
        FsmState idle = Fsm.AddState(IDLE);
        FsmState splash = Fsm.AddState("splash");
        FsmState preload = Fsm.AddState("preload");
        FsmState home = Fsm.AddState("home");
        FsmState game = Fsm.AddState("game");
        FsmState done = Fsm.AddState("done");

        // actions
        idle.AddAction(new FsmDelegateAction(idle, delegate (FsmState owner)
        {
            // Start with splash
            this.Publish(new LoadSplashSignal());
        }));

        splash.AddAction(new FsmDelegateAction(splash, delegate (FsmState owner)
        {
            Promise.AllSequentially(Scene.EndFramePromise)
                .Then(_ => Scene.LoadScenePromise<PreloaderRoot>(EScene.Preloader))
                .Then(_ => Scene.LoadScenePromise<PopupCollectionRoot>(EScene.PopupCollection))
                .Then(_ => PopupCollection = Scene.GetScene<PopupCollectionRoot>(EScene.PopupCollection))
                .Then(_ => Preloader = Scene.GetScene<PreloaderRoot>(EScene.Preloader))
                .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading001 | LoadingImages.Loading002 | LoadingImages.Loading003 | LoadingImages.Loading004))
                .Then(_ => Preloader.FadeInLoadingScreenPromise())
                .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                .Then(_ => Scene.LoadSceneAdditivePromise<AudioRoot>(EScene.Audio))
                .Then(_ => Scene.LoadSceneAdditivePromise<SplashRoot>(EScene.Splash))
                .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                .Then(_ => Scene.EndFramePromise());
        }));

        preload.AddAction(new FsmDelegateAction(preload, delegate (FsmState owner)
        {
            // load preloader here
            Scene.LoadScenePromise<ScrabbleExamServicesRoot>(EScene.ScrabbleExamServicesRoot);
        }));


        home.AddAction(new FsmDelegateAction(home, delegate (FsmState owner)
        {
            Promise.AllSequentially(Scene.EndFramePromise)
                .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading002))
                .Then(_ => Preloader.FadeInLoadingScreenPromise())
                .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                .Then(_ => Scene.LoadScenePromise<ScrabbleExamSandboxTitleRoot>(EScene.ScrabbleExamSandboxTitleRoot))
                .Then(_ => Scene.WaitPromise(1f))
                .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                .Then(_ => Scene.EndFramePromise());
        }));



        game.AddAction(new FsmDelegateAction(game, delegate (FsmState owner)
        {
            Promise.AllSequentially(Scene.EndFramePromise)
                .Then(_ => Preloader.LoadLoadingScreenPromise())
                .Then(_ => Preloader.FadeInLoadingScreenPromise())
                .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                .Then(_ => Scene.LoadSceneAdditivePromise<ScrabbleExamSandboxRoot>(EScene.ScrabbleExamSandboxRoot))
                .Then(_ => Scene.LoadSceneAdditivePromise<ScrabbleExamUIRoot>(EScene.ScrabbleExamUIRoot))
                .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                .Then(_ => Scene.EndFramePromise());
        }));

        done.AddAction(new FsmDelegateAction(done, delegate (FsmState owner)
        {
        }));

        // transitions
        idle.AddTransition(START_SPLASH, splash);

        splash.AddTransition(START_PRELOAD, preload);

        preload.AddTransition(LOAD_HOME, home);



        home.AddTransition(LOAD_GAME, game);

        game.AddTransition(LOAD_HOME, home);

        // auto start fsm
        Fsm.Start(IDLE);
    }

    #endregion
}
