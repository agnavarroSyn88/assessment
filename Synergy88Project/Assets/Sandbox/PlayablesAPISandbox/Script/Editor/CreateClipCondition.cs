﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Sandbox.Playables
{
    public class CreateClipCondition
    {

        [MenuItem("Assets/Create/ClipWindowCondition")]
        public static void CreateClipEventConditionData()
        {
            ScriptableObjectUtility.CreateAsset<ClipWindowTimeCondition>();
        }
        [MenuItem("Assets/Create/ClipKeyCondition")]
        public static void CreateClipEventConditionData2()
        {
            ScriptableObjectUtility.CreateAsset<ClipKeyCondition>();
        }
        [MenuItem("Assets/Create/ClipBoolCondition")]
        public static void CreateClipEventConditionData3()
        {
            ScriptableObjectUtility.CreateAsset<ClipBoolCondition>();
        }
    }
}