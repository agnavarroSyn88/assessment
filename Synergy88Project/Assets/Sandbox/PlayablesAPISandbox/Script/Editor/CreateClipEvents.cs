﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Sandbox.Playables
{
    public class CreateClipEvents
    {

        [MenuItem("Assets/Create/ClipEventData")]
        public static void CreateClipEventData()
        {
            ScriptableObjectUtility.CreateAsset<ClipEventData>();
        }
    }
}