﻿using FullInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Animations;
using UnityEngine.Playables;
using FullInspector;
using System;
using UniRx;
using Common.Query;
using Synergy88;
using Sandbox.Character;
using Sandbox.ControllerInput;

namespace Sandbox.Playables
{
    public partial class CharacterAnimPlayables : BaseBehavior
    {
        enum AnimState
        {
            Idle,
            Interacting
        }

        enum BLENDTYPE
        {
            MecanimToClip,
            ClipToClip,
            ClipToMecanim
        }
        public void ReceiveActionSignal()
        {
            TriggerAnimationClip(_ClipEventDataList[0]);
        }
        public void ReceiveCancelSignal()
        {
            if (_CanCancelMovement)
            {
                EndAnim();
            }
        }
        //VARIABLES
        //====================================================================
        #region VARIABLES
            
        //GENERATED VARIABLES
        //FLAGS
        private bool _IfConditionsMet = false;
        private bool _CanInitQueue = true;
        private bool _CanProceedQueue = false;
        private bool _CanCancelMovement = false;

        //GENERATED DATA
        [Header("Generated Data")]
        [SerializeField, InspectorDisabled]
        private AnimationClip _CurrentAnimClip;
        [SerializeField, InspectorDisabled]
        private AnimState _CurrentState;
        [SerializeField, InspectorDisabled]
        private ClipEventData
            _CurrentClipEvent,
            _QueueClipEvent;
        private List<ClipEventData> _ClipDataQueueList = new List<ClipEventData>();
        private ClipEntry _CurrentClipBranch;
        private IMessageBroker _InputBroker;

        //COMPONENTS
        [Header("Required Data")]
        [SerializeField]
        private GameObject _AnimatedObject;
        [SerializeField]
        private List<ClipEventData> _ClipEventDataList;
        [SerializeField]
        private RuntimeAnimatorController _AnimController;
        
        //PLAYABLES API RELATED
        private PlayableGraph _PlayableGraph;
        private AnimationMixerPlayable _MixerPlayable;
        private AnimationClipPlayable _ClipControl;
        private AnimatorControllerPlayable _AnimatorControl;
        private AnimationPlayableOutput _PlayableOutput;
         
        //BLENDING AND INTERPOLATION
        TypeOfInterpolation interpolType;
        float _LerpTime = 0;
        float _BlendWeight;
        [SerializeField]
        private float _LerpSpeed = 1f;

        [SerializeField]
        KeyCode _LatestKeyPress;
        #endregion
        //====================================================================
        //INITIALIZATION
        //====================================================================
        #region INITIALIZATION
        void Start()
        {
            _CurrentState = AnimState.Idle;

            //INIT PLAYABLES API
            _PlayableGraph = PlayableGraph.Create();
            _PlayableOutput = AnimationPlayableOutput.Create(_PlayableGraph, "Animation", _AnimatedObject.GetComponent<Animator>());
            _MixerPlayable = AnimationMixerPlayable.Create(_PlayableGraph, 3);
            _PlayableOutput.SetSourcePlayable(_MixerPlayable);
            
            _ClipControl = AnimationClipPlayable.Create(_PlayableGraph, null);
            _AnimatorControl = AnimatorControllerPlayable.Create(_PlayableGraph, _AnimController);

            //INIT QUERY
            IQueryRequest query = QuerySystem.Start(QueryIds.MessageBroker);
            query.AddParameter(QueryIds.MessageBrokerId, "P" + GetComponent<CharacterMovement>().GetPlayerID().ToString());
            _InputBroker = QuerySystem.Complete<IMessageBroker>();
        }
        #endregion
        //====================================================================
        //UPDATES
        //====================================================================
        #region UPDATES
        void FixedUpdate()
        {
            ProcessQueue();
  
            UpdateInterpolation();
            WaitForAnimationEnd();
            ProcessWeightDistribution();
        }
        void ProcessQueue()
        {
            if (_CanInitQueue == true)
            {
                if (_ClipDataQueueList.Count > 0)
                {
                    _CurrentClipEvent = _ClipDataQueueList[0];
                    ProcessEventData(_CurrentClipEvent);
                    CheckAllConditions(_ClipDataQueueList[0]);
                    _ClipDataQueueList.RemoveAt(0);
                    _CanInitQueue = false;
                }
            }
        }
        void WaitForAnimationEnd()
        {
            if (_CurrentState == AnimState.Idle)
                return;
            if (CheckIfEndOfAnimation())
            {
                if (_CanProceedQueue == true)
                {
                    InstantPlayAnim();
                }
                else if (_CanProceedQueue == false)
                {
                    EndAnim();
                }
            }
        }
        void ProcessWeightDistribution()
        {
            _BlendWeight = Mathf.Clamp01(_BlendWeight);
            _MixerPlayable.SetInputWeight(1, _BlendWeight);
            _MixerPlayable.SetInputWeight(0, 1.0f - _BlendWeight);
        }
        #endregion
        //====================================================================
        //TRIGGERED FUNCTIONS
        //====================================================================
        #region TRIGGERED FUNCTIONS
        public void TriggerAnimationClip(ClipEventData clipEvenetData)
        {
            _ClipDataQueueList.Add(clipEvenetData);
            if (_CanInitQueue == false)
            {
                if (_ClipDataQueueList.Count > 0)
                {
                    try
                    {
                        _QueueClipEvent = _CurrentClipBranch.Output;
                    }
                    catch
                    {
                        _QueueClipEvent = _ClipDataQueueList[0];
                    }
                    _ClipDataQueueList.RemoveAt(0);
                    CheckAllConditions(_QueueClipEvent);
                }
            }
        }
        
        private void ProcessEventData(ClipEventData clipEvenetData)
        {
            if (_CurrentState == AnimState.Idle)
            {
                _CurrentState = AnimState.Interacting;
                ConnectClips(BLENDTYPE.MecanimToClip, clipEvenetData.Clip);
            }
            else if (_CurrentState == AnimState.Interacting)
            {
                ConnectClips(BLENDTYPE.ClipToClip, clipEvenetData.Clip);
            }
        }

        private void ConnectClips(BLENDTYPE blendType, AnimationClip clip)
        {
            //HANDLES THE DISCONNECTION AND CONNECTION OF TWO CLIPS AND BLENDING THEM TOGETHER WHICH IS AFFECTED BY "_BlendWeight"
            double cachedAnimFrame = 0;
            _PlayableGraph.Disconnect(_MixerPlayable, 0);
            _PlayableGraph.Disconnect(_MixerPlayable, 1);

            switch (blendType)
            {
                case BLENDTYPE.MecanimToClip:
                    {
                        _ClipControl = AnimationClipPlayable.Create(_PlayableGraph, clip);
                        _PlayableGraph.Connect(_AnimatorControl, 0, _MixerPlayable, 0);
                        TriggerInterpolation(TypeOfInterpolation.Positive);
                    }
                    break;
                case BLENDTYPE.ClipToClip:
                    {
                        cachedAnimFrame = _ClipControl.GetTime();

                        AnimationClipPlayable previousClip = AnimationClipPlayable.Create(_PlayableGraph, _CurrentAnimClip);
                        _ClipControl = AnimationClipPlayable.Create(_PlayableGraph, clip);
                        previousClip.SetSpeed(2);
                        _PlayableGraph.Connect(previousClip, 0, _MixerPlayable, 0);

                        _BlendWeight = 0;

                        TriggerInterpolation(TypeOfInterpolation.Positive);
                        previousClip.SetTime(cachedAnimFrame);
                        _ClipControl.SetTime(cachedAnimFrame);
                    }
                    break;
                case BLENDTYPE.ClipToMecanim:
                    {
                        cachedAnimFrame = _ClipControl.GetTime();
                        _ClipControl = AnimationClipPlayable.Create(_PlayableGraph, _CurrentAnimClip);

                        _PlayableGraph.Connect(_AnimatorControl, 0, _MixerPlayable, 0);

                        TriggerInterpolation(TypeOfInterpolation.Negative);
                        _ClipControl.SetTime(cachedAnimFrame);
                    }
                    break;
            }
            _CurrentClipEvent = null;
            _PlayableGraph.Connect(_ClipControl, 0, _MixerPlayable, 1);
            _CurrentAnimClip = clip;
            _LerpTime = 0;
            _PlayableGraph.Play();
        }

        private void CheckAllConditions(ClipEventData clipEntryBranch)
        {

            float normalizedTime = GetNormalizedTime();
            int _branchCount = clipEntryBranch.Branches.Count;
            _IfConditionsMet = true;

            bool ifBlockMovement = false;
            bool ifInstantAnim = false;
            for (int _branchIndex = 0; _branchIndex < _branchCount; _branchIndex++)
            {
                ClipEntry tempcurrentClipEntry = clipEntryBranch.Branches[_branchIndex];
                int _conditionCount = tempcurrentClipEntry.Conditions.Count;
                for (int _conditionIndex = 0; _conditionIndex < _conditionCount; _conditionIndex++)
                {
                    IClipCondition currentCondition = clipEntryBranch.Branches[_branchIndex].Conditions[_conditionIndex];
                    if (Map.ContainsKey(currentCondition))
                    {
                        EventParam_ param = new EventParam_();
                        param.Entry = tempcurrentClipEntry;
                        param.Clip = currentCondition;
                        param.Data = new Dictionary<string, object>();
                        param.Data[ConditionKeys.NORMALIZED_TIME] = normalizedTime;
                        param.Data[ConditionKeys.LATEST_KEY_PRESS] = _LatestKeyPress;

                        param.Data[ConditionKeys.BOOL_LIST] = _IfConditionsMet;
                        param.Data[ConditionKeys.IF_INSTANT_ANIM] = ifInstantAnim;
                        param.Data[ConditionKeys.IF_BLOCK_MOVEMENT] = ifBlockMovement;
                        param.Data[ConditionKeys.IF_CANCEL_MOVEMENT] = _CanCancelMovement;
                        Map[currentCondition].Invoke(param);

                        _IfConditionsMet = (bool)param.Data[ConditionKeys.BOOL_LIST];
                        ifBlockMovement = (bool)param.Data[ConditionKeys.IF_BLOCK_MOVEMENT];
                        _CanCancelMovement = (bool)param.Data[ConditionKeys.IF_CANCEL_MOVEMENT];
                        ifInstantAnim = (bool)param.Data[ConditionKeys.IF_INSTANT_ANIM];

                        _InputBroker.Publish(new DisableInputSandboxSignal
                        {
                            BlockMovement = ifBlockMovement
                        });
                        if (_IfConditionsMet)
                        {
                            _CanProceedQueue = true;
                            _CurrentClipBranch = tempcurrentClipEntry;
                            if (ifInstantAnim)
                            {
                                InstantPlayAnim();
                            }
                        }
                        else
                        {
                            _CanProceedQueue = false;
                            _CurrentClipBranch = null;
                            _QueueClipEvent = null;
                        }
                    }
                }
            }
        }
        
        private void EndAnim()
        {
            _CanCancelMovement = false;
            _CanInitQueue = true;
            _CurrentClipEvent = null;
            _QueueClipEvent = null;
            _CurrentClipBranch = null;

            ConnectClips(BLENDTYPE.ClipToMecanim, null);
            _CurrentState = AnimState.Idle;
            _InputBroker.Publish(new DisableInputSandboxSignal
            {
                BlockMovement = false
            });
        }

        private void InstantPlayAnim()
        {
            _CurrentClipEvent = _CurrentClipBranch.Output;
            _QueueClipEvent = null;
            _CanProceedQueue = false;
            ProcessEventData(_CurrentClipEvent);
            _ClipControl.SetTime(0);
        }

        #endregion
        //====================================================================
        //INTERPOLATION
        //====================================================================
        #region INTERPOLATION
        enum TypeOfInterpolation
        {
            None,
            Positive,
            Negative
        }
        void TriggerInterpolation(TypeOfInterpolation val)
        {
            interpolType = val;
        }

        void UpdateInterpolation()
        {
            if (interpolType == TypeOfInterpolation.None)
                return;

            _LerpTime += (_LerpSpeed * Time.deltaTime);
            if (interpolType == TypeOfInterpolation.Positive)
                _BlendWeight = Mathf.Lerp(0, 1, _LerpTime);

            if (interpolType == TypeOfInterpolation.Negative)
                _BlendWeight = Mathf.Lerp(0, 1, 1 - _LerpTime);


            if (_LerpTime >= 1)
            {
                _LerpTime = 0;
                interpolType = TypeOfInterpolation.None;
            }
        }
        #endregion
        //====================================================================
        #region EVENT HANDLERS
        [SerializeField]
        public Dictionary<IClipCondition, UnityEvent<EventParam_>> Map;

        public void HandleTimeMatch(EventParam_ param)
        {
            float nTime = (float)param.Data[ConditionKeys.NORMALIZED_TIME];
            param.Data[ConditionKeys.BOOL_LIST] = param.Clip.CheckCondition(nTime)  ;
        }

        public void HandleKeyMatch(EventParam_ param)
        {
            KeyCode key = (KeyCode)param.Data[ConditionKeys.LATEST_KEY_PRESS];

            param.Data[ConditionKeys.BOOL_LIST] = param.Clip.CheckCondition(key) == true;
        }

        public void HandleFinishAnim(EventParam_ param)
        {
            bool key = (bool)param.Data[ConditionKeys.IF_INSTANT_ANIM];

            param.Data[ConditionKeys.IF_INSTANT_ANIM] = param.Clip.GetRawData();
        }
        public void HandleCancelBool(EventParam_ param)
        {
            bool key = (bool)param.Data[ConditionKeys.IF_CANCEL_MOVEMENT];
            param.Data[ConditionKeys.IF_CANCEL_MOVEMENT] = param.Clip.GetRawData();
        }
        public void HandleMoveBlock(EventParam_ param)
        {
            bool key = (bool)param.Data[ConditionKeys.IF_BLOCK_MOVEMENT];
            param.Data[ConditionKeys.IF_BLOCK_MOVEMENT] = param.Clip.GetRawData();
        }
        #endregion
        //====================================================================


        float GetNormalizedTime()
        {
            return ((float)_ClipControl.GetTime()) / _ClipControl.GetAnimationClip().length;
        }

        bool CheckIfEndOfAnimation()
        {
            float normalizedTime = GetNormalizedTime();
            if (normalizedTime > 1)
            {
                return true;
            }
            return false;
        }
        [Serializable]
        public struct EventParam_
        {
            public ClipEntry Entry;
            public IClipCondition Clip;
            public Dictionary<string, object> Data;
        }
    }
    #region DATA STRUCT
    public static class ConditionKeys
    {

        public static string NORMALIZED_TIME = "normalizedTime";
        public static string LATEST_KEY_PRESS = "latestKeyPress";
        public static string BOOL_LIST = "boolList";
        public static string IF_INSTANT_ANIM = "ifInstantAnim";
        public static string IF_CANCEL_MOVEMENT = "IfCancelMovement";
        public static string IF_BLOCK_MOVEMENT = "IfBlockMovement";

    }
    #endregion
}