﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.Playables
{
    public class OverrideAnimatorCollider : MonoBehaviour
    {

        [SerializeField]
        private ClipEventData clip;

        bool interacted;
        private void OnTriggerEnter(Collider other)
        {if(!interacted)
            if (other.GetComponent<CharacterAnimPlayables>())
            {
                CharacterAnimPlayables animPlayables = other.GetComponent<CharacterAnimPlayables>();
                    animPlayables.TriggerAnimationClip(clip);
                    StartCoroutine(waiter());
            }
        }

        IEnumerator waiter()
        {
            interacted = true;
            yield return new WaitForSeconds(1f);
            interacted = false;
        }
    }
}
