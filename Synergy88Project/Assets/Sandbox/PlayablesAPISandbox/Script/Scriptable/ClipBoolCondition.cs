﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.Playables
{
    public class ClipBoolCondition : IClipCondition
    {
        public bool Key;
        public override bool CheckCondition<T>(T var)
        {
            bool curKey = (bool)((object)var);
            if (curKey == Key)
                return true;
            return false;
        }

        public override object GetRawData()
        {
            return Key;
        }
    }
}