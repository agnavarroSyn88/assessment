﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using System;


namespace Sandbox.Playables
{
    public class ClipEntry
    {
        public List<IClipCondition> Conditions;
        public ClipEventData Output;
    }

    [Serializable]
    public class ClipEventData : BaseScriptableObject
    {

        public AnimationClip Clip;
        public List<ClipEntry> Branches;
    }
}