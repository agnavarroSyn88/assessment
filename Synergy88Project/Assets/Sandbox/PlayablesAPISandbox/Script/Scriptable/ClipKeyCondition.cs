﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.Playables
{
    [Serializable]
    public class ClipKeyCondition : IClipCondition
    {
        public KeyCode Key;
        public override bool CheckCondition<T>(T var)
        {
            KeyCode curKey = (KeyCode)((object)var);
            if (curKey == Key)
                return true;
            return false;

        }

        public override object GetRawData()
        {
            return Key;
        }
    }
}