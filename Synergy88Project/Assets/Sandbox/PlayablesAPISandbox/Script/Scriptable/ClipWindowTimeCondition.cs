﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using System;
using Sandbox.Character;

namespace Sandbox.Playables
{
    [Serializable]
    public class ClipWindowTimeCondition : IClipCondition
    {

        [InspectorRange(0, 1)]
        public float MinWindow, MaxWindow;

        [InspectorRange(0, 1)]
        public float triggeredWindow;

        public override bool CheckCondition<T>(T var)
        {
            float normalizedTime = (float)((object)(var));

            
            if (normalizedTime == Mathf.Clamp(normalizedTime, MinWindow, MaxWindow))
            {
                triggeredWindow = normalizedTime;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override object GetRawData()
        {
            return triggeredWindow;
        }
    }
}