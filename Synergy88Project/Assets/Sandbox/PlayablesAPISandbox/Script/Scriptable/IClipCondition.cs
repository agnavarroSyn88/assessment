﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
using System;

namespace Sandbox.Playables
{
    [Serializable]
    public abstract class IClipCondition : BaseScriptableObject
    {
        public abstract bool CheckCondition<T>(T val) where T : struct;
        public abstract object GetRawData();
    }

}