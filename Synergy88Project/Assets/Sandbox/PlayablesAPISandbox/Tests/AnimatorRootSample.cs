﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

public class AnimatorRootSample : MonoBehaviour
{

    public AnimationClip cliper1,cliper2;
    RuntimeAnimatorController AnimController;
    public float BlendWeight;

    PlayableGraph playableGraph;
    AnimationMixerPlayable mixerPlayable;

    AnimationClipPlayable ClipControl;
    AnimatorControllerPlayable AnimatorControl;
    AnimationPlayableOutput playableOutput;

    private void Awake()
    {

        playableGraph = PlayableGraph.Create();
        playableOutput = AnimationPlayableOutput.Create(playableGraph, "Animation", GetComponent<Animator>());
        mixerPlayable = AnimationMixerPlayable.Create(playableGraph, 2);
        playableOutput.SetSourcePlayable(mixerPlayable);

    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {

            playableGraph.Disconnect(mixerPlayable, 0);
            playableGraph.Disconnect(mixerPlayable, 1);
            ClipControl = AnimationClipPlayable.Create(playableGraph, cliper1);
            AnimatorControl = AnimatorControllerPlayable.Create(playableGraph, AnimController);
            playableGraph.Connect(AnimatorControl, 0, mixerPlayable, 0);
            playableGraph.Connect(ClipControl, 0, mixerPlayable, 1);

            BlendWeight = 1;
            playableGraph.Play();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            playableGraph.Disconnect(mixerPlayable, 0);
            playableGraph.Disconnect(mixerPlayable, 1);
            ClipControl = AnimationClipPlayable.Create(playableGraph, cliper1);
            AnimationClipPlayable Cl2ipControl = AnimationClipPlayable.Create(playableGraph, cliper2);
            playableGraph.Connect(ClipControl, 0, mixerPlayable, 0);
            playableGraph.Connect(Cl2ipControl, 0, mixerPlayable, 1);

            BlendWeight = 1;
            playableGraph.Play();
        }
        ProcessWeightDistribution();
    }
    void ProcessWeightDistribution()
    {
        BlendWeight = Mathf.Clamp01(BlendWeight);
        mixerPlayable.SetInputWeight(1, BlendWeight);
        mixerPlayable.SetInputWeight(0, 1.0f - BlendWeight);
    }
}