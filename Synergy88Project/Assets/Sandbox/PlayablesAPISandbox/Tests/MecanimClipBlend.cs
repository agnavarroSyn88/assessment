﻿using Sandbox.Character;
using Sandbox.Playables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

public class MecanimClipBlend : MonoBehaviour
{
    enum AnimState
    {
        Idle,
        Interacting
    }
    
    //VARIABLES
    //====================================================================
    #region VARIABLES
    [SerializeField]
    GameObject AnimatedObject;

    public AnimationClip InteractClip, Interact2Clip;

    [Header("Queue")]
    public AnimationClip QueueClip, CurrentClip;
    public RuntimeAnimatorController AnimController;
    public float BlendWeight;

    PlayableGraph playableGraph;
    AnimationMixerPlayable mixerPlayable;

    AnimationClipPlayable ClipControl;
    AnimatorControllerPlayable AnimatorControl;
    AnimationPlayableOutput playableOutput;

    bool triggerLerp;
    public float lerpTime = 0;

    public float lerpSpeed = 1f;
    public float clipTimeWindow;
    [SerializeField]
    private AnimState animState;
    #endregion
    //INITIALIZATION
    //====================================================================
    #region INITIALIZATION
    void Start()
    {
        animState = AnimState.Idle;

        playableGraph = PlayableGraph.Create();
        playableOutput = AnimationPlayableOutput.Create(playableGraph, "Animation", AnimatedObject.GetComponent<Animator>());
        mixerPlayable = AnimationMixerPlayable.Create(playableGraph, 2);
        playableOutput.SetSourcePlayable(mixerPlayable);


        ClipControl = AnimationClipPlayable.Create(playableGraph, null);

        AnimatorControl = AnimatorControllerPlayable.Create(playableGraph, AnimController);


    }
    #endregion
    //====================================================================


    void processInteraction(AnimationClip clip)
    {
        if(animState == AnimState.Interacting)
        {
            Debug.LogError("QUEUE ME:");
            QueueClip = clip;
        }
        else if(animState == AnimState.Idle)
        {
            Debug.LogError("INIT ME:");
            InitializeClip(clip);
            animState = AnimState.Interacting;
        }
    }

    void processAnimWindow()
    {
        if (animState == AnimState.Idle)
            return;

        if (ClipControl.GetTime() > clipTimeWindow)
        {
            if (QueueClip != null)// QUEUED ANIMATION
            {
                Debug.LogError("PLAY MY 2ND ANIM");
                OverriteClip(QueueClip);
                QueueClip = null;
                ClipControl.SetTime(0);
            }
            else if (QueueClip == null)//SINGLE ANIMATION ONLY
            {
                Debug.LogError("END MY ANIM");
                OverriteUsingAnimator();
                animState = AnimState.Idle;
            }
        }
    }
    //====================================================================
    
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            processInteraction(InteractClip);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            processInteraction(Interact2Clip);
        }
        

        updateInterpolate();
        processAnimWindow();
        ProcessWeightDistribution();

        //=-=============================================================================================================================================
        
    }
    void InitializeClip(AnimationClip clip)
    {
        double cacher = 0;
        if (playableGraph.GetPlayableCount() > 0)
        {
            cacher = AnimatorControl.GetTime();
        }

        playableGraph.Disconnect(mixerPlayable, 0);
        playableGraph.Disconnect(mixerPlayable, 1);

        ClipControl = AnimationClipPlayable.Create(playableGraph, clip);

        playableGraph.Connect(AnimatorControl, 0, mixerPlayable, 0);
        playableGraph.Connect(ClipControl, 0, mixerPlayable, 1);

        CurrentClip = clip;
        BlendWeight = 0;
        lerpTime = 0;
        triggerinterpolate(typeofinterpolate.positive);
        playableGraph.Play();
    }

    void OverriteClip(AnimationClip clip)
    {
        double cacher = ClipControl.GetTime();

        playableGraph.Disconnect(mixerPlayable, 0);
        playableGraph.Disconnect(mixerPlayable, 1);

        AnimationClipPlayable clip1 = AnimationClipPlayable.Create(playableGraph, CurrentClip);
        AnimationClipPlayable clip2 = AnimationClipPlayable.Create(playableGraph, clip);

        ClipControl = clip2;

        playableGraph.Connect(clip1, 0, mixerPlayable, 0);
        playableGraph.Connect(clip2, 0, mixerPlayable, 1);

        CurrentClip = clip;
        BlendWeight = 0;
        lerpTime = 0;

        triggerinterpolate(typeofinterpolate.positive);
        clip1.SetTime(cacher);
        clip2.SetTime(cacher);
        playableGraph.Play();
    }
    void OverriteUsingAnimator()
    {
        double cacher = ClipControl.GetTime();

        playableGraph.Disconnect(mixerPlayable, 0);
        playableGraph.Disconnect(mixerPlayable, 1);

        ClipControl = AnimationClipPlayable.Create(playableGraph, CurrentClip);


        playableGraph.Connect(AnimatorControl, 0, mixerPlayable, 0);
        playableGraph.Connect(ClipControl, 0, mixerPlayable, 1);

        lerpTime = 0;
        triggerinterpolate(typeofinterpolate.negative);
        ClipControl.SetTime(cacher);
        playableGraph.Play();
    }

    void ResetVars()
    {

    }
    //INTERPOLATION
    //====================================================================
    #region INTERPOLATION
    enum typeofinterpolate
    {
        none,
        positive,
        negative
    }
    private typeofinterpolate interpolType;
    void triggerinterpolate(typeofinterpolate val)
    {
        interpolType = val;
    }

    void updateInterpolate()
    {
        if (interpolType == typeofinterpolate.none)
            return;

        lerpTime += (lerpSpeed * Time.deltaTime);
        if (interpolType == typeofinterpolate.positive)
            BlendWeight = Mathf.Lerp(0, 1, lerpTime);

        if (interpolType == typeofinterpolate.negative)
            BlendWeight = Mathf.Lerp(0, 1, 1 - lerpTime);


        if (lerpTime >= 1)
        {
            lerpTime = 0;
            interpolType = typeofinterpolate.none;
        }
    }
    #endregion
    //====================================================================

    void ProcessWeightDistribution()
    {
        BlendWeight = Mathf.Clamp01(BlendWeight);
        mixerPlayable.SetInputWeight(1, BlendWeight);
        mixerPlayable.SetInputWeight(0, 1.0f - BlendWeight);
    }
}