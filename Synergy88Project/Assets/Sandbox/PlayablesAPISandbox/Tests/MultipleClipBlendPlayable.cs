﻿using UnityEngine;
using UniRx;
using FrameworkInputSystem;
using FullInspector;
using System.Collections.Generic;
using System;
using Rewired;
using UnityEngine.Playables;
using UnityEngine.Animations;
using Sandbox.Character;

public class MultipleClipBlendPlayable : MonoBehaviour {

    //VARIABLES
    //====================================================================
    #region VARIABLES
    [SerializeField]
    GameObject AnimatedObject;

    public AnimationClip InteractClip, Interact2Clip, Interact3Clip;

    public RuntimeAnimatorController AnimController;

    PlayableGraph playableGraph;
    AnimationMixerPlayable mixerPlayable;

    AnimatorControllerPlayable AnimatorControl;
    AnimationPlayableOutput playableOutput;

    public float weight1, weight2, weight3;
    #endregion
    //INITIALIZATION
    //====================================================================
    #region INITIALIZATION
    void Start()
    {

        playableGraph = PlayableGraph.Create();
        playableOutput = AnimationPlayableOutput.Create(playableGraph, "Animation", AnimatedObject.GetComponent<Animator>());
        mixerPlayable = AnimationMixerPlayable.Create(playableGraph, 3);
        playableOutput.SetSourcePlayable(mixerPlayable);


        AnimationClipPlayable clip1 = AnimationClipPlayable.Create(playableGraph, InteractClip);
        AnimationClipPlayable clip2 = AnimationClipPlayable.Create(playableGraph, Interact2Clip);
        AnimationClipPlayable clip3 = AnimationClipPlayable.Create(playableGraph, Interact3Clip);
        AnimatorControl = AnimatorControllerPlayable.Create(playableGraph, AnimController);

    
        playableGraph.Connect(AnimatorControl, 0, mixerPlayable, 0);
        playableGraph.Connect(clip2, 0, mixerPlayable, 1);
        playableGraph.Connect(clip3, 0, mixerPlayable, 2);
        playableGraph.Play();
    }
    #endregion
    //====================================================================

    void Update()
    {


        ProcessWeightDistribution();

        

    }


    void ProcessWeightDistribution()
    {
        mixerPlayable.SetInputWeight(0, weight1);
        mixerPlayable.SetInputWeight(1, weight2);
        mixerPlayable.SetInputWeight(2, weight3);
    }
}