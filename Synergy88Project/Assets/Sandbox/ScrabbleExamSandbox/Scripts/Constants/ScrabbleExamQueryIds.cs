﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.ScrabbleExam
{

    public abstract class ScrabbleExamQueryIds
    { 
        public const string LetterTileCount = "LetterTileCount";
        public const string LetterParam = "LetterParam";
        public const string TotalLetterTypes = "TotalLetterTypes";
        public const string LetterPoints = "LetterPoints";
        public const string IsLetterInventoryEmpty = "IsLetterInventoryEmpty";
        public const string TotalTileCount = "TotalTileCount";

        public const string Grid = "Grid";
        public const string GridSlotData = "GridSlotData";
        public const string GridIndexParam = "GridIndexParam";

        public const string Score = "Score";

        public const string TileRack = "TileRack";
        public const string IsTileRackEmpty = "IsTileRackEmpty";

        public const string CurrentSkin = "CurrentSkin";
        public const string Skins = "Skins";
        public const string ChosenSkinName = "ChosenSkinName";

        public const string Multiplier = "Multiplier";
        public const string MultiplierParam = "MultiplierParam";

        
    }
}