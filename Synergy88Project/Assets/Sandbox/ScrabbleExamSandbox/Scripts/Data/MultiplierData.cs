﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class MultiplierData
    {
        public string MultiplierLabel;
        public int MultiplierValue;
        public MultiplierType Type;
        public string CharacterIdentifier;
        public Color MultiplierColor;
    }

    public enum MultiplierType
    {
        Word,
        Letter,
        None
    }
}