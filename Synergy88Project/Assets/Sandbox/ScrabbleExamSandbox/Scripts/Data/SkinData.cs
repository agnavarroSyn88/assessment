﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class SkinData
    {
        public string SkinId;
        public Sprite TileTexture;
        public Font TileFont;
        public Color TileFontColor;
        public Sprite SlotTexture;
    }
}
