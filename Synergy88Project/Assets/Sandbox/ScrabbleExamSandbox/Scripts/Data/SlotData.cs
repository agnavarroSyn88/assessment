﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class SlotData 
    {
        public int Index;
        public SlotType Type;
        public MultiplierData Multiplier;
        public Sprite SkinImage;
        public TileData TilePlaced = null;
        public bool IsDisabled = false;
        public bool HasTile = false;
        public MessageBroker MsgBroker = new MessageBroker();

        public int ReturnAppliedScore()
        {
            int score = TilePlaced.Score;
            if (Multiplier.Type == MultiplierType.Letter)
            {
                score *= Multiplier.MultiplierValue;
            }

            return score;
        }
    }

    public enum SlotMultiplier
    {
        None,
        DoubleLetterScore,
        DoubleWordScore,
        TripleLetterScore,
        TripleWordScore
    }

    public enum SlotType
    {
        Grid,
        Rack
    }
}