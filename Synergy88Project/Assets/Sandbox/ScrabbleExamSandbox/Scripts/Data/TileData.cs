﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class TileData
    {

        public string Letter;
        public int Score;
        public int TileIndex;
        public Sprite TileTexture;
        public Font TileFont;
        public Color TileFontColor;
        public bool IsTileUsable = true;
        public MessageBroker MsgBroker = new MessageBroker();
    }
}
