﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Sandbox.ScrabbleExam;

public class CreateBoardInventory
{
    [MenuItem("Assets/Create/ScrabbleExam/BoardInventory")]
    public static void CreateBoardInventoryItem()
    {
        BoardInventory bI = ScriptableObject.CreateInstance<BoardInventory>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path.Equals(""))
        {
            path = "Assets";
        }
        else
        {
            if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }
        }

        string newPath = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(BoardInventory).ToString() + ".asset");

        AssetDatabase.CreateAsset(bI, newPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = bI;
    }
}
