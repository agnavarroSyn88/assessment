﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Sandbox.ScrabbleExam;

public class CreateLetterInventory  {

	[MenuItem("Assets/Create/ScrabbleExam/LetterInventory")]
    public static void CreateLetterInventoryItem()
    {
        
        LetterInventory lt = ScriptableObject.CreateInstance<LetterInventory>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if(path.Equals(""))
        {
            path = "Assets";
        }
        else
        {
            if(Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }
        }

        string newPath = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(LetterInventory).ToString() + ".asset");

        AssetDatabase.CreateAsset(lt, newPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = lt;
    }
}
