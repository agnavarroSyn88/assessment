﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Sandbox.ScrabbleExam;

public class CreateMultiplierInventory {

    [MenuItem("Assets/Create/ScrabbleExam/MultiplierInventory")]
    public static void CreateMultiplierInventoryItem()
    {

        MultiplierInventory mI = ScriptableObject.CreateInstance<MultiplierInventory>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path.Equals(""))
        {
            path = "Assets";
        }
        else
        {
            if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }
        }

        string newPath = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(MultiplierInventory).ToString() + ".asset");

        AssetDatabase.CreateAsset(mI, newPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = mI;
    }
}
