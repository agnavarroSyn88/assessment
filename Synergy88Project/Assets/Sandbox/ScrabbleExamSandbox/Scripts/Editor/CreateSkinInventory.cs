﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Sandbox.ScrabbleExam;

public class CreateSkinInventory
{
    [MenuItem("Assets/Create/ScrabbleExam/SkinInventory")]
    public static void CreateLetterInventoryItem()
    {

        SkinInventory sI = ScriptableObject.CreateInstance<SkinInventory>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (path.Equals(""))
        {
            path = "Assets";
        }
        else
        {
            if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }
        }

        string newPath = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(SkinInventory).ToString() + ".asset");

        AssetDatabase.CreateAsset(sI, newPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = sI;
    }
}
