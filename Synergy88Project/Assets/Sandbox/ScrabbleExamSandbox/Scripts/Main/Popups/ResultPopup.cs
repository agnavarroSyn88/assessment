﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using uPromise;

using UniRx;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;
using Common.Utils;
using Synergy88;

namespace Sandbox.ScrabbleExam
{
    public class ResultPopup : PopupWindow
    {
        [SerializeField]
        private Text FinalScoreText;

        [SerializeField]
        private Text ScoreText;

        [SerializeField]
        private Text HomeButtonText;

        private IntReactiveProperty Score = new IntReactiveProperty(0);

        private ReactiveProperty<DeviceOrientation> CurrentOrientation = new ReactiveProperty<DeviceOrientation>();

        

        protected override void Awake()
        {
            base.Awake();
            Score = QuerySystem.Query<IntReactiveProperty>(ScrabbleExamQueryIds.Score);

            Score.Subscribe(_ => UpdateScore(_));

            CurrentOrientation
              .Subscribe(_ =>
              {
                  HandleDeviceOrientation(_);
              }).AddTo(this);

        }

        private void Start()
        {

        }

        void UpdateScore(int score)
        {
            ScoreText.text = "Score: " + score.ToString();
        }

        void HandleDeviceOrientation(DeviceOrientation orientation)
        {
            switch (orientation)
            {
                case DeviceOrientation.LandscapeLeft:
                case DeviceOrientation.LandscapeRight:
                    FinalScoreText.fontSize = 75;
                    ScoreText.fontSize = 100;
                    HomeButtonText.fontSize = 50;
                    break;
                case DeviceOrientation.Portrait:
                case DeviceOrientation.PortraitUpsideDown:
                    FinalScoreText.fontSize = 150;
                    ScoreText.fontSize = 200;
                    HomeButtonText.fontSize = 100;
                    break;
            }
        }
    }
}
