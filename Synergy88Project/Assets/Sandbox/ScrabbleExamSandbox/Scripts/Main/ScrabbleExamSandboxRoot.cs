﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;

namespace Sandbox.ScrabbleExam
{

    public class ScrabbleExamSandboxRoot : Scene
    {
        [SerializeField]
        private WordValidator WordChecker;

        [SerializeField]
        private List<TileData> TilesPlayed = new List<TileData>();

        [SerializeField]
        bool isConnected = false;

        private IntReactiveProperty Score = new IntReactiveProperty(0);

        private bool IsFirstTurn = true;

        [SerializeField]
        private List<int> GridIndexPlayed = new List<int>();

        protected override void Awake()
        {
            base.Awake();
            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.Score, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(Score);
            });

            this.Receive<PlayTile>()
                .Subscribe(_ =>
                {
                    if (!TilesPlayed.Contains(_.TilePlaced))
                        TilesPlayed.Add(_.TilePlaced);
                }).AddTo(this);

            this.Receive<UnPlayTile>()
                .Subscribe(_ =>
                {
                    if(TilesPlayed.Contains(_.TileRemoved))
                        TilesPlayed.Remove(_.TileRemoved);
                }).AddTo(this);

            this.Receive<SubmitWordSignal>()
                .Subscribe(_ =>
                {
                    CheckForWords();
                }).AddTo(this);
        }

        protected override void Start()
        {
            base.Start();

        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.Score);
        }

        void CheckForWords()
        {
            TilesPlayed.Sort((a, b) => a.TileIndex.CompareTo(b.TileIndex));
            bool isTurnValid = false;
            int pointsToBeAdded = 0;
            List<SlotData> grid = QuerySystem.Query<List<SlotData>>(ScrabbleExamQueryIds.Grid);
            bool hasStartTile = false;
            isConnected = false;

            for (int i = 0; i < TilesPlayed.Count; i++)
            {
                string wordFormed = "";
                int wordScore = 0;
                int wordMultiplier = 1;
                int currentTile;
                Debug.Log(TilesPlayed[0].Letter);
                int indexFirstTile = TilesPlayed[i].TileIndex;
                
                
                if(i == 0 || TilesPlayed[i].TileIndex - TilesPlayed[i - 1].TileIndex != 1)
                {
                    Debug.Log(indexFirstTile);
                    while(indexFirstTile -1 >= 0 && grid[indexFirstTile-1].HasTile)
                    {
                        Debug.Log(indexFirstTile);
                        indexFirstTile--;   
                    }

                    currentTile = indexFirstTile;
                    Debug.Log(currentTile);
                    while (currentTile >= 0 && currentTile < grid.Count && grid[currentTile].HasTile)
                    {
                        if(IsFirstTurn)
                        {
                            if(grid[currentTile].Multiplier.MultiplierLabel.Equals("Start"))
                            {
                                Debug.Log("has start tile");
                                hasStartTile = true;
                            }
                        }
                        else
                        {
                            if(GridIndexPlayed.Contains(currentTile))
                            {
                                isConnected = true;
                            }
                        }
                        Debug.Log(currentTile);
                        wordFormed += grid[currentTile].TilePlaced.Letter;
                        wordScore += grid[currentTile].ReturnAppliedScore();
                        if (grid[currentTile].Multiplier.Type == MultiplierType.Word)
                        {
                            wordMultiplier *= grid[currentTile].Multiplier.MultiplierValue;
                        }
                        
                        currentTile++;
                        
                        Debug.Log(wordFormed + "Length: " + wordFormed.Length);
                    }


                    WordStatus status = WordChecker.IsValid(wordFormed);
                    Debug.Log(status);
                    if (status == WordStatus.UnUsed || status == WordStatus.Used)
                    {
                        pointsToBeAdded += wordScore * wordMultiplier;
                        Debug.Log(wordFormed);
                        isTurnValid = true;

                    }
                    else if (wordFormed.Length > 1)
                    {
                        Debug.Log("word is invalid");
                        isTurnValid = false;
                        break;
                    }

                }

                indexFirstTile = TilesPlayed[i].TileIndex;

                if (i == 0 || TilesPlayed[i].TileIndex - TilesPlayed[i - 1].TileIndex != 15)
                {

                    while (indexFirstTile - 15 >= 0 && grid[indexFirstTile-15].HasTile)
                    {
                        indexFirstTile -= 15;
                    }

                    wordFormed = "";
                    wordScore = 0;
                    wordMultiplier = 1;
                    currentTile = indexFirstTile;
                    while (currentTile >= 0 && currentTile < grid.Count && grid[currentTile].HasTile)
                    {
                        if (IsFirstTurn)
                        {
                            if (grid[currentTile].Multiplier.MultiplierLabel.Equals("Start"))
                            {
                                Debug.Log("has start tile");
                                hasStartTile = true;
                            }
                        }
                        else
                        {
                            if (GridIndexPlayed.Contains(currentTile))
                            {
                                isConnected = true;
                            }
                        }
                        Debug.Log(currentTile);
                        wordFormed += grid[currentTile].TilePlaced.Letter;
                        wordScore += grid[currentTile].ReturnAppliedScore();
                        if (grid[currentTile].Multiplier.Type == MultiplierType.Word)
                        {
                            wordMultiplier *= grid[currentTile].Multiplier.MultiplierValue;
                        }
                        currentTile += 15;
                        Debug.Log(wordFormed + "Length: " + wordFormed.Length);
                    }
                  

                    WordStatus status = WordChecker.IsValid(wordFormed);
                    Debug.Log(status);
                    if (status == WordStatus.UnUsed || status == WordStatus.Used)
                    {
                        pointsToBeAdded += wordScore * wordMultiplier;
                        Debug.Log(wordFormed);
                        isTurnValid = true;
                        
                    }
                    else if (wordFormed.Length > 1)
                    {
                        Debug.Log("word is invalid");
                        isTurnValid = false;
                        break;
                    }

                }
            }

            if (isTurnValid)
            {
                if (IsFirstTurn)
                {
                    if (hasStartTile)
                    {
                        isTurnValid = true;
                    }
                    else
                    {
                        isTurnValid = false;
                    }
                }
                else
                {
                    if (isConnected)
                    {
                        isTurnValid = true;
                    }
                    else
                    {
                        isTurnValid = false;
                    }
                }
            }

            if (isTurnValid)
            {
                if (IsFirstTurn)
                    IsFirstTurn = false;

                if (TilesPlayed.Count == 7)
                    Score.Value += 50;

                Score.Value += pointsToBeAdded;
                foreach (TileData tile in TilesPlayed)
                {
                    tile.IsTileUsable = false;
                    IQueryRequest request = QuerySystem.Start(ScrabbleExamQueryIds.Multiplier);
                    request.AddParameter(ScrabbleExamQueryIds.MultiplierParam, "1");
                    grid[tile.TileIndex].Multiplier = QuerySystem.Complete<MultiplierData>();
                    tile.MsgBroker.Publish(new SealBlankTile());
                    GridIndexPlayed.Add(tile.TileIndex);

                }
                TilesPlayed.Clear();

                bool isEndGame = QuerySystem.Query<bool>(ScrabbleExamQueryIds.IsTileRackEmpty)
                    && QuerySystem.Query<bool>(ScrabbleExamQueryIds.IsLetterInventoryEmpty);

                if(isEndGame)
                {
                    QuerySystem.Query<PopupCollectionRoot>(QueryIds.PopupCollection).Show(Popup.Results);
                }
            }
            else
            {
                List<GameObject> TileRack = QuerySystem.Query<List<GameObject>>(ScrabbleExamQueryIds.TileRack);
                foreach(GameObject slot in TileRack)
                {
                    if(!slot.GetComponent<SlotUI>().SlotData.HasTile)
                    {
                        TileData tp = TilesPlayed[0];
                        grid[tp.TileIndex].HasTile = false;
                        tp.MsgBroker.Publish(new ReturnTileToRack() { SlotPosition = slot.transform.position, SlotParent = slot.transform });
                        TilesPlayed.RemoveAt(0);
                        
                    }
                }
                TilesPlayed.Clear();
               

            }
            
        }

    }
}