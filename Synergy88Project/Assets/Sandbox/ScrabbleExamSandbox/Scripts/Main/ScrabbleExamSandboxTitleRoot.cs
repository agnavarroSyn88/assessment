﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;

namespace Sandbox.ScrabbleExam
{
    public class ScrabbleExamSandboxTitleRoot : Scene
    {
        private ReactiveProperty<DeviceOrientation> CurrentOrientation = new ReactiveProperty<DeviceOrientation>();

        [SerializeField]
        private Transform TilePicker;

        [SerializeField]
        private GameObject TilePrefab;

        [SerializeField]
        private RectTransform ScrollHolder;

        [SerializeField]
        private Text TitleText;

        [SerializeField]
        private HorizontalScrollSnap SkinChooserScroller;

        StringReactiveProperty ChosenSkin = new StringReactiveProperty("");

        Dictionary<string, SkinData> Skins = new Dictionary<string, SkinData>();

        List<string> SkinIds = new List<string>();

        List<RectTransform> SkinObjects = new List<RectTransform>();


        protected override void Awake()
        {
            base.Awake();
            CurrentOrientation = QuerySystem.Query<ReactiveProperty<DeviceOrientation>>(QueryIds.CurrentDeviceOrientaion);

            Skins = QuerySystem.Query<Dictionary<string, SkinData>>(ScrabbleExamQueryIds.Skins);

            ChosenSkin = QuerySystem.Query<StringReactiveProperty>(ScrabbleExamQueryIds.ChosenSkinName);

            foreach (KeyValuePair<string, SkinData> skin in Skins)
            {
                GameObject tile = Instantiate(TilePrefab, TilePicker);
                TileData newTile = new TileData();
                newTile.Letter = "A";
                newTile.Score = 1;
                newTile.TileFont = skin.Value.TileFont;
                newTile.TileFontColor = skin.Value.TileFontColor;
                newTile.TileTexture = skin.Value.TileTexture;
                tile.GetComponent<TileUI>().SetTileData(newTile);
                SkinIds.Add(skin.Value.SkinId);
                SkinObjects.Add(tile.GetComponent<RectTransform>());
            }

            //HandleDeviceOrientation(CurrentOrientation.Value);

            AddButtonHandler(EButtonType.Play, (ButtonClickedSignal signal) =>
            {

                this.Publish(new LoadGameSignal());
            });


            /*CurrentOrientation
                .Subscribe(_ =>
                {
                    HandleDeviceOrientation(_);
                }).AddTo(this);*/
        }

        protected override void Start()
        {
            base.Start();

        }

        void Update()
        {
            ChosenSkin.Value = SkinIds[SkinChooserScroller.CurrentPage];
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
        }

        void HandleDeviceOrientation(DeviceOrientation orientation)
        {
            switch(orientation)
            {
                case DeviceOrientation.LandscapeLeft:
                case DeviceOrientation.LandscapeRight:                    
                    ResizeSkinObjects(350f);
                    TitleText.fontSize = 150;
                    break;
                case DeviceOrientation.Portrait:
                case DeviceOrientation.PortraitUpsideDown:
                    ResizeSkinObjects(700f);
                    TitleText.fontSize = 250;
                    break;
            }
        }

        void ResizeSkinObjects(float size)
        {
            ScrollHolder.sizeDelta = new Vector2(size, size);

            SkinChooserScroller.InitialiseChildObjectsFromScene();
        }
    }
}