﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;

namespace Sandbox.ScrabbleExam
{
    public class ScrabbleExamServicesRoot : Scene
    {

        protected override void Awake()
        {
            base.Awake();

        }

        protected override void Start()
        {
            base.Start();
            this.Publish(new LoadHomeSignal());

        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
        }
    }
}
