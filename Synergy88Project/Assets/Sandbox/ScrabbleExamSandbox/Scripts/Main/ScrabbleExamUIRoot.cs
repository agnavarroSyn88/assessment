﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;

namespace Sandbox.ScrabbleExam
{
    public class ScrabbleExamUIRoot : Scene
    {

        PopupCollectionRoot PopupCollection;

        protected override void Awake()
        {
            base.Awake();
             
            PopupCollection = QuerySystem.Query<PopupCollectionRoot>(QueryIds.PopupCollection);
            AddButtonHandler(EButtonType.Submit, (ButtonClickedSignal signal) =>
            {
                this.Publish(new SubmitWordSignal());
            });

            AddButtonHandler(EButtonType.Pass, (ButtonClickedSignal signal) =>
            {
                this.Publish(new PassSignal());
            });

            AddButtonHandler(EButtonType.ReplaceAllTiles, (ButtonClickedSignal signal) =>
            {
                this.Publish(new ChangeTiles());
            });

            AddButtonHandler(EButtonType.Home, (ButtonClickedSignal signal) =>
            {
                if (PopupCollection.HasPopUp(Popup.Results))
                    PopupCollection.Hide();

                this.Publish(new LoadHomeSignal());
            });
        }

        protected override void Start()
        {
            base.Start();

        }

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}
