﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Synergy88;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using System;

namespace Sandbox.ScrabbleExam
{
    public class GridGeneratorService : BaseBehavior
    {
        public const int MAX_NO_GRIDSLOTS = 225;

        [SerializeField]
        private BoardInventory Boards;

        [SerializeField]
        private MultiplierInventory Multipliers;

        [SerializeField]
        private GameObject GridParent;

        [SerializeField]
        private GameObject SlotPrefab;

        [SerializeField]
        private List<SlotData> Grid;

        [SerializeField]
        private int chosenBoardIndex = 0;

        protected override void Awake()
        {
            base.Awake();
            Factory.Register<GridGeneratorService>(this);

            chosenBoardIndex = UnityEngine.Random.Range(0, Boards.BoardData.Count);

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.GridSlotData, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                int index = (int)request.GetParameter(ScrabbleExamQueryIds.GridIndexParam);
                result.Set(Grid[index]);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.Grid, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                
                result.Set(Grid);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.Multiplier, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                string identifier = (string)request.GetParameter(ScrabbleExamQueryIds.MultiplierParam);
                result.Set(Multipliers.Multipliers[identifier]);
            });

            GenerateGrid();
        }

        protected void OnDestroy()
        {
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.GridSlotData);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.Grid);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.Multiplier);
            Factory.Clean<GridGeneratorService>();
        }

        private void GenerateGrid()
        {
            SkinData chosenSkin = QuerySystem.Query<SkinData>(ScrabbleExamQueryIds.CurrentSkin);
            List<string> chosenBoard = Boards.BoardData[chosenBoardIndex];
            Grid = new List<SlotData>();
            float x = -10.5f;
            float y = 10.5f;
            for(int i = 0; i< MAX_NO_GRIDSLOTS; i++)
            {
                if (i != 0 && i % 15 == 0)
                {
                    x = -10.5f;
                    y -= 1.5f;
                }
                GameObject temp = Instantiate(SlotPrefab, new Vector3(x, y, 0f), Quaternion.identity, GridParent.transform);
                temp.name = i.ToString();
                SlotData newSlotData = new SlotData();
                newSlotData.Index = i;
                newSlotData.Multiplier = Multipliers.Multipliers[chosenBoard[i]];
                newSlotData.Type = SlotType.Grid;
                if (chosenBoard[i].Equals("0"))
                {
                    newSlotData.IsDisabled = true; 
                }
                newSlotData.SkinImage = chosenSkin.SlotTexture;
                temp.GetComponent<SlotUI>().SetSlotUI(newSlotData);
                //place into grid parent
                //temp.transform.parent = GridParent.transform;
                Grid.Add(newSlotData);
                x += 1.5f;
                
                
            }

        }
    }
}