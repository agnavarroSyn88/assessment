﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Synergy88;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using System;

namespace Sandbox.ScrabbleExam
{

    public class LetterInventoryService : BaseBehavior
    {

        [SerializeField]
        private LetterInventory LetterInventory;

       
        protected override void Awake()
        {
            base.Awake();
            Factory.Register<LetterInventoryService>(this);

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.LetterTileCount, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                string letter = (string)request.GetParameter(ScrabbleExamQueryIds.LetterParam);
                result.Set(LetterInventory.LettersCount[letter]);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.TotalLetterTypes, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(LetterInventory.LettersCount.Count);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.LetterPoints, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                string letter = (string)request.GetParameter(ScrabbleExamQueryIds.LetterParam);
                result.Set(LetterInventory.LettersPoints[letter]);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.IsLetterInventoryEmpty, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(InventoryEmpty());
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.TotalTileCount, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(TotalTiles());
            });

            MessageBroker.Default.Receive<ReturnLetterToInventory>()
                .Subscribe(_ =>
                {
                    LetterInventory.LettersCount[_.Letter].Value++;
                }).AddTo(this);

        }

        protected void OnDestroy()
        {
            ResetLetterTileInventory();
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.LetterTileCount);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.LetterPoints);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.TotalLetterTypes);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.IsLetterInventoryEmpty);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.TotalTileCount);
            Factory.Clean<LetterInventoryService>();
            
        }

        private bool InventoryEmpty()
        {
            bool isEmpty = true;
            foreach(var letterCount in LetterInventory.LettersCount)
            {
                if(letterCount.Value.Value > 0)
                {
                    isEmpty = false;
                    break;
                }
            }
            return isEmpty;
        }

        private int TotalTiles()
        {
            int allTiles = 0;
            foreach(var tileCount in LetterInventory.LettersCount)
            {
                allTiles += tileCount.Value.Value;
            }
            return allTiles;
        }


        void ResetLetterTileInventory()
        {
            LetterInventory.LettersCount["A"].Value = 9;
            LetterInventory.LettersCount["B"].Value = 2;
            LetterInventory.LettersCount["C"].Value = 2;
            LetterInventory.LettersCount["D"].Value = 4;
            LetterInventory.LettersCount["E"].Value = 12;
            LetterInventory.LettersCount["F"].Value = 2;
            LetterInventory.LettersCount["G"].Value = 3;
            LetterInventory.LettersCount["H"].Value = 2;
            LetterInventory.LettersCount["I"].Value = 9;
            LetterInventory.LettersCount["J"].Value = 1;
            LetterInventory.LettersCount["K"].Value = 1;
            LetterInventory.LettersCount["L"].Value = 4;
            LetterInventory.LettersCount["M"].Value = 2;
            LetterInventory.LettersCount["N"].Value = 6;
            LetterInventory.LettersCount["O"].Value = 8;
            LetterInventory.LettersCount["P"].Value = 2;
            LetterInventory.LettersCount["Q"].Value = 1;
            LetterInventory.LettersCount["R"].Value = 6;
            LetterInventory.LettersCount["S"].Value = 4;
            LetterInventory.LettersCount["T"].Value = 6;
            LetterInventory.LettersCount["U"].Value = 4;
            LetterInventory.LettersCount["V"].Value = 2;
            LetterInventory.LettersCount["W"].Value = 2;
            LetterInventory.LettersCount["X"].Value = 1;
            LetterInventory.LettersCount["Y"].Value = 4;
            LetterInventory.LettersCount["Z"].Value = 1;
            LetterInventory.LettersCount[" "].Value = 2;
        }

    }
}


