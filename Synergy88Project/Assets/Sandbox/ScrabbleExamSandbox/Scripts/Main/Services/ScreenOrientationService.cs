﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Synergy88;
using FullInspector;
using Common.Utils;
using Common.Query;

namespace Sandbox.ScreenOrientationInput
{
    public class ScreenOrientationService : BaseBehavior
    {
        [SerializeField]
        private ReactiveProperty<DeviceOrientation> CurrentOrientation = new ReactiveProperty<DeviceOrientation>();

        [SerializeField]
        private Camera GameCamera;

        protected override void Awake()
        {
            base.Awake();
            Factory.Register<ScreenOrientationService>(this);
            CurrentOrientation.Value = DeviceOrientation.LandscapeLeft;
            GameCamera = QuerySystem.Query<Camera>(QueryIds.SystemCamera);
            QuerySystem.RegisterResolver(QueryIds.CurrentDeviceOrientaion, delegate (IQueryRequest request, IMutableQueryResult result)
            {

                result.Set(CurrentOrientation);
            });


        }

        // Update is called once per frame
        void Update()
        {
#if UNITY_ANDROID || UNITY_IOS
            CurrentOrientation.Value = Input.deviceOrientation;
#elif UNITY_EDITOR 
            Debug.Log("Camera Pixel Rect: " + GameCamera.pixelRect.ToString());
            if (GameCamera.pixelRect.width > GameCamera.pixelRect.height)
                CurrentOrientation.Value = DeviceOrientation.LandscapeLeft;
            else if (GameCamera.pixelRect.height > GameCamera.pixelRect.width)
                CurrentOrientation.Value = DeviceOrientation.Portrait;
#endif
        }

        protected void OnDestroy()
        {
            Factory.Clean<ScreenOrientationService>();
        }
    }
}