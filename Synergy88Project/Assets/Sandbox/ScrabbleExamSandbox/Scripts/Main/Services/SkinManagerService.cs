﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Synergy88;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using System;

namespace Sandbox.ScrabbleExam
{
    public class SkinManagerService: BaseBehavior
    {
        [SerializeField]
        private SkinInventory Skins;

        [SerializeField]
        private SkinData CurrentSkin;

        [SerializeField]
        private StringReactiveProperty ChosenSkinName = new StringReactiveProperty("");

        protected override void Awake()
        {
            base.Awake();
            Factory.Register<SkinManagerService>(this);
            //CurrentSkin = Skins.Skins[ChosenSkinName.Value];
            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.CurrentSkin, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(CurrentSkin);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.Skins, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(Skins.Skins);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.ChosenSkinName, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(ChosenSkinName);
            });

            ChosenSkinName
                .Subscribe(_ =>
                {
                    CurrentSkin = Skins.Skins[_];
                }).AddTo(this);
        }

        protected void OnDestroy()
        {
            
            Factory.Clean<SkinManagerService>();

        }
    }
}