﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using Synergy88;

namespace Sandbox.ScrabbleExam
{
    public class TileRackService : BaseBehavior
    {
        [SerializeField]
        private List<GameObject> RackSlots = new List<GameObject>();

        [SerializeField]
        private List<string> Letters = new List<string>();

        [SerializeField]
        private GameObject TilePrefab;

        [SerializeField]
        private Camera GameCamera;

        [SerializeField]
        private Font DefaultFont;

        [SerializeField]
        private SlotPositions SlotPlaces;

        private ReactiveProperty<DeviceOrientation> CurrentOrientation = new ReactiveProperty<DeviceOrientation>();

        protected override void Awake()
        {
            CurrentOrientation = QuerySystem.Query<ReactiveProperty<DeviceOrientation>>(QueryIds.CurrentDeviceOrientaion);

            HandleDeviceOrientation(CurrentOrientation.Value);
            GameCamera = QuerySystem.Query<Camera>(QueryIds.SystemCamera);
            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.TileRack, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(RackSlots);
            });

            QuerySystem.RegisterResolver(ScrabbleExamQueryIds.IsTileRackEmpty, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(TileRackEmpty());
            });

            MessageBroker.Default.Receive<PassSignal>()
                .Subscribe(_ =>
                {
                    FillTileRack();
                }).AddTo(this);

            MessageBroker.Default.Receive<ChangeTiles>()
                .Subscribe(_ =>
                {
                    ReplaceAllTiles();
                }).AddTo(this);

            CurrentOrientation
               .Subscribe(_ =>
               {
                   HandleDeviceOrientation(_);
               }).AddTo(this);
        }

        void Start()
        {
            foreach (GameObject slot in RackSlots)
            {
                slot.GetComponent<SlotUI>().SlotData.HasTile = false;
            }
            FillTileRack();

        }

        protected void OnDestroy()
        {
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.TileRack);
            QuerySystem.RemoveResolver(ScrabbleExamQueryIds.IsTileRackEmpty);
        }

        private bool TileRackEmpty()
        {
            bool isEmpty = true;
            foreach(var slot in RackSlots)
            {
                if(slot.GetComponent<SlotUI>().SlotData.HasTile)
                {
                    isEmpty = false;
                    break;
                }
                
            }
            return isEmpty;
        }

        public void FillTileRack()
        {
            SkinData chosenSkin = QuerySystem.Query<SkinData>(ScrabbleExamQueryIds.CurrentSkin);
            int totalLettersCount = QuerySystem.Query<int>(ScrabbleExamQueryIds.TotalLetterTypes);
            IntReactiveProperty tileCount = new IntReactiveProperty(0);
            foreach (GameObject slot in RackSlots)
            {
                SlotUI slotUI = slot.GetComponent<SlotUI>();
                if (!slotUI.SlotData.HasTile)
                {
                    int randomNum = 0;
                    do
                    {
                        randomNum = Random.Range(0, totalLettersCount);
                        IQueryRequest request = QuerySystem.Start(ScrabbleExamQueryIds.LetterTileCount);
                        request.AddParameter(ScrabbleExamQueryIds.LetterParam, Letters[randomNum]);
                        tileCount = QuerySystem.Complete<IntReactiveProperty>();
                        if (tileCount.Value > 0)
                        {
                            GameObject temp = Instantiate(TilePrefab, slot.transform);
                            temp.GetComponent<DragTile>().GameCamera = GameCamera;
                            temp.transform.localPosition = -Vector3.forward;
                            tileCount.Value--;
                            TileData newLetter = new TileData();
                            newLetter.Letter = Letters[randomNum];
                            IQueryRequest pointsRequest = QuerySystem.Start(ScrabbleExamQueryIds.LetterPoints);
                            pointsRequest.AddParameter(ScrabbleExamQueryIds.LetterParam, Letters[randomNum]);
                            int letterPoint = QuerySystem.Complete<int>();
                            newLetter.Score = letterPoint;
                            //set skin info
                            newLetter.TileFont = chosenSkin.TileFont;
                            newLetter.TileTexture = chosenSkin.TileTexture;
                            newLetter.TileFontColor = chosenSkin.TileFontColor;
                            temp.GetComponent<TileUI>().SetTileData(newLetter);
                            slotUI.SlotData.TilePlaced = newLetter;
                            slotUI.SlotData.HasTile = true;
                            break;
                        }

                        if (GetFreeSlots() > 0 && QuerySystem.Query<int>(ScrabbleExamQueryIds.TotalTileCount) <= 0)
                            break;

                    } while (tileCount.Value <= 0);
                }
            }
        }

        public void ReplaceAllTiles()
        {
            SkinData chosenSkin = QuerySystem.Query<SkinData>(ScrabbleExamQueryIds.CurrentSkin);
            int totalLettersCount = QuerySystem.Query<int>(ScrabbleExamQueryIds.TotalLetterTypes);
            IntReactiveProperty tileCount = new IntReactiveProperty(0);
            foreach (GameObject slot in RackSlots)
            {
                GameObject tile = slot.transform.GetChild(1).gameObject;
                if (tile.GetComponent<TileUI>() != null)
                {
                    string letter = tile.GetComponent<TileUI>().TileData.Letter;
                    IQueryRequest request = QuerySystem.Start(ScrabbleExamQueryIds.LetterTileCount);
                    request.AddParameter(ScrabbleExamQueryIds.LetterParam, letter);
                    tileCount = QuerySystem.Complete<IntReactiveProperty>();
                    tileCount.Value++;

                    SlotUI slotUI = slot.GetComponent<SlotUI>();
                    int randomNum = 0;
                    do
                    {
                        randomNum = Random.Range(0, totalLettersCount);
                        request = QuerySystem.Start(ScrabbleExamQueryIds.LetterTileCount);
                        request.AddParameter(ScrabbleExamQueryIds.LetterParam, Letters[randomNum]);
                        tileCount = QuerySystem.Complete<IntReactiveProperty>();
                        if (tileCount.Value > 0)
                        {

                            tileCount.Value--;
                            TileData newLetter = new TileData();
                            newLetter.Letter = Letters[randomNum];
                            IQueryRequest pointsRequest = QuerySystem.Start(ScrabbleExamQueryIds.LetterPoints);
                            pointsRequest.AddParameter(ScrabbleExamQueryIds.LetterParam, Letters[randomNum]);
                            int letterPoint = QuerySystem.Complete<int>();
                            newLetter.Score = letterPoint;
                            newLetter.TileFont = chosenSkin.TileFont;
                            newLetter.TileTexture = chosenSkin.TileTexture;
                            newLetter.TileFontColor = chosenSkin.TileFontColor;
                            tile.GetComponent<TileUI>().SetTileData(newLetter);
                            slotUI.SlotData.TilePlaced = newLetter;
                            slotUI.SlotData.HasTile = true;
                            break;
                        }

                    } while (tileCount.Value <= 0);
                }
            }
        }

        private int GetFreeSlots()
        {
            int freeSlots = 0;
            foreach(GameObject slot in RackSlots)
            {
                if(!slot.GetComponent<SlotUI>().SlotData.HasTile)
                {
                    freeSlots++;
                }
            }

            return freeSlots;
        }

        void HandleDeviceOrientation(DeviceOrientation orientation)
        {
            switch (orientation)
            {
                case DeviceOrientation.LandscapeLeft:
                case DeviceOrientation.LandscapeRight:
                    transform.position = new Vector3(13.5f, 0f, 0f);
                    MoveSlotsToPosition(DeviceOrientation.LandscapeLeft);
                    break;
                case DeviceOrientation.Portrait:
                case DeviceOrientation.PortraitUpsideDown:
                    transform.position = new Vector3(0f, -13.5f, 0f);
                    MoveSlotsToPosition(DeviceOrientation.Portrait);
                    break;
            }
        }

        void MoveSlotsToPosition(DeviceOrientation orientation)
        {
            for(int i = 0; i< RackSlots.Count; i++)
            {
                RackSlots[i].transform.position = SlotPlaces.Positions[orientation][i];
                SlotData slotData = RackSlots[i].GetComponent<SlotUI>().SlotData;
                if (slotData.HasTile)
                {
                    slotData.TilePlaced.MsgBroker.Publish(new UpdateTilePosition() { Position = SlotPlaces.Positions[orientation][i] });
                }
            }
        }
    }
}
