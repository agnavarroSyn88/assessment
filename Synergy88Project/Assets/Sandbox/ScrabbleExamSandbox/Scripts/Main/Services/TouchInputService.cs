﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Synergy88;
using FullInspector;
using Common.Utils;
using Common.Query;

namespace Sandbox.TouchInput
{
    public class TouchInputService : BaseBehavior
    {
        [SerializeField]
        Camera GameCamera;

        ReactiveProperty<Rect> pixelRect = new ReactiveProperty<Rect>();

        TKPinchRecognizer PinchRecognizer;
        TKPanRecognizer PanRecognizer;
        TKTouchPadRecognizer TouchPadRecognizer;

        private void InitializeTouchKit()
        {
            TouchKit.removeAllGestureRecognizers();
            AddPinchRecognizer();
            AddPanRecognizer();
            AddTouchPadRecognizer();


        }

        protected override void Awake()
        {
            base.Awake();
            Factory.Register<TouchInputService>(this);
            GameCamera = QuerySystem.Query<Camera>(QueryIds.SystemCamera);
            pixelRect = QuerySystem.Query<ReactiveProperty<Rect>>(QueryIds.CameraPixelRect);

            InitializeTouchKit();

            pixelRect.Subscribe(_ => InitializeTouchKit()).AddTo(this);
        }


        protected void OnDestroy()
        {
            Factory.Clean<TouchInputService>();

            TouchKit.removeAllGestureRecognizers();
        }

        void Update()
        {
            if(Input.mouseScrollDelta.magnitude >0)
                MessageBroker.Default.Publish(new PinchSignal() { Scale = Input.GetAxis("Mouse ScrollWheel")});

            
        }

        private void AddPinchRecognizer()
        {
            PinchRecognizer = new TKPinchRecognizer();
            PinchRecognizer.boundaryFrame = new TKRect(GameCamera.pixelRect.width, GameCamera.pixelRect.height, GameCamera.pixelRect.center);

            PinchRecognizer.gestureRecognizedEvent += (r) =>
            {
                MessageBroker.Default.Publish(new PinchSignal { Scale = PinchRecognizer.deltaScale });
                Debug.Log("pinch recognizer fired: " + r);
            };
            TouchKit.addGestureRecognizer(PinchRecognizer);
        }

        private void AddPanRecognizer()
        {
            PanRecognizer = new TKPanRecognizer();
            PanRecognizer.boundaryFrame = new TKRect(GameCamera.pixelRect.width, GameCamera.pixelRect.height, GameCamera.pixelRect.center);

            if (Application.platform == RuntimePlatform.IPhonePlayer)
                PanRecognizer.minimumNumberOfTouches = 2;

            PanRecognizer.gestureRecognizedEvent += (r) =>
            {
                MessageBroker.Default.Publish(new PanSignal { Translation = PanRecognizer.deltaTranslation });
                Debug.Log("pan recognizer fired: " + r);
            };

            // continuous gestures have a complete event so that we know when they are done recognizing
            PanRecognizer.gestureCompleteEvent += r =>
            {
                Debug.Log("pan gesture complete");
            };
            TouchKit.addGestureRecognizer(PanRecognizer);
        }

        private void AddTouchPadRecognizer()
        {
            TKRect frame = new TKRect(GameCamera.pixelRect.width, GameCamera.pixelRect.height, GameCamera.pixelRect.center);
            TouchPadRecognizer = new TKTouchPadRecognizer(frame);

            TouchPadRecognizer.gestureRecognizedEvent += (r) =>
            {

                MessageBroker.Default.Publish(new TouchPadSignal()
                {
                    TouchPosition = TouchPadRecognizer.touchLocation()
                });
                Debug.Log("touchpad recognizer fired: " + r);
            };

            TouchPadRecognizer.gestureCompleteEvent += r =>
            {
                MessageBroker.Default.Publish(new TouchPadSignal()
                {
                    TouchPosition = TouchPadRecognizer.touchLocation()
                });
                Debug.Log("touchpad gesture complete");
            };
            TouchKit.addGestureRecognizer(TouchPadRecognizer);
        }
    }
}
