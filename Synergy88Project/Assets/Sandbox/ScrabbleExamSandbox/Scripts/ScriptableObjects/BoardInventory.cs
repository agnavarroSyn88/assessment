﻿using FullInspector;
using UnityEngine;
using System.Collections;
using UniRx;
using System.Collections.Generic;
using System.Linq;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class BoardInventory : BaseScriptableObject
    {
        public Dictionary<int, List<string>> BoardData = new Dictionary<int, List<string>>();

        public string path = "";

        [InspectorButton("Create New Board")]
        public void CreateBoard()
        {
            List<string> newBoard = new List<string>();

            TextAsset data = Resources.Load<TextAsset>(path);

            char[] textArray = data.text.ToCharArray();

            foreach(char c in textArray)
            {
                if(!c.Equals('\n') && !c.Equals('\r'))
                    newBoard.Add(c.ToString());
            }
            int index = BoardData.Count;

            BoardData.Add(index, newBoard);
        }
    }
}