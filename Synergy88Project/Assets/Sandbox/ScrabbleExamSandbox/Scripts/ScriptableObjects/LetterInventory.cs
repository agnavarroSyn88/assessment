﻿using FullInspector;
using UnityEngine;
using System.Collections;
using UniRx;
using System.Collections.Generic;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class LetterInventory : BaseScriptableObject
    {

        public Dictionary<string, IntReactiveProperty> LettersCount = new Dictionary<string, IntReactiveProperty>();
        public Dictionary<string, int> LettersPoints = new Dictionary<string, int>();
    }
}