﻿using FullInspector;
using UnityEngine;
using System.Collections;
using UniRx;
using System.Collections.Generic;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class MultiplierInventory : BaseScriptableObject
    {
        public Dictionary<string, MultiplierData> Multipliers = new Dictionary<string, MultiplierData>();
    }
}