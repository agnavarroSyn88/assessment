﻿using FullInspector;
using UnityEngine;
using System.Collections;
using UniRx;
using System.Collections.Generic;

namespace Sandbox.ScrabbleExam
{
    public class SkinInventory : BaseScriptableObject
    {
        public Dictionary<string, SkinData> Skins = new Dictionary<string, SkinData>();
    }
}