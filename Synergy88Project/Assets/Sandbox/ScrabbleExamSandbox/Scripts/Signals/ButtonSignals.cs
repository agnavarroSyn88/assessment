﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.ScrabbleExam
{
    public class SubmitWordSignal { }
    public class PassSignal { }
    public class ChangeTiles { }
}
