﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.ScrabbleExam
{

    public class SealBlankTile { }
    public class PlayTile { public TileData TilePlaced; }
    public class UnPlayTile { public TileData TileRemoved; }
    public class ReturnTileToRack
    {
        public Vector3 SlotPosition;
        public Transform SlotParent;
    }
    public class UpdateTilePosition { public Vector3 Position; }
}
