﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;

namespace Sandbox.ScrabbleExam
{
    public class ScoreUI : BaseBehavior
    {
        [SerializeField]
        private Text ScoreText;

        private IntReactiveProperty Score = new IntReactiveProperty(0);

        
        protected override void Awake()
        {
            Score = QuerySystem.Query<IntReactiveProperty>(ScrabbleExamQueryIds.Score);

            Score.Subscribe(_ => UpdateScore(_));
        }

        void UpdateScore(int score)
        {
            ScoreText.text = "Score: " + score.ToString();
        }

    }
}
