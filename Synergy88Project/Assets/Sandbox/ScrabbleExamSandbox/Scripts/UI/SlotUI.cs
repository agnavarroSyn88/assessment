﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using Synergy88;

namespace Sandbox.ScrabbleExam
{
    public class SlotUI : BaseBehavior
    {
        [SerializeField]
        private Image SlotImage;

        [SerializeField]
        private Text MultiplierText;

        [SerializeField]
        private SlotData _SlotData;
        public SlotData SlotData { get { return _SlotData; } }


        // Use this for initialization
        void Start()
        {
            SetSlotUI(_SlotData);

            /*_SlotData.MsgBroker.Receive<PlayTile>()
                .Subscribe(_ =>
                {
                    SetTilePlaced(_.TilePlaced);
                }).AddTo(this);

           */
           
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetTilePlaced(TileData tile)
        {
            _SlotData.TilePlaced = tile;
        }

        public void RemoveTilePlaced()
        {
            _SlotData.TilePlaced = null;
        }


        public void SetSlotUI(SlotData slotData)
        {
            _SlotData = slotData;
            if (_SlotData.IsDisabled)
            {
                gameObject.SetActive(false);
                return;
            }
            SlotImage.sprite = _SlotData.SkinImage;
           
            switch(slotData.Type)
            {
                case SlotType.Grid:
                    MultiplierText.text = slotData.Multiplier.MultiplierLabel;
                    if(slotData.Multiplier.Type == MultiplierType.None)
                    {
                        MultiplierText.gameObject.SetActive(false);
                    }
                    else
                    {
                        MultiplierText.gameObject.SetActive(true);
                    }
                    SlotImage.color = slotData.Multiplier.MultiplierColor;
                    break;
                case SlotType.Rack:
                    MultiplierText.enabled = false;
                    //SlotImage.color = Color.grey;

                    break;
            }
        }
    }
}
