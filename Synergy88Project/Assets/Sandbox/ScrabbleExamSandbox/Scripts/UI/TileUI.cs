﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using Synergy88;

namespace Sandbox.ScrabbleExam
{

    public class TileUI : BaseBehavior
    {
        [SerializeField]
        private Image TileImage;

        [SerializeField]
        private InputField TileLetter;

        [SerializeField]
        private Text TilePoint;

        [SerializeField]
        private TileData _TileData = new TileData();
        public TileData TileData { get { return _TileData; } }

        void Start()
        {

        }


        public void SetTileData(TileData tileData)
        {
            _TileData = tileData;
            TileImage.sprite = _TileData.TileTexture;
            if(_TileData.Letter.Equals(" "))
            {
                TileLetter.interactable = true;
            }
            else
            {
                TileLetter.interactable = false;
            }
            TileLetter.textComponent.font = _TileData.TileFont;
            TileLetter.textComponent.color = _TileData.TileFontColor;
            TileLetter.text = _TileData.Letter;
            TilePoint.font = _TileData.TileFont;
            TilePoint.color = _TileData.TileFontColor;
            TilePoint.text = _TileData.Score.ToString();

            _TileData.MsgBroker.Receive<SealBlankTile>()
               .Subscribe(_ =>
               {
                   Debug.Log("Seal Blank Tile " + _TileData.Letter);
                   TileLetter.interactable = false;
               }).AddTo(this);
            _TileData.MsgBroker.Receive<ReturnTileToRack>()
                .Subscribe(_ =>
                {
                    GetComponent<DragTile>().SendToSlot(_.SlotPosition, _.SlotParent);
                }).AddTo(this);
            _TileData.MsgBroker.Receive<UpdateTilePosition>()
                .Subscribe(_ =>
                {
                    GetComponent<DragTile>().UpdateStartingPosition(_.Position);
                }).AddTo(this);

        }

        public void ChangeLetter(string value)
        {
            _TileData.Letter = TileLetter.text;
           
        }
    }
}