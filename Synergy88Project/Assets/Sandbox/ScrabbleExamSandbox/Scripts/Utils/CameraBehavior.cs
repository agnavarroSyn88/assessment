﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Synergy88;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using System;
using Sandbox.TouchInput;

namespace Sandbox.ScrabbleExam
{
    public class CameraBehavior : BaseBehavior
    {
        [SerializeField]
        private Camera GameCamera;

        [SerializeField]
        private bool IsDebug;

        private float MaxOrthographicSize = 21;
        private float MinOrthographicSize = 8f;
        private int SCALE_MULTIPLIER = 5;

        [SerializeField]
        private Vector3 MaxPanCoordinates;
        [SerializeField]
        private Vector3 MinPanCoordinates;

        private ReactiveProperty<DeviceOrientation> CurrentOrientation = new ReactiveProperty<DeviceOrientation>();
        protected override void Awake()
        {
            
            base.Awake();
            
            if (!IsDebug)
                GameCamera = QuerySystem.Query<Camera>(QueryIds.SystemCamera);

            CurrentOrientation = QuerySystem.Query<ReactiveProperty<DeviceOrientation>>(QueryIds.CurrentDeviceOrientaion);
            HandleDeviceOrientation(CurrentOrientation.Value);

            GameCamera.orthographicSize = MaxOrthographicSize;
            
            MessageBroker.Default.Receive<PanSignal>()
                .Subscribe(_ =>
                {
                    if (DragTile.ObjectDragged == null)
                    {
                        Vector3 cameraPan = GameCamera.transform.position - new Vector3(_.Translation.x, _.Translation.y) / 80;
                        Vector3 newCameraPos = new Vector3(Mathf.Clamp(cameraPan.x, MinPanCoordinates.x, MaxPanCoordinates.x),
                            Mathf.Clamp(cameraPan.y, MinPanCoordinates.y, MaxPanCoordinates.y), GameCamera.transform.position.z);
                        GameCamera.transform.position = newCameraPos;
                    }
                }).AddTo(this);

            MessageBroker.Default.Receive<PinchSignal>()
                .Subscribe(_ =>
                {
                    
                    float cameraOrthoSize = Mathf.Clamp(GameCamera.orthographicSize - (_.Scale * SCALE_MULTIPLIER), MinOrthographicSize, MaxOrthographicSize);
                    GameCamera.orthographicSize = cameraOrthoSize;
                }).AddTo(this);

            CurrentOrientation
               .Subscribe(_ =>
               {
                   HandleDeviceOrientation(_);
               }).AddTo(this);
        }

        void HandleDeviceOrientation(DeviceOrientation orientation)
        {
            switch (orientation)
            {
                case DeviceOrientation.LandscapeLeft:
                case DeviceOrientation.LandscapeRight:
                    MaxOrthographicSize = 15.5f;
                    MinOrthographicSize = 5f;
                    GameCamera.orthographicSize = MaxOrthographicSize;
                    break;
                case DeviceOrientation.Portrait:
                case DeviceOrientation.PortraitUpsideDown:
                    MaxOrthographicSize = 21;
                    MinOrthographicSize = 8f;
                    GameCamera.orthographicSize = MaxOrthographicSize;
                    break;
            }
        }

        protected void OnDestroy()
        {

        }
    }
}