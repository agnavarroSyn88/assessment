﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Common.Query;
using Common.Utils;
using FullInspector;
using Synergy88;
using Sandbox.TouchInput;

namespace Sandbox.ScrabbleExam
{
    public class DragTile : BaseBehavior
    { 
        [HideInInspector]
        public static GameObject ObjectDragged = null;

        [SerializeField]
        private Camera _GameCamera;
        public Camera GameCamera { get { return _GameCamera; } set { _GameCamera = value; } }
        // Use this for initialization

        private Vector3 startingPosition;
        private Transform myParent;

        private const string SLOT_TAG = "Slot";
        private const string TILE_TAG = "Tile";

        [SerializeField]
        private List<GameObject> TouchedSlots;

        private Vector2 TouchInput;
        void Start()
        {
            GameCamera = QuerySystem.Query<Camera>(QueryIds.SystemCamera);
            startingPosition = transform.position;
            myParent = transform.parent;
            TouchedSlots = new List<GameObject>();
            MessageBroker.Default.Receive<TouchPadSignal>()
                .Subscribe(_ =>
                {
                    TouchInput = _.TouchPosition;
                    //Debug.Log("Touch Pos: " + TouchInput.ToString());
                }).AddTo(this);
        }


        void OnMouseDown()
        {
            if (GetComponent<TileUI>().TileData.IsTileUsable)
            {
                ObjectDragged = gameObject;
                myParent.GetComponent<SlotUI>().SlotData.HasTile = false;
            }
        }

        void OnMouseDrag()
        {
            if (GetComponent<TileUI>().TileData.IsTileUsable)
            {
                transform.position = _GameCamera.ScreenToWorldPoint(new Vector3(TouchInput.x, TouchInput.y, 1f));
            }
        }

        void OnMouseUp()
        {
            if (GetComponent<TileUI>().TileData.IsTileUsable)
            {
                Drop();
            }
        }

        public void Drop()
        {
            transform.localScale = new Vector3(1, 1, 1);
            //gameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;

            Debug.Log("touched Slots: " + TouchedSlots.Count.ToString());
            ObjectDragged = null;
            Vector3 newPosition;
            SlotData parentSlot;
            if (TouchedSlots.Count == 0)
            {
                transform.position = startingPosition;
                transform.parent = myParent;
                parentSlot = transform.parent.GetComponent<SlotUI>().SlotData;
                parentSlot.TilePlaced = transform.GetComponent<TileUI>().TileData;
                parentSlot.HasTile = true;
                parentSlot.TilePlaced.TileIndex = parentSlot.Index;
                if(parentSlot.Type == SlotType.Grid)
                {
                    MessageBroker.Default.Publish(new PlayTile() { TilePlaced = parentSlot.TilePlaced });
                }
                else if(parentSlot.Type == SlotType.Rack)
                {
                    MessageBroker.Default.Publish(new UnPlayTile() { TileRemoved = parentSlot.TilePlaced });
                }
                return;
            }

            GameObject currentCell = TouchedSlots[0];
            if (TouchedSlots.Count == 1)
            {
                newPosition = currentCell.transform.position;
            }
            else
            {
                var distance = Vector3.Distance(transform.position,TouchedSlots[0].transform.position);

                foreach (GameObject cell in TouchedSlots)
                {
                    if (Vector3.Distance(transform.position, cell.transform.position) < distance)
                    {
                        currentCell = cell;
                        distance = Vector3.Distance(transform.position, cell.transform.position);
                    }
                }
                newPosition = currentCell.transform.position;
                
            }
            if (currentCell.transform.childCount > 1)
            {
                transform.position = startingPosition;
                transform.parent = myParent;
                parentSlot = transform.parent.GetComponent<SlotUI>().SlotData;
                parentSlot.TilePlaced = transform.GetComponent<TileUI>().TileData;
                parentSlot.HasTile = true;
                parentSlot.TilePlaced.TileIndex = parentSlot.Index;
                if (parentSlot.Type == SlotType.Grid)
                {
                    MessageBroker.Default.Publish(new PlayTile() { TilePlaced = parentSlot.TilePlaced });
                }
                else
                {
                    MessageBroker.Default.Publish(new UnPlayTile() { TileRemoved = parentSlot.TilePlaced });
                }
                return;
            }
            else
            {
                transform.parent = currentCell.transform;
                startingPosition = newPosition;
                myParent = transform.parent;
                parentSlot = transform.parent.GetComponent<SlotUI>().SlotData;
                parentSlot.TilePlaced = transform.GetComponent<TileUI>().TileData;
                parentSlot.HasTile = true;
                parentSlot.TilePlaced.TileIndex = parentSlot.Index;
                if (parentSlot.Type == SlotType.Grid)
                {
                    MessageBroker.Default.Publish(new PlayTile() { TilePlaced = parentSlot.TilePlaced });
                }
                else
                {
                    MessageBroker.Default.Publish(new UnPlayTile() { TileRemoved = parentSlot.TilePlaced });
                }
                StartCoroutine(SlotIntoPlace(transform.position, (newPosition - Vector3.forward )));
            }

        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == SLOT_TAG)
            {
                if (!TouchedSlots.Contains(other.gameObject))
                {
                    TouchedSlots.Add(other.gameObject);
                }
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == SLOT_TAG)
            {
                if (TouchedSlots.Contains(other.gameObject))
                {
                    TouchedSlots.Remove(other.gameObject);
                }
            }
        }

        public void SendToSlot(Vector3 position, Transform parent)
        {
            myParent = parent;
            startingPosition = position;
            transform.parent = parent;
            SlotData parentSlot = parent.GetComponent<SlotUI>().SlotData;
            parentSlot.TilePlaced = transform.GetComponent<TileUI>().TileData;
            parentSlot.HasTile = true;
            parentSlot.TilePlaced.TileIndex = parentSlot.Index;
            StartCoroutine(SlotIntoPlace(transform.position, position - Vector3.forward));

        }

        public void UpdateStartingPosition(Vector3 position)
        {
            startingPosition = position;
        }

        IEnumerator SlotIntoPlace(Vector3 startingPos, Vector3 endingPos)
        {
            float duration = 0.1f;
            float elapsedTime = 0;
            //audSource.Play();
            while (elapsedTime < duration)
            {
                transform.position = Vector3.Lerp(startingPos, endingPos, elapsedTime / duration);
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            transform.position = endingPos;
        }
    }
}

