﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UniRx;
using Common.Utils;
using Common.Query;
using Synergy88;
using FullInspector;

namespace Sandbox.ScrabbleExam
{
    [System.Serializable]
    public class SlotPositions :BaseBehavior
    {
        public Dictionary<DeviceOrientation, List<Vector3>> Positions = new Dictionary<DeviceOrientation, List<Vector3>>();

        protected override void Awake()
        {
            base.Awake();

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}