﻿using Sandbox.Spawner;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreateSpawnData  {


    [MenuItem("Assets/Create/SpawnData")]
    public static void CreateSpawnDataObj()
    {
        ScriptableObjectUtility.CreateAsset<SpawnData>();
    }
}
