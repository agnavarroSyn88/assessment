﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
namespace Sandbox.Spawner
{
    public class SpawnerBehaviour : BaseBehavior
    {

        [SerializeField, InspectorOrder(0)]
        private SpawnData _SpawnData;
        public SpawnData SpawnData
        {
            get { return _SpawnData; }
        }

        [SerializeField, InspectorOrder(2)]
        private Vector3 MoveLocation;

        [InspectorButton, InspectorOrder(1), InspectorTooltip("Updates Spawn Data Position using this objects Current Position")]
        private void UpdateLocationData()
        {
            SpawnData.SpawnLocation = transform.position;
        }
        [InspectorButton, InspectorOrder(3), InspectorTooltip("Moves Current object to specified position")]
        private void MoveToLocation()
        {
            transform.position = MoveLocation;
        }

        void Start()
        {

        }


        void Update()
        {

        }
    }
}