﻿using Common.Utils;
using Sandbox.Character;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using FullInspector;

namespace Sandbox.Spawner
{
    public class SpawnerPoolService : MonoBehaviour
    {

        [SerializeField]
        private Transform _PoolParent;

        [SerializeField]
        private Dictionary<int, SpawnData> _SpawnDataCollection;

        [SerializeField]
        private InputField _IdField;

        private void Awake()
        {
            Factory.Register<SpawnerPoolService>(this);

            _SpawnDataCollection = new Dictionary<int, SpawnData>();
            foreach (Transform child in _PoolParent)
            {
                SpawnData newSpawnData = child.GetComponent<SpawnerBehaviour>().SpawnData;
                _SpawnDataCollection.Add(newSpawnData.SpawnID, newSpawnData);
            }
        }
        private void OnDestroy()
        {

            Factory.Clean<SpawnerPoolService>();
        }

        private MessageBroker _MessageBroker = new MessageBroker();
        public MessageBroker GetMsgBroker
        {
            get{    return _MessageBroker;  }
        }
        public void CreateCharacterDispatch()
        {
            if (_IdField.text == "")
                return;

            _MessageBroker.Publish(new RequestCharacterSpawnSignal()
            {
                SpawnData = _SpawnDataCollection[int.Parse(_IdField.text)]
            }
            );
        }
    }
}
