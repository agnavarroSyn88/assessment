﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FullInspector;
//using UnityEditor;

namespace Sandbox.Spawner
{
    public class SpawnData : BaseScriptableObject
    {

        public Vector3 SpawnLocation;
        public int SpawnID;
        public SpawnType SpawnType;
    }
    public enum SpawnType
    {
        Character,
        Object
    }
}