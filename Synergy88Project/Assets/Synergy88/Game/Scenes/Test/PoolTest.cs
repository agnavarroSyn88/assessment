﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using FullInspector;

using Common;
using Common.Utils;
using System.Reflection;

namespace Test
{
    public class PoolTest : BaseNetworkBehavior
    {
        [SerializeField]
        private PrefabManager Pool;

        [SerializeField]
        private List<SwarmItem> ActiveItems;

        protected override void Awake()
        {
            base.Awake();

            Assertion.AssertNotNull(Pool);
        }

        [InspectorButton]
        public void Create()
        {
            ActiveItems.Add(Pool.Request("Char1").GetComponent<SwarmItem>());
        }

        [InspectorButton]
        public void Destroy()
        {
            if (ActiveItems.Count >= 0)
            {
                SwarmItem item = ActiveItems.FirstOrDefault();
                item.Kill();
                ActiveItems.Remove(item);
            }
        }
    }
}
