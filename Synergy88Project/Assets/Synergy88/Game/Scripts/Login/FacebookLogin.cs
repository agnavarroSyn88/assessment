﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Facebook.Unity;

using MiniJSON;

using UniRx;

using Common.Fsm;
using Common.Query;
using Common.Signal;
using Common.Utils;

namespace Synergy88
{
    public class FacebookLogin : SignalComponent, IService
    {
        // Facebook Keys
        private static readonly string FB_ID_KEY = "id";
        private static readonly string FB_NAME_KEY = "name";
        private static readonly string FB_LAST_NAME_KEY = "last_name";
        private static readonly string FB_FIRST_NAME_KEY = "first_name";
        private static readonly string FB_EMAIL = "email";
        private static readonly string FB_PROFILE_PHOTO = "picture";
        private static readonly string FB_PROFILE_PHOTO_DATA = "data";
        private static readonly string FB_PROFILE_PHOTO_URL = "url";
        private static readonly string FB_USER_BIRTHDAY_KEY = "birthday";
        private static readonly string FB_USER_GENDER_KEY = "gender";

        // Prefs Keys
        private static readonly string FACEBOOK_EMAIL_KEY = "facebookEmailKey";
        private static readonly string FACEBOOK_ID_KEY = "facebookIdKey";
        private static readonly string FACEBOOK_FIRST_NAME = "facebookFistname";
        private static readonly string FACEBOOK_FULL_NAME = "facebookFullname";
        private static readonly string FACEBOOK_PROFILE_PHOTO = "facebookProfilePhoto";
        private static readonly string FACEBOOK_USER_GENDER = "facebookUserGender";
        private static readonly string FACEBOOK_USER_BIRTHDAY = "facebookUserBirthday";
        
        [SerializeField]
        //private string fbId = "10207031375695482";
        private string FbId = string.Empty;

        [SerializeField]
        //private string fullName = "Aries Sanchez Sulit";
        private string FullName = string.Empty;

        [SerializeField]
        //private string fullName = "Aries Sanchez Sulit";
        private string FirstName = string.Empty;

        [SerializeField]
        //private string fullName = "Aries Sanchez Sulit";
        private string LastName = string.Empty;

        [SerializeField]
        private string ProfilePhoto = string.Empty;

        [SerializeField]
        //private string email = "aries.sulit@email.com";
        private string Email = string.Empty;

        [SerializeField]
        //private string email = "01/01/1970";
        private string Birthday = string.Empty;

        [SerializeField]
        //private string email = "male";
        private string Gender = string.Empty;

        [SerializeField]
        private List<string> Permissions;

        [SerializeField]
        private List<Permission> GrantedPermissions;

        private Dictionary<string, bool> GrantedPermissionMap;

        // permissions
        [SerializeField]
        private readonly List<string> PERMISSIONS = new List<string>(){
            "public_profile",
            "email",
            "user_friends",
            "user_birthday"
        };

        #region IService implementation
        [SerializeField]
        private bool _IsServiceRequired;
        public bool IsServiceRequired
        {
            get
            {
                return _IsServiceRequired;
            }
        }

        public string ServiceName { get { return name; } }

        private ReactiveProperty<ServiceState> _CurrentServiceState = new ReactiveProperty<ServiceState>(ServiceState.Uninitialized);
        public ReactiveProperty<ServiceState> CurrentServiceState
        {
            get
            {
                return _CurrentServiceState;
            }
        }

        public void InitializeService()
        {
            // init permissions
            Permissions = new List<string>();
            GrantedPermissions = new List<Permission>();
            GrantedPermissionMap = new Dictionary<string, bool>();

            Func<string, string> GetStoredData = (string key) => {
                string value = PlayerPrefs.GetString(key, string.Empty);
                return value;
            };

            // load from defaults
            FbId = GetStoredData(FACEBOOK_ID_KEY);
            Email = GetStoredData(FACEBOOK_EMAIL_KEY);
            FirstName = GetStoredData(FACEBOOK_FIRST_NAME);
            FullName = GetStoredData(FACEBOOK_FULL_NAME);
            ProfilePhoto = GetStoredData(FACEBOOK_PROFILE_PHOTO);
            Birthday = GetStoredData(FACEBOOK_USER_BIRTHDAY);
            Gender = GetStoredData(FACEBOOK_USER_GENDER);

            PrepareFsm();
            
            QuerySystem.RegisterResolver(QueryIds.HasLoggedInUser, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(HasLoggedInUser());
            });

            QuerySystem.RegisterResolver(QueryIds.UserEmail, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(Email);
            });

            QuerySystem.RegisterResolver(QueryIds.UserFacebookId, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(FbId);
            });

            QuerySystem.RegisterResolver(QueryIds.UserFirstName, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(FirstName);
            });

            QuerySystem.RegisterResolver(QueryIds.UserFullName, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(FullName);
            });

            QuerySystem.RegisterResolver(QueryIds.UserProfilePhoto, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(ProfilePhoto);
            });

            QuerySystem.RegisterResolver(QueryIds.UserGender, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(Gender);
            });

            QuerySystem.RegisterResolver(QueryIds.UserBirthday, (IQueryRequest request, IMutableQueryResult result) => {
                result.Set(Birthday);
            });
            
            Receive<OnFacebookLoginSignal>()
                .Subscribe(sig => OnFacebookLogin())
                .AddTo(this);

            Receive<OnFacebookLogoutSignal>()
                .Subscribe(sig => OnFacebookLogout())
                .AddTo(this);
        }

        public void TerminateService()
        {
            QuerySystem.RemoveResolver(QueryIds.HasLoggedInUser);
            QuerySystem.RemoveResolver(QueryIds.UserEmail);
            QuerySystem.RemoveResolver(QueryIds.UserFacebookId);
            QuerySystem.RemoveResolver(QueryIds.UserFirstName);
            QuerySystem.RemoveResolver(QueryIds.UserFullName);
            QuerySystem.RemoveResolver(QueryIds.UserProfilePhoto);
            QuerySystem.RemoveResolver(QueryIds.UserGender);
            QuerySystem.RemoveResolver(QueryIds.UserBirthday);
        }
        #endregion
        
        private void Update()
        {
            if (Fsm != null)
            {
                Fsm.Update();
            }
        }

        #region Facebook Fsm

        // initial state
        private const string IDLE = "Idle";

        // events
        private const string INIT_FB = "InitFb";
        private const string FINISHED = "Finished";
        private const string LOGOUT_REQUESTED = "LogoutRequested";
        private const string LOGIN_SUCCESS = "LoginSuccess";
        private const string LOGIN_FAILED = "LoginFailed";
        private const string SUCCESS = "Success";
        private const string FAILED = "Failed";
        private const string REQUEST_FAILED = "RequestFailed";

        private Fsm Fsm;

        private void PrepareFsm()
        {
            Fsm = new Fsm("FacebookLogin");

            // states
            FsmState idle = Fsm.AddState(IDLE);
            FsmState init = Fsm.AddState("Init");
            FsmState login = Fsm.AddState("Login");
            FsmState requestPlayerData = Fsm.AddState("RequestPlayerData");
            FsmState loginSuccessful = Fsm.AddState("LoginSuccessful");
            FsmState loginFailed = Fsm.AddState("loginFailed");
            FsmState requestFailed = Fsm.AddState("requestFailed");
            FsmState logout = Fsm.AddState("Logout");

            idle.AddAction(new FsmDelegateAction(idle, delegate (FsmState owner) {
                owner.SendEvent(INIT_FB);
            }));

            // actions
            init.AddAction(new FsmDelegateAction(init, 
                delegate (FsmState owner) {
                    Debug.LogFormat("FacebookLogin::PrepareFsm State:{0}\n", owner.GetName());

                    if (FB.IsInitialized)
                    {
                        // already initialized
                        owner.SendEvent(FINISHED);
                        return;
                    }
                    else
                    {
                        FB.Init(OnInitComplete, OnHideUnity);
                    }
                }, 
                delegate (FsmState owner) {
                    // OnUpdate()
                    // check if FB is initialized and send Finished event
                    if (FB.IsInitialized)
                    {
                        owner.SendEvent(FINISHED);
                    }
                }));

            login.AddAction(new FsmDelegateAction(login, 
                delegate (FsmState owner) {
                    Debug.LogFormat("FacebookLogin::PrepareFsm State:{0}\n", owner.GetName());

                    if (FB.IsLoggedIn)
                    {
                        // already logged in
                        owner.SendEvent(FINISHED);
                        return;
                    }

                    DoFbLogin();
                }));

            requestPlayerData.AddAction(new FsmDelegateAction(requestPlayerData, delegate (FsmState owner) {
                Debug.LogFormat("FacebookLogin::PrepareFsm State:{0}\n", owner.GetName());
                //FB.API("/me", HttpMethod.GET, HandlePlayerDataResult);
                //FB.API("/me?fields=email,name,picture", HttpMethod.GET, HandlePlayerDataResult);
                FB.API("me?fields=email,name,first_name,last_name,picture.height(713).width(713),gender,birthday", HttpMethod.GET, HandlePlayerDataResult);
            }));

            loginSuccessful.AddAction(new FsmDelegateAction(loginSuccessful, delegate (FsmState owner) {
                Debug.LogFormat("FacebookLogin::PrepareFsm State:{0}\n", owner.GetName());
                CurrentServiceState.Value = ServiceState.Initialized;
            }));

            loginFailed.AddAction(new FsmDelegateAction(loginFailed, delegate (FsmState owner) {
                Debug.LogFormat("FacebookLogin::PrepareFsm State:{0}\n", owner.GetName());
                Publish(new OnFacebookLoginFailedSignal());
                owner.SendEvent(FINISHED);
                CurrentServiceState.Value = ServiceState.Error;
            }));

            logout.AddAction(new FsmDelegateAction(logout, delegate (FsmState owner) {
                Debug.LogFormat("FacebookLogin::PrepareFsm State:{0}\n", owner.GetName());
                DoFbLogout();
                Fsm.SendEvent(IDLE);
            }));

            requestFailed.AddAction(new FsmDelegateAction(requestFailed, delegate (FsmState owner) {
                Fsm.SendEvent(FINISHED);
            }));

            // transitions
            idle.AddTransition(INIT_FB, init);

            init.AddTransition(FINISHED, login);

            login.AddTransition(LOGIN_SUCCESS, requestPlayerData);
            login.AddTransition(FINISHED, requestPlayerData);
            login.AddTransition(LOGIN_FAILED, loginFailed);

            loginFailed.AddTransition(FINISHED, idle);

            requestPlayerData.AddTransition(FAILED, requestPlayerData); // request again if it failed
            requestPlayerData.AddTransition(SUCCESS, loginSuccessful);
            requestPlayerData.AddTransition(REQUEST_FAILED, requestFailed);

            requestFailed.AddTransition(FINISHED, logout);

            loginSuccessful.AddTransition(LOGOUT_REQUESTED, logout);
            loginSuccessful.AddTransition(LOGIN_SUCCESS, requestPlayerData);

            logout.AddTransition(IDLE, idle);

            // auto start fsm
            Fsm.Start(IDLE);
        }

        #endregion

        #region Signals

        private void OnFacebookLogin()
        {
            Email = string.Empty;
            FullName = string.Empty;

            // start the login process by starting the FSM
            if (!FB.IsLoggedIn)
            {
                Fsm.SendEvent(INIT_FB);
            }
            else
            {
                Fsm.SendEvent(LOGIN_SUCCESS);
            }
        }

        private void OnFacebookLogout()
        {
            Fsm.SendEvent(LOGOUT_REQUESTED);
        }

        #endregion

        private void DoFbLogin()
        {
            FB.LogInWithReadPermissions(PERMISSIONS, HandleLoginResult);
        }

        private void DoFbLogout()
        {
            Publish(new OnFacebookLogoutSuccessfulSignal()
            {
                FacebookId = FbId
            });

            PlayerPrefs.DeleteKey(FacebookLogin.FACEBOOK_EMAIL_KEY);
            PlayerPrefs.DeleteKey(FacebookLogin.FACEBOOK_ID_KEY);

            FB.LogOut();

            // Delete all keys
            PlayerPrefs.DeleteAll();

            // Clear cached values
            FbId = string.Empty;
            FullName = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            ProfilePhoto = string.Empty;
            Email = string.Empty;
            Birthday = string.Empty;
            Gender = string.Empty;
        }

        private void HandleLoginResult(IResult result)
        {
            if (result == null)
            {
                Debug.Log("FacebookLogin::HandleLoginResult Null Result\n");
                Fsm.SendEvent(LOGIN_FAILED);
                return;
            }

            // Some platforms returns an empty string instead of null.
            if (!string.IsNullOrEmpty(result.Error))
            {
                Debug.LogFormat("FacebookLogin::HandleLoginResult Login Error: {0}\n", result.Error);
                Fsm.SendEvent(LOGIN_FAILED);
            }
            else if (result.Cancelled)
            {
                Debug.LogFormat("FacebookLogin::HandleLoginResult Login Cancelled: {0}\n", result.RawResult);
                Fsm.SendEvent(LOGIN_FAILED);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                Debug.LogFormat("FacebookLogin::HandleLoginResult Login Success: {0}\n", result.RawResult);
                ProcessPermissions(result.RawResult);
                Fsm.SendEvent(LOGIN_SUCCESS);
            }
            else
            {
                Debug.Log("FacebookLogin::HandleLoginResult Login Empty Response\n");
                Fsm.SendEvent(LOGIN_FAILED);
            }
        }

        private void HandlePlayerDataResult(IResult result)
        {
            if (result == null)
            {
                Debug.Log("FacebookLogin::HandlePlayerDataResult Null Result");
                Fsm.SendEvent(FAILED);
                return;
            }

            // Some platforms return the empty string instead of null.
            if (!string.IsNullOrEmpty(result.Error))
            {
                Debug.LogFormat("FacebookLogin::HandlePlayerDataResult Error: {0}\n", result.Error);
                Fsm.SendEvent(FAILED);
            }
            else if (result.Cancelled)
            {
                Debug.LogFormat("FacebookLogin::HandlePlayerDataResult  Cancelled: {0}", result.RawResult);
                Fsm.SendEvent(FAILED);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                Debug.LogFormat("FacebookLogin::HandlePlayerDataResult  Success: {0}\n", result.RawResult);
                SavePlayerData(result.RawResult);
                Fsm.SendEvent(SUCCESS);
            }
            else
            {
                Debug.Log("FacebookLogin::HandlePlayerDataResult  Empty Response\n");
                Fsm.SendEvent(FAILED);
            }
        }

        //	{
        //		"email": "aries.sulit@gmail.com",
        //		"name": "Aries Sanchez Sulit",
        //		"picture": {
        //			"data": {
        //				"height": 713,
        //				"is_silhouette": false,
        //				"url": "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xlp1/v/t1.0-1/10410893_10204493882219731_2253049558551334393_n.jpg?oh=740d9b8d9d205b26ed702eaee791b20c&oe=57417837&__gda__=1463505422_daa58978b4952292a425b7a10c9025d1",
        //				"width": 713
        //			}
        //		},
        //		"id": "10204344991737562"
        //	}

        private void SavePlayerData(string rawResult)
        {
            Debug.LogFormat("FacebookLogin::SavePlayerData RawResult:{0}\n", rawResult);

            Dictionary<string, object> root = (Dictionary<string, object>)Json.Deserialize(rawResult);

            root.TryGetValue(FB_ID_KEY, out FbId);
            root.TryGetValue(FB_NAME_KEY, out FullName);
            root.TryGetValue(FB_FIRST_NAME_KEY, out FirstName);
            root.TryGetValue(FB_LAST_NAME_KEY, out LastName);
            root.TryGetValue(FB_EMAIL, out Email);
            root.TryGetValue(FB_USER_BIRTHDAY_KEY, out Birthday);
            root.TryGetValue(FB_USER_GENDER_KEY, out Gender);
            root.TryGetValue(FB_PROFILE_PHOTO, out root);
            root.TryGetValue(FB_PROFILE_PHOTO_DATA, out root);
            root.TryGetValue(FB_PROFILE_PHOTO_URL, out ProfilePhoto);

            PlayerPrefs.SetString(FACEBOOK_ID_KEY, FbId);
            PlayerPrefs.SetString(FACEBOOK_FULL_NAME, FullName);
            PlayerPrefs.SetString(FACEBOOK_FIRST_NAME, FirstName);
            PlayerPrefs.SetString(FACEBOOK_USER_GENDER, Gender);
            PlayerPrefs.SetString(FACEBOOK_USER_BIRTHDAY, Birthday);
            PlayerPrefs.SetString(FACEBOOK_EMAIL_KEY, Email);
            PlayerPrefs.SetString(FACEBOOK_PROFILE_PHOTO, ProfilePhoto);
        }

        private void OnInitComplete()
        {
            Debug.LogFormat("FacebookLogin::OnInitComplete\n");
        }

        private void OnHideUnity(bool isGameShown)
        {
            Debug.LogFormat("FacebookLogin::OnHideUnity IsShow:{0}\n", isGameShown);
        }

        private bool HasLoggedInUser()
        {
            if (!PlayerPrefs.HasKey(FacebookLogin.FACEBOOK_EMAIL_KEY))
            {
                return false;
            }

            if (PlayerPrefs.HasKey(FacebookLogin.FACEBOOK_ID_KEY))
            {
                Email = PlayerPrefs.GetString(FacebookLogin.FACEBOOK_EMAIL_KEY);
                FbId = PlayerPrefs.GetString(FacebookLogin.FACEBOOK_ID_KEY);
                return true;
            }

            return false;
        }

        #region Utils

        private void ProcessPermissions(string result)
        {
            //			Debug.LogFormat("FacebookLogin::ProcessPermissions Result:{0}\n", result);
            //
            //			Dictionary<string, object> resultData = (Dictionary<string, object>)Json.Deserialize(result);
            //			string[] permissions = GetPermissions(resultData);
            //			List<object> grantedPermissions = GetGrantedPermissions(resultData);
            //
            //			foreach (string permission in permissions) {
            //				permissions.Add(permission);
            //				grantedPermissions.Add(new Permission(permission, false));
            //				grantedPermissionMap.Add(permission, false);
            //			}
            //
            //			foreach (object permission in grantedPermissions) {
            //				string strPerm = (string)permission;
            //				if (!grantedPermissionMap.ContainsKey(strPerm)) {
            //					grantedPermissions.Add(new Permission(strPerm, true));
            //					grantedPermissionMap.Add(strPerm, true);
            //				}
            //				else {
            //					//Permission perm = grantedPermissions.Find(p => p.Type.Equals(strPerm));
            //					//perm.Granted = true;
            //					//grantedPermissions[grantedPermissions.IndexOf(perm)] = perm;
            //					//grantedPermissionMap[strPerm] = true;
            //					Permission holder = null;
            //					for (int index = 0; index < grantedPermissions.Count; index++) {
            //						Permission p = grantedPermissions[index];
            //						if (p.Type.Equals(strPerm)) {
            //							holder = p; 
            //							holder.Granted = true;
            //							grantedPermissions[index] = holder;
            //							break;
            //						}
            //					}
            //
            //					if (holder == null) {
            //						grantedPermissions.Add(new Permission(strPerm, true));
            //					}
            //
            //					grantedPermissionMap[strPerm] = true;
            //				}
            //			}
            //
            //			Debug.LogFormat("FacebookLogin::ProcessPermissions Permissions:{0} GrantedPermissions:{1}", Json.Serialize(permissions), Json.Serialize(grantedPermissionMap));
        }

        private string[] GetPermissions(Dictionary<string, object> result)
        {
            string rawPermissions = (string)result["permissions"];
            return rawPermissions.Split(',');
        }

        // List<string>
        private List<object> GetGrantedPermissions(Dictionary<string, object> result)
        {
            return (List<object>)result["granted_permissions"];
        }

        #endregion

        //
    }

    [Serializable]
    public class Permission
    {
        public string Type;
        public bool Granted;

        public Permission(string type, bool isGranted)
        {
            Type = type;
            Granted = isGranted;
        }
    }

}