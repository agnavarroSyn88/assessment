﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using uPromise;

using UniRx;

using FullInspector;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;

namespace Synergy88
{
    public class ConcreteInstaller : BaseBehavior, IInstaller
    {
        private Scene Scene;

        public virtual void Add()
        {

        }

        public virtual void Install()
        {
            Scene = Scene.GetScene<SystemRoot>(EScene.System);

            Promise.AllSequentially(Scene.EndFramePromise)
                .Then(_ => Scene.LoadScenePromise<PreloaderRoot>(EScene.Preloader))
                .Then(_ => Scene.LoadScenePromise<PopupCollectionRoot>(EScene.PopupCollection))
                .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                .Then(_ => Scene.LoadScenePromise<BackgroundRoot>(EScene.Background))
                .Then(_ => Scene.LoadSceneAdditivePromise<AudioRoot>(EScene.Audio))
                .Then(_ => Scene.LoadSceneAdditivePromise<SplashRoot>(EScene.Splash))
                .Then(_ => Scene.EndFramePromise());
        }

        public virtual void UnInstall()
        {

        }
    }
}