﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using uPromise;

using UniRx;

using FullInspector;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;

namespace Synergy88
{
    public class SceneFlowInstaller : ConcreteInstaller, IInstaller
    {
        // initial state
        private const string IDLE = "Idle";

        // events
        private const string START_SPLASH = "StartSplash";
        private const string START_PRELOAD = "StartPreload";
        private const string START_LOGIN = "StartLogin";
        private const string LOAD_HOME = "LoadHome";
        private const string LOAD_MORE_GAMES = "LOAD_MORE_GAMES";
        private const string LOAD_SHOP = "LOAD_SHOP";
        private const string LOAD_SETTINGS = "LOAD_SETTINGS";
        private const string LOAD_GAME = "LOAD_GAME";
        private const string FINISHED = "Finished";

        private Scene Scene;
        private PreloaderRoot Preloader;
        private PopupCollectionRoot PopupCollection;

        // Fsm
        private Fsm Fsm;

        [SerializeField]
        private EScene _CurrentScene = EScene.Invalid;
        public EScene CurrentScene
        {
            get
            {
                return _CurrentScene;
            }
            private set
            {
                _CurrentScene = value;
            }
        }

        [SerializeField]
        private EScene _PreviousScene = EScene.Invalid;
        public EScene PreviousScene
        {
            get
            {
                return _PreviousScene;
            }
            private set
            {
                _PreviousScene = value;
            }
        }

        private void OnDestroy()
        {
            QuerySystem.RemoveResolver(QueryIds.CurrentScene);
            QuerySystem.RemoveResolver(QueryIds.PreviousScene);
            QuerySystem.RemoveResolver(QueryIds.SystemState);
            QuerySystem.RemoveResolver(QueryIds.Preloader);
            QuerySystem.RemoveResolver(QueryIds.PopupCollection);
        }

        public override void Add()
        {

        }

        public override void Install()
        {
            Scene = Scene.GetScene<SystemRoot>(EScene.System);

            SetupQueries();
            SetupListeners();
            PrepareSceneFsm();
        }

        private void SetupQueries()
        {
            QuerySystem.RegisterResolver(QueryIds.CurrentScene, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(CurrentScene);
            });

            QuerySystem.RegisterResolver(QueryIds.PreviousScene, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(PreviousScene);
            });

            QuerySystem.RegisterResolver(QueryIds.SystemState, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(Fsm.GetCurrentState());
            });

            QuerySystem.RegisterResolver(QueryIds.Preloader, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(Preloader);
            });

            QuerySystem.RegisterResolver(QueryIds.PopupCollection, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(PopupCollection);
            });
        }

        private void SetupListeners()
        {
            // Setup Listeners
            this.Receive<OnLoadSceneSignal>()
                .Subscribe(sig => OnLoadScene(sig.SceneName))
                .AddTo(this);

            this.Receive<LoadSplashSignal>()
                .Subscribe(_ => OnLoadSplash())
                .AddTo(this);

            this.Receive<SplashDoneSignal>()
                .Subscribe(_ => OnSplashDone())
                .AddTo(this);

            this.Receive<ServicesReadySignal>()
                .Subscribe(_ => OnServicesReady())
                .AddTo(this);

            this.Receive<LoadHomeSignal>()
                .Subscribe(_ => OnLoadHome())
                .AddTo(this);

            this.Receive<LoadMoreGamesSignal>()
                .Subscribe(_ => OnLoadMoreGames())
                .AddTo(this);

            this.Receive<LoadShopSignal>()
                .Subscribe(_ => OnLoadShop())
                .AddTo(this);

            this.Receive<LoadSettingsSignal>()
                .Subscribe(_ => OnLoadSettings())
                .AddTo(this);

            this.Receive<LoadGameSignal>()
                .Subscribe(_ => OnLoadGame())
                .AddTo(this);
        }
        
        #region Signals

        /// <summary>
        /// Records the currently and previously loaded scenes.
        /// Does not track Invalid and Cleaner scenes.
        /// </summary>
        private void OnLoadScene(EScene sceneName)
        {
            Debug.LogFormat("{0} OnLoadScene: {1}", Time.time, sceneName);

            if (CurrentScene == EScene.Invalid)
            {
                CurrentScene = sceneName;
            }
            else if (sceneName != EScene.Invalid && sceneName != EScene.Cleaner)
            {
                PreviousScene = CurrentScene;
                CurrentScene = sceneName;
            }
        }

        private void OnLoadSplash()
        {
            Fsm.SendEvent(START_SPLASH);
        }

        private void OnSplashDone()
        {
            Fsm.SendEvent(START_PRELOAD);
        }

        private void OnServicesReady()
        {
            Fsm.SendEvent(START_LOGIN);
        }

        private void OnLoadHome()
        {
            Fsm.SendEvent(LOAD_HOME);
        }

        private void OnLoadMoreGames()
        {
            Fsm.SendEvent(LOAD_MORE_GAMES);
        }

        private void OnLoadShop()
        {
            Fsm.SendEvent(LOAD_SHOP);
        }

        private void OnLoadSettings()
        {
            Fsm.SendEvent(LOAD_SETTINGS);
        }

        private void OnLoadGame()
        {
            Fsm.SendEvent(LOAD_GAME);
        }

        #endregion

        #region Scene Flow Fsm
        private void PrepareSceneFsm()
        {
            Fsm = new Fsm("SceneFsm");

            // states
            FsmState idle = Fsm.AddState(IDLE);
            FsmState splash = Fsm.AddState("splash");
            FsmState preload = Fsm.AddState("preload");
            FsmState login = Fsm.AddState("login");
            FsmState home = Fsm.AddState("home");
            FsmState moreGames = Fsm.AddState("moreGames");
            FsmState shop = Fsm.AddState("shop");
            FsmState settings = Fsm.AddState("settings");
            FsmState game = Fsm.AddState("game");
            FsmState done = Fsm.AddState("done");

            // actions
            idle.AddAction(new FsmDelegateAction(idle, delegate (FsmState owner)
            {
                // Start with splash
                this.Publish(new LoadSplashSignal());
            }));

            splash.AddAction(new FsmDelegateAction(splash, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Scene.LoadScenePromise<PreloaderRoot>(EScene.Preloader))
                    .Then(_ => Scene.LoadScenePromise<PopupCollectionRoot>(EScene.PopupCollection))
                    .Then(_ => PopupCollection = Scene.GetScene<PopupCollectionRoot>(EScene.PopupCollection))
                    .Then(_ => Preloader = Scene.GetScene<PreloaderRoot>(EScene.Preloader))
                    .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading001 | LoadingImages.Loading002 | LoadingImages.Loading003 | LoadingImages.Loading004))
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                    .Then(_ => Scene.LoadScenePromise<BackgroundRoot>(EScene.Background))
                    .Then(_ => Scene.LoadSceneAdditivePromise<AudioRoot>(EScene.Audio))
                    .Then(_ => Scene.LoadSceneAdditivePromise<SplashRoot>(EScene.Splash))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                    .Then(_ => Scene.EndFramePromise());
            }));

            preload.AddAction(new FsmDelegateAction(preload, delegate (FsmState owner)
            {
                // load preloader here
                Scene.LoadScenePromise<ServicesRoot>(EScene.Services);
            }));

            login.AddAction(new FsmDelegateAction(login, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading001))
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<LoginRoot>(EScene.Login))
                    .Then(_ => Scene.WaitPromise(1f))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise());
            }));

            home.AddAction(new FsmDelegateAction(home, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading002))
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                    .Then(_ => Scene.LoadScenePromise<HomeRoot>(EScene.Home))
                    .Then(_ => Scene.LoadSceneAdditivePromise<CurrencyRoot>(EScene.Currency))
                    .Then(_ => Scene.WaitPromise(1f))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                    .Then(_ => Scene.EndFramePromise());
            }));

            moreGames.AddAction(new FsmDelegateAction(moreGames, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading003))
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                    .Then(_ => Scene.LoadScenePromise<MoreGamesRoot>(EScene.MoreGames))
                    .Then(_ => Scene.LoadSceneAdditivePromise<BackRoot>(EScene.Back))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                    .Then(_ => Scene.EndFramePromise());
            }));

            shop.AddAction(new FsmDelegateAction(shop, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Preloader.LoadLoadingScreenPromise(LoadingImages.Loading004))
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                    .Then(_ => Scene.LoadScenePromise<ShopRoot>(EScene.Shop))
                    .Then(_ => Scene.LoadSceneAdditivePromise<CurrencyRoot>(EScene.Currency))
                    .Then(_ => Scene.LoadSceneAdditivePromise<BackRoot>(EScene.Back))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                    .Then(_ => Scene.EndFramePromise());
            }));

            settings.AddAction(new FsmDelegateAction(settings, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Preloader.LoadLoadingScreenPromise())
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                    .Then(_ => Scene.LoadScenePromise<SettingsRoot>(EScene.Settings))
                    .Then(_ => Scene.LoadSceneAdditivePromise<BackRoot>(EScene.Back))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                    .Then(_ => Scene.EndFramePromise());
            }));

            game.AddAction(new FsmDelegateAction(game, delegate (FsmState owner)
            {
                Promise.AllSequentially(Scene.EndFramePromise)
                    .Then(_ => Preloader.LoadLoadingScreenPromise())
                    .Then(_ => Preloader.FadeInLoadingScreenPromise())
                    .Then(_ => Scene.LoadScenePromise<CleanerRoot>(EScene.Cleaner))
                    .Then(_ => Scene.LoadScenePromise<GameRoot>(EScene.Game))
                    .Then(_ => Scene.LoadSceneAdditivePromise<CurrencyRoot>(EScene.Currency))
                    .Then(_ => Preloader.FadeOutLoadingScreenPromise())
                    .Then(_ => Scene.EndFramePromise());
            }));

            done.AddAction(new FsmDelegateAction(done, delegate (FsmState owner)
            {
            }));

            // transitions
            idle.AddTransition(START_SPLASH, splash);

            splash.AddTransition(START_PRELOAD, preload);

            preload.AddTransition(START_LOGIN, login);

            login.AddTransition(LOAD_HOME, home);

            home.AddTransition(LOAD_MORE_GAMES, moreGames);
            home.AddTransition(LOAD_SHOP, shop);
            home.AddTransition(LOAD_SETTINGS, settings);
            home.AddTransition(LOAD_GAME, game);

            moreGames.AddTransition(LOAD_HOME, home);
            shop.AddTransition(LOAD_HOME, home);
            settings.AddTransition(LOAD_HOME, home);
            game.AddTransition(LOAD_HOME, home);

            // auto start fsm
            Fsm.Start(IDLE);
        }

        #endregion
    }
}