﻿using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
using UnityEditor.Callbacks;
using UnityEditor.Advertisements;
#endif

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using uPromise;

using UniRx;

using Common;
using Common.Extensions;
using Common.Fsm;
using Common.Query;
using Common.Signal;

// alias
using CColor = Common.Extensions.Color;

namespace Synergy88
{

    public class SystemRoot : Scene
    {
        // utils
        private static readonly string LOG = CColor.green.LogHeader("[SYSTEMROOT]");

#if UNITY_EDITOR || UNITY_EDITOR_64 || UNITY_EDITOR_OSX
        [PostProcessScene]
        public static void OnPostProcessScene()
        {
            AdvertisementSettings.enabled = true;
            AdvertisementSettings.initializeOnStartup = false;
        }
#endif
        
        [SerializeField]
        private Camera _SystemCamera;
        public Camera SystemCamera
        {
            get
            {
                return _SystemCamera;
            }
            private set
            {
                _SystemCamera = value;
            }
        }

        [SerializeField]
        private SystemVersion _SystemVersion;
        public SystemVersion SystemVersion
        {
            get
            {
                return _SystemVersion;
            }
            private set
            {
                _SystemVersion = value;
            }
        }
        
        #region Unity Life Cycle

        protected override void Awake()
        {
            // Setup DI Queries
            QuerySystem.RegisterResolver(QueryIds.SystemCamera, delegate (IQueryRequest request, IMutableQueryResult result)
            {
                result.Set(SystemCamera);
            });
            
            // Assert cache objects
            Assertion.AssertNotNull(SystemCamera);
            Assertion.AssertNotNull(SystemVersion);

            // Call parent's awake
            base.Awake();

            // prepare Fsm
            //PrepareSceneFsm();

            // Test Install Installers
            Install();
        }

        protected override void OnDestroy()
        {
            QuerySystem.RemoveResolver(QueryIds.SystemCamera);
        }

        #endregion
    }
}