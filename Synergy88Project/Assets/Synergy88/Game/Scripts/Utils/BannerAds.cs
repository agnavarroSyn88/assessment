﻿using UnityEngine;

using System;
using System.Collections;

#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
using GoogleMobileAds;
using GoogleMobileAds.Api;
#endif

using Common;
using Common.Signal;

// alias
using Const = Synergy88.Const;

using UniRx;

namespace Synergy88
{
    public class BannerAds : SignalComponent, IService
    {
#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
        // banner
        private BannerView BannerView;
        private bool BannerToggle = false;
#endif

        #region IService implementation
        [SerializeField]
        private bool _IsServiceRequired;
        public bool IsServiceRequired
        {
            get
            {
                return _IsServiceRequired;
            }
        }

        public string ServiceName
        {
            get
            {
                return name;
            }
        }

        private ReactiveProperty<ServiceState> _CurrentServiceState = new ReactiveProperty<ServiceState>(ServiceState.Uninitialized);
        public ReactiveProperty<ServiceState> CurrentServiceState
        {
            get
            {
                return _CurrentServiceState;
            }
        }

        public void InitializeService()
        {
#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
            Receive<ShowBannerAdsSignal>()
                .Subscribe(_ => OnShowBannerAds())
                .AddTo(this);

            Receive<HideBannerAdsSignal>()
                .Subscribe(_ => OnHideBannerAds())
                .AddTo(this);

            Receive<ToggleBannerAdsSignal>()
                .Subscribe(_ => OnToggleBannerAds())
                .AddTo(this);

            string adUnitId = string.Empty;

            // mobile
            if (Platform.IsMobileAndroid())
            {
                adUnitId = Const.GA_BANNER_AD_UNIT_ID_ANDROID;
            }
            else if (Platform.IsMobileIOS())
            {
                adUnitId = Const.GA_BANNER_AD_UNIT_ID_IOS;
            }
            // editor
            else
            {
                // editor
#if UNITY_ANDROID
                adUnitId = Const.GA_BANNER_AD_UNIT_ID_ANDROID;
#elif UNITY_IOS
				adUnitId = Const.GA_BANNER_AD_UNIT_ID_IOS;
#else
				Assertion.Assert(false, "Unsupported Platform");
#endif
            }

            // create ad
            BannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

            // Create ad request
            AdRequest request = new AdRequest.Builder().Build();

            // Load a banner ad.
            BannerView.LoadAd(request);

            // Register for ad events.
            BannerView.AdLoaded += HandleAdLoaded;
            BannerView.AdFailedToLoad += HandleAdFailedToLoad;
            BannerView.AdOpened += HandleAdOpened;
            BannerView.AdClosing += HandleAdClosing;
            BannerView.AdClosed += HandleAdClosed;
            BannerView.AdLeftApplication += HandleAdLeftApplication;

            // hide banner ads
            OnHideBannerAds();
#endif

            CurrentServiceState.Value = ServiceState.Initialized;
        }

        public void TerminateService()
        {
#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
            // Register for ad events.
            BannerView.AdLoaded -= HandleAdLoaded;
            BannerView.AdFailedToLoad -= HandleAdFailedToLoad;
            BannerView.AdOpened -= HandleAdOpened;
            BannerView.AdClosing -= HandleAdClosing;
            BannerView.AdClosed -= HandleAdClosed;
            BannerView.AdLeftApplication -= HandleAdLeftApplication;
#endif
        }
        #endregion

#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)

        #region Signals

        private void OnShowBannerAds()
        {
            BannerView.Show();
            BannerToggle = true;
        }

        private void OnHideBannerAds()
        {
            BannerView.Hide();
            BannerToggle = false;
        }

        private void OnToggleBannerAds()
        {
            if (BannerToggle)
            {
                OnHideBannerAds();
            }
            else
            {
                OnShowBannerAds();
            }
        }

        #endregion
        
        #region Banner Events
        private void HandleAdLoaded(object sender, EventArgs args)
        {
        }

        private void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
        }

        private void HandleAdOpened(object sender, EventArgs args)
        {
        }

        private void HandleAdClosing(object sender, EventArgs args)
        {
        }

        private void HandleAdClosed(object sender, EventArgs args)
        {
        }

        private void HandleAdLeftApplication(object sender, EventArgs args)
        {
        }
        #endregion

#endif
    }
}
