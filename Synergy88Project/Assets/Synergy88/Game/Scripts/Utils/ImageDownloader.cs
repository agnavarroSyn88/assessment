﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Common;
using Common.Signal;
using Common.Utils;

using UniRx;

namespace Synergy88
{
	public class ImageDownloader : SignalComponent, IService
    {
		private Dictionary<string, Texture2D> cachedTextures = new Dictionary<string, Texture2D>();

        #region IService implementation
        [SerializeField]
        private bool _IsServiceRequired;
        public bool IsServiceRequired
        {
            get
            {
                return _IsServiceRequired;
            }
        }

        public string ServiceName { get { return name; } }

        private ReactiveProperty<ServiceState> _CurrentServiceState = new ReactiveProperty<ServiceState>(ServiceState.Uninitialized);
        public ReactiveProperty<ServiceState> CurrentServiceState
        {
            get
            {
                return _CurrentServiceState;
            }
        }

        public void InitializeService()
        {
            //Factory.Register<ImageDownloader>(this);
            Receive<DownloadImageSignal>()
                .Subscribe(sig => OnDownloadImage(sig))
                .AddTo(this);

            CurrentServiceState.Value = ServiceState.Initialized;
        }

        public void TerminateService()
        {
			//Factory.Clean<ImageDownloader>();
        }
        #endregion

		private void OnDownloadImage(DownloadImageSignal signal) {
            string imageId = signal.ImageId;
            string imageUrl = signal.Url;

			// TODO: 
			//	Please use uniRx instead
			//	Add download progress checking
			this.StartCoroutine(this.DownloadImage(imageId, imageUrl));

			// uniRx
			//ObservableWWW.Get(imageId, null, this).Subscribe(_ => {
			//	Debug.LogFormat("Downloaded:{0}\n", _);
			//});
		}

		private IEnumerator DownloadImage(string imageId, string imageUrl) {
			// check local images
			string localPath = imageUrl;
			Texture2D texture;
			byte[] textureData;

			// check if texture is cached, then use the cached instead
			if (this.cachedTextures.ContainsKey(imageId)) {
				texture = this.cachedTextures[imageId];
			}
			else {
				localPath = imageUrl;

				// wait for the image to be downloaded
				WWW www = new WWW(imageUrl);
				yield return www;	

				if (www.error != null) {
					Debug.LogErrorFormat("Error downloading image! id:{0} url:{1}\n", imageId, imageUrl);
					yield break;
				}

				// get the downloaded texture
				texture = (Texture2D)www.texture;
				textureData = www.bytes;

				// cache the texture
				this.cachedTextures[imageId] = texture;
			}

            Publish(new ImageDownloadedSignal()
            {
                ImageId = imageId,
                Texture = texture
            });
		}
	}

}