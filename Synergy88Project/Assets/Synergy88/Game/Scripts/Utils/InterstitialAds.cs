﻿using UnityEngine;

using System;
using System.Collections;

#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
using GoogleMobileAds;
using GoogleMobileAds.Api;
#endif

using Common;
using Common.Signal;

// alias
using Const = Synergy88.Const;

using UniRx;

namespace Synergy88
{

    public class InterstitialAds : SignalComponent
    {

        private const string DUMMY_TEST_DEVICE = "0123456789ABCDEF0123456789ABCDEF";
        private const string DUMMY_KEYWORD = "game";
        private static readonly DateTime DUMMY_DATE = new DateTime(1985, 1, 1);
        private const string DUMMY_KEY = "key";
        private const string DUMMY_VALUE = "value";

#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
        // interstitals
        private InterstitialAd Interstitial;
#endif

        private void Awake()
        {
#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
            Receive<ShowInterstitialAdsSignal>()
                .Subscribe(_ => OnShowInterstitialAds())
                .AddTo(this);

            Receive<HideInterstitialAdsSignal>()
                .Subscribe(_ => OnHideInterstitialAds())
                .AddTo(this);

            Receive<ToggleInterstitialAdsSignal>()
                .Subscribe(_ => OnToggleInterstitialAds())
                .AddTo(this);
#endif
        }

        private void Start()
        {
#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)
            string adUnitId = string.Empty;

            // mobile
            if (Platform.IsMobileAndroid())
            {
                adUnitId = Const.GA_INTERSTITIALS_AD_UNIT_ID_ANDROID;
            }
            else if (Platform.IsMobileIOS())
            {
                adUnitId = Const.GA_INTERSTITIALS_AD_UNIT_ID_IOS;
            }
            // editor
            else
            {
                // editor
#if UNITY_ANDROID
                adUnitId = Const.GA_INTERSTITIALS_AD_UNIT_ID_ANDROID;
#elif UNITY_IOS
				adUnitId = Const.GA_INTERSTITIALS_AD_UNIT_ID_IOS;
#else
				Assertion.Assert(false, "Unsupported Platform");
#endif
            }

            // create interstitials
            Interstitial = new InterstitialAd(adUnitId);

            // register for ad events.
            Interstitial.AdLoaded += HandleInterstitialLoaded;
            Interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
            Interstitial.AdOpened += HandleInterstitialOpened;
            Interstitial.AdClosing += HandleInterstitialClosing;
            Interstitial.AdClosed += HandleInterstitialClosed;
            Interstitial.AdLeftApplication += HandleInterstitialLeftApplication;

            // add handler
            MobileAdsHandler handler = new MobileAdsHandler();
            Interstitial.SetInAppPurchaseHandler(handler);

            // Load an interstitial ad.
            Interstitial.LoadAd(this.CreateAdRequest());
#endif
        }

#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)

        private AdRequest CreateAdRequest()
        {
            return new AdRequest.Builder()
                .AddTestDevice(AdRequest.TestDeviceSimulator)
                .AddTestDevice(DUMMY_TEST_DEVICE)
                .AddKeyword(DUMMY_KEYWORD)
                .SetGender(Gender.Male)
                .SetBirthday(DUMMY_DATE)
                .TagForChildDirectedTreatment(false)
                .AddExtra(DUMMY_KEY, DUMMY_VALUE)
                .Build();

        }

        #region Signals

        private void OnShowInterstitialAds()
        {
            if (Interstitial.IsLoaded())
            {
                Interstitial.Show();
            }
            // not yet ready
        }

        private void OnHideInterstitialAds()
        {

        }

        private void OnToggleInterstitialAds()
        {

        }

        #endregion

        #region Interstitial callback handlers

        private void HandleInterstitialLoaded(object sender, EventArgs args)
        {
        }

        private void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
        }

        private void HandleInterstitialOpened(object sender, EventArgs args)
        {
        }

        private void HandleInterstitialClosing(object sender, EventArgs args)
        {
        }

        private void HandleInterstitialClosed(object sender, EventArgs args)
        {
        }

        private void HandleInterstitialLeftApplication(object sender, EventArgs args)
        {
        }

        #endregion

#endif

    }

#if ENABLE_GOOGLE_MOBILE_ADS && (UNITY_ANDROID || UNITY_IOS)

    public class MobileAdsHandler : IInAppPurchaseHandler
    {

        private readonly string[] validSkus = { "android.test.purchased" };

        //Will only be sent on a success.
        public void OnInAppPurchaseFinished(IInAppPurchaseResult result)
        {
            result.FinishPurchase();
            // "Purchase Succeeded! Credit user here.";
        }

        //Check SKU against valid SKUs.
        public bool IsValidPurchase(string sku)
        {
            foreach (string validSku in validSkus)
            {
                if (sku == validSku)
                {
                    return true;
                }
            }

            return false;
        }

        //Return the app's public key.
        public string AndroidPublicKey
        {
            //In a real app, return public key instead of null.
            get
            {
                return null;
            }
        }
    }

#endif

}