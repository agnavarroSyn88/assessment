﻿using System;
using System.Collections;
using System.Collections.Generic;

using Common;
using Common.Signal;

using UniRx;
using UniRx.Triggers;

// alias
using Const = Synergy88.Const;

namespace Synergy88
{
    using UnityEngine;
#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
    using UnityEngine.Advertisements;
#endif

    /// <summary>
    /// Handles display of unity ads.
    /// </summary>
	public class UnityAds : SignalComponent, IService
    {
        /// <summary>
        /// Holder for subscriptions to be disposed when the service is terminated.
        /// </summary>
        private CompositeDisposable TerminationDisposables = new CompositeDisposable();

        #region IService implementation
        [SerializeField]
        private bool _IsServiceRequired;
        public bool IsServiceRequired
        {
            get
            {
                return _IsServiceRequired;
            }
        }

        public string ServiceName { get { return name; } }

        private ReactiveProperty<ServiceState> _CurrentServiceState = new ReactiveProperty<ServiceState>(ServiceState.Uninitialized);
        public ReactiveProperty<ServiceState> CurrentServiceState
        {
            get
            {
                return _CurrentServiceState;
            }
        }

        public void InitializeService()
        {
#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
            Receive<ShowUnityAdsSignal>()
                .Subscribe(sig => OnShowUnityAds(sig.Region))
                .AddTo(TerminationDisposables);
#endif

            CurrentServiceState.Value = ServiceState.Initialized;
        }

        public void TerminateService()
        {
            // dispose all subscriptions and clear list
            TerminationDisposables.Clear();
        }
        #endregion

#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
        private void Initialize()
        {
            string adUnitId = string.Empty;

            // mobile
            if (Platform.IsMobileAndroid())
            {
                adUnitId = Const.UNITY_GAME_ID_ANDROID;
            }
            else if (Platform.IsMobileIOS())
            {
                adUnitId = Const.UNITY_GAME_ID_IOS;
            }
            // editor
            else
            {
                // editor
#if UNITY_ANDROID
                adUnitId = Const.UNITY_GAME_ID_ANDROID;
#elif UNITY_IOS
				adUnitId = Const.UNITY_GAME_ID_IOS;
#else
				Assertion.Assert(false, "Unsupported Platform");
#endif
            }
#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
            if (!Advertisement.isInitialized)
            {
                Advertisement.Initialize(adUnitId);
            }
#endif
        }

        private void OnShowUnityAds(string region)
        {
#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
            if (!string.IsNullOrEmpty(region))
            {
                if (Advertisement.IsReady(region))
                {
                    ShowOptions options = new ShowOptions();
                    options.resultCallback = this.HandleResult;
                    Advertisement.Show(region, options);
                }
            }
            else
            {
                if (Advertisement.IsReady())
                {
                    Advertisement.Show();
                }
            }
#endif
        }

#if (UNITY_ANDROID || UNITY_IOS) && UNITY_ADS
        private void HandleResult(ShowResult result)
        {
            Debug.LogFormat("UnityAds::HandleResult Result:{0}\n", result);
        }
#endif
#endif
    }
}
