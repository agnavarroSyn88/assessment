﻿using UnityEngine;
using UnityEngine.Analytics;

using System;
using System.Collections;
using System.Collections.Generic;

using Common;
using Common.Signal;
using Common.Utils;
using Common.Query;

using UniRx;

namespace Synergy88 {
	
    /// <summary>
    /// Listens to signals to add custom analytics events.
    /// </summary>
	public class UnityAnalytics : SignalComponent, IService
    {
		// events
		public const string UNI_ANA_SCREEN_EVENT = "ScreenEvent";
		public const string UNI_ANA_BUTTON_EVENT = "ButtonEvent";
		// settings event
		// coins event
		public const string UNI_ANA_COINS_EVENT = "CoinsEvent";
		// shop event
		public const string UNI_ANA_SHOP_EVENT = "ShopEvent";
		// moregames event
		public const string UNI_ANA_MOREGAMES_EVENT = "MoreGamesEvent";

		// TODO: Generalize the keys
		// keys
		public const string UNI_ANA_SCREEN_KEY = "Screen";
		public const string UNI_ANA_BUTTON_KEY = "Button";

		// shop keys
		// coins keys
		public const string UNI_ANA_ITEM_ID = "ItemId";
		public const string UNI_ANA_ITEM_NAME = "ItemName";
		public const string UNI_ANA_ITEM_QUANTITY = "ItemQuantity";

		// TODO: Generalize the data
		private Dictionary<string, object> data = new Dictionary<string, object>();

        /// <summary>
        /// Holder for subscriptions to be disposed when the service is terminated.
        /// </summary>
        private CompositeDisposable TerminationDisposables = new CompositeDisposable();

        #region IService implementation
        [SerializeField]
        private bool _IsServiceRequired;
        public bool IsServiceRequired
        {
            get
            {
                return _IsServiceRequired;
            }
        }

        public string ServiceName { get { return name; } }

        private ReactiveProperty<ServiceState> _CurrentServiceState = new ReactiveProperty<ServiceState>(ServiceState.Uninitialized);
        public ReactiveProperty<ServiceState> CurrentServiceState
        {
            get
            {
                return _CurrentServiceState;
            }
        }

        public void InitializeService()
        {
            Factory.Register<UnityAnalytics>(this);

            /*
			Dictionary<string, object> dummyData = new Dictionary<string, object>() {
				{ "key_001", 1 },
			};
			
			Analytics.CustomEvent("Event001", dummyData);
			Analytics.SetUserBirthYear(1991);
			Analytics.SetUserGender(Gender.Male);
			Analytics.SetUserId("User001");
			Analytics.Transaction("Product001", 0.99, "USD");
			*/

            Receive<OnLoadSceneSignal>()
                .Subscribe(sig => OnLoadScene(sig.SceneName))
                .AddTo(TerminationDisposables);

            Receive<ButtonClickedSignal>()
                .Subscribe(sig => OnClickedButton(sig))
                .AddTo(TerminationDisposables);

            CurrentServiceState.Value = ServiceState.Initialized;
        }

        public void TerminateService()
        {
			Factory.Clean<UnityAnalytics>();

            // dispose all subscriptions and clear list
            TerminationDisposables.Clear();
        }
        #endregion

		#region Signals

		private void OnLoadScene(EScene scene) {
			this.data.Clear();
			this.data.Add(UNI_ANA_SCREEN_KEY, (object)scene.ToString());

			Analytics.CustomEvent(UNI_ANA_SCREEN_EVENT, this.data);
		}

		private void OnClickedButton(ButtonClickedSignal signal) {
            EButtonType button = signal.ButtonType;
			this.data.Clear();
			this.data.Add(UNI_ANA_SCREEN_KEY, (object)button.ToString());

			Analytics.CustomEvent(UNI_ANA_SCREEN_EVENT, this.data);

            // add additional events for item buttons
            switch (button)
            {
                case EButtonType.CoinItem:
                    {
                        string itemId = (string)signal.Data;
                        Debug.LogFormat("Track: type: {0}, id: {1}", button, itemId);

                        this.data.Clear();
                        this.data.Add(UNI_ANA_ITEM_ID, itemId);
                        this.data.Add(UNI_ANA_ITEM_QUANTITY, 1);
                        Analytics.CustomEvent(UNI_ANA_COINS_EVENT, this.data);
                    }
                    break;
                case EButtonType.ShopItem:
                    {
                        string itemId = (string)signal.Data;
                        Debug.LogFormat("Track: type: {0}, id: {1}", button, itemId);

                        this.data.Clear();
                        this.data.Add(UNI_ANA_ITEM_ID, itemId);
                        this.data.Add(UNI_ANA_ITEM_QUANTITY, 1);
                        Analytics.CustomEvent(UNI_ANA_SHOP_EVENT, this.data);
                    }
                    break;
                case EButtonType.MoreGamesItem:
                    {
                        string itemId = (string)signal.Data;
                        IQueryRequest request = QuerySystem.Start(QueryIds.MoreGamesItems);
                        request.AddParameter(QueryIds.MoreGamesItemId, itemId);
                        MoreGamesItemData item = QuerySystem.Complete<MoreGamesItemData>();
                        Debug.LogFormat("Track: type: {0}, id: {1}", button, itemId);

                        this.data.Clear();
                        this.data.Add(UNI_ANA_ITEM_ID, itemId);
                        this.data.Add(UNI_ANA_ITEM_NAME, item.Name);
                        Analytics.CustomEvent(UNI_ANA_MOREGAMES_EVENT, this.data);
                    }
                    break;
                default:
                    break;
            }
		}

        #endregion
    }

}