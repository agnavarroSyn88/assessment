﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using FullInspector;

using Common;
using Common.Utils;

namespace Synergy88
{
    public enum WordStatus
    {
        Invalid     = 0x0,
        NotAWord    = 0x1 << 0,
        Used        = 0x1 << 1,
        UnUsed      = 0x1 << 2,
        Max         = 0x1 << 3,
    };

    public class WordValidator : BaseBehavior
    {
        public static readonly string ENGLISH = "en";
        public static readonly string SOWPODS = "sowpods";
        public static readonly Char NEXT_LINE = '\n';
        
        [SerializeField]
        private Dictionary<string, bool> CachedWords;

        protected override void Awake()
        {
            Factory.Register<WordValidator>(this);

            base.Awake();
            
            CachedWords = new Dictionary<string, bool>();
            InitializeWords();
        }

        private void OnDestroy()
        {
            Factory.Clean<WordValidator>();
        }

        private void InitializeWords()
        {
            string[] words = (Resources.Load(ENGLISH) as TextAsset).text.Split(NEXT_LINE);

            foreach (string word in words)
            {
                CachedWords.Add(word.Trim(), false);
            }
        }

        public WordStatus IsValid(string word)
        {
            string clean = word.ToLower().Trim();
            Debug.Log(clean);
            bool contains = CachedWords.ContainsKey(clean);

            if (contains)
            {
                if (CachedWords[clean])
                {
                    return WordStatus.Used;
                }
                else
                {
                    CachedWords[clean] = true;
                    return WordStatus.UnUsed;
                }
            }

            return WordStatus.NotAWord;
        }

        public void OccupyWord(string word)
        {
            string clean = word.ToLower().Trim();
            bool contains = CachedWords.ContainsKey(clean);

            if (contains)
            {
                CachedWords[clean] = true;
            }
        }
    }
}